using System;
using System.Threading;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Server;
using Server.Commands;
using Server.Gumps;
using Server.Guilds;
using Server.Engines.PartySystem;
using Server.Network;
using Server.Items;
using Server.Prompts;
using Server.Targeting;
using Server.Mobiles;
using Server.Accounting;
using Server.Engines.Help;

namespace Server.Misc
{
    class ServerConsole
    {
        private static bool Hearconsole;
        private static ArrayList m_ConsoleHear = new ArrayList();
        private static PageEntry[] m_List;
        public static bool paging;
        public static void Initialize()
        {
            EventSink.ServerStarted += new ServerStartedEventHandler(EventSink_ServerStarted);
            EventSink.Speech += new SpeechEventHandler(OnSpeech);
        }
        public static void EventSink_ServerStarted()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(ConsoleListen));
            Console.WriteLine("Console Commands initialized...");
        }
        private static void OnSpeech(SpeechEventArgs args)
        {
            if (args.Mobile != null && Hearconsole)
            {
                try
                {
                    if (args.Mobile.Region.Name.Length > 0)
                        Console.WriteLine("" + args.Mobile.Name + " (" + args.Mobile.Region.Name + "): " + args.Speech + "");
                    else
                        Console.WriteLine("" + args.Mobile.Name + ": " + args.Speech + "");
                }
                catch
                { 
                }
            }
        }
        public static void ConsoleListen(Object stateInfo)
        {
            if (!paging)
            {
                string input = Console.ReadLine();
                Next(input);
            }
        }
        public static void PageResp(object obj)
        {
            paging = true;
            object[] objects = (object[])obj;
            int w = (int)objects[0];
            int pag = (int)objects[1];
            int paG;
            if (w == 1)
            {
            up:
                try { paG = Convert.ToInt32(Console.ReadLine()); }
                catch { Console.WriteLine("Thats not a number,try again."); goto up; }
                Console.WriteLine("Type your response");
                object[] ob = new object[] { 2, paG };
                ThreadPool.QueueUserWorkItem(new WaitCallback(PageResp), ob);
            }
            else
            {
                string resp = Console.ReadLine();
                ArrayList list = PageQueue.List;
                m_List = (PageEntry[])list.ToArray(typeof(PageEntry));
                if (m_List.Length > 0)
                {
                    if (pag > m_List.Length)
                    {
                        Console.WriteLine("Error: Not a valid page number");
                    }
                    else
                    {
                        for (int i = 0; i < m_List.Length; ++i)
                        {
                            PageEntry e = m_List[i];
                            if (i == pag)
                            {
                                e.Sender.SendGump(new MessageSentGump(e.Sender, "Admin", resp));
                                PageQueue.Remove(e);
                                Console.WriteLine("Message Sent...");
                            }
                        }
                    }
                }
                else { Console.WriteLine("There are no pages to display."); }
            }
            paging = false;
            ThreadPool.QueueUserWorkItem(new WaitCallback(ConsoleListen));
        }
        public static void BroadcastMessage(AccessLevel ac, int hue, string message)
        {
            foreach (NetState state in NetState.Instances)
            {
                Mobile m = state.Mobile;
                if (m != null && m.AccessLevel >= ac)
                    m.SendMessage(hue, message);
            }
        }
        public static void Next(string nput)
        {
            string input = nput.ToLower();
            if (input.StartsWith("bc"))
            {
                string imput = input.Replace("bc", "");
                BroadcastMessage(AccessLevel.Player, 0x35, String.Format("[Admin] {0}", imput));
                Console.WriteLine("Players will see: {0}", imput);
            }
            else if (input.StartsWith("sc"))
            {
                string imput = input.Replace("staff", "");
                BroadcastMessage(AccessLevel.Counselor, 0x32, String.Format("[Admin] {0}", imput));
                Console.WriteLine("Staff will see: {0}", imput);
            }
            else if (input.StartsWith("ban"))
            {
                string imput = input.Replace("ban", "");
                ArrayList list = new ArrayList();
                List<NetState> states = NetState.Instances;
                if (states.Count == 0)
                { Console.WriteLine("There are no players online."); }
                for (int i = 0; i < states.Count; ++i)
                {
                    Account a = states[i].Account as Account;
                    if (a == null)
                        continue;
                    Mobile m = states[i].Mobile;
                    if (m == null)
                        continue;
                    string innput = imput.ToLower();
                    if (m.Name.ToLower() == innput.Trim())
                    {
                        NetState m_ns = m.NetState;
                        Console.WriteLine("Mobile name: '{0}' Account name: '{1}'", m.Name, a.Username);
                        a.Banned = true;
                        m_ns.Dispose();
                        Console.WriteLine("Banning complete.");
                    }
                }
            }
            else if (input.StartsWith("kick"))
            {
                string imput = input.Replace("kick", "");
                ArrayList list = new ArrayList();
                List<NetState> states = NetState.Instances;
                if (states.Count == 0)
                { Console.WriteLine("There are no players online."); }
                for (int i = 0; i < states.Count; ++i)
                {
                    Account a = states[i].Account as Account;
                    if (a == null)
                        continue;
                    Mobile m = states[i].Mobile;
                    if (m == null)
                        continue;
                    string innput = imput.ToLower();
                    if (m.Name.ToLower() == innput.Trim())
                    {
                        NetState m_ns = m.NetState;
                        Console.WriteLine("Mobile name: '{0}' Account name: '{1}'", m.Name, a.Username);
                        m_ns.Dispose();
                        Console.WriteLine("Kicking complete.");
                    }
                }
            }
            else
            {
                switch (input.Trim())
                {
                    case "shutdown":
                        {
                            Misc.AutoSave.Save();
                            Core.Process.Kill();
                            break;
                        }
                    case "shutdown nosave": Core.Process.Kill(); break;
                    case "restart":
                        {
                            BroadcastMessage(AccessLevel.Player, 0x35, String.Format("[Server] We are restarting..."));
                            Misc.AutoSave.Save();
                            Process.Start(Core.ExePath, Core.Arguments);
                            Core.Process.Kill();
                            break;
                        }
                    case "restart nosave": Process.Start(Core.ExePath, Core.Arguments); Core.Process.Kill(); break;
                    case "online":
                        {
                            ArrayList list = new ArrayList();
                            List<NetState> states = NetState.Instances;
                            if (states.Count == 0)
                            { Console.WriteLine("There are no users online at this time."); }
                            for (int i = 0; i < states.Count; ++i)
                            {
                                Account a = states[i].Account as Account;
                                if (a == null)
                                    continue;
                                Mobile m = states[i].Mobile;
                                if (m != null)
                                    Console.WriteLine("- Account: {0}, Name: {1}, IP: {2}", a.Username, m.Name, states[i]);
                            }
                            break;
                        }
                    case "save": Misc.AutoSave.Save(); break;
                    case "hear"://credit to Zippy for the HearAll script!
                        {
                            Hearconsole = !Hearconsole;
                            if (Hearconsole)
                                Console.WriteLine("Now sending all speech to the console.");
                            else
                                Console.WriteLine("No longer sending speech to the console.");
                            break;
                        }
                    case "pages":
                        {
                            paging = true;
                            ArrayList list = PageQueue.List;
                            PageEntry e;
                            for (int i = 0; i < list.Count; )
                            {
                                e = (PageEntry)list[i];
                                if (e.Sender.Deleted || e.Sender.NetState == null)
                                {
                                    e.AddResponse(e.Sender, "[Logout]");
                                    PageQueue.Remove(e);
                                }
                                else { ++i; }
                            }
                            m_List = (PageEntry[])list.ToArray(typeof(PageEntry));
                            if (m_List.Length > 0)
                            {
                                for (int i = 0; i < m_List.Length; ++i)
                                {
                                    e = m_List[i];
                                    string type = PageQueue.GetPageTypeName(e.Type);
                                    Console.WriteLine("--------------Page Number: " + i + " --------------------");
                                    Console.WriteLine("Player   :" + e.Sender.Name);
                                    Console.WriteLine("Catagory :" + type);
                                    Console.WriteLine("Message  :" + e.Message);
                                }
                                Console.WriteLine("Type the number of the page to respond to.");
                                object[] oj = new object[] { 1, 2 };
                                ThreadPool.QueueUserWorkItem(new WaitCallback(PageResp), oj);
                            }
                            else { Console.WriteLine("No pages to display."); paging = false; }
                            break;
                        }
                    case "help":
                    case "list": //Credit to HomeDaddy for this wonderful list!
                    default:
                        {
                            Console.WriteLine(" ");
                            Console.WriteLine("Commands:");
                            Console.WriteLine("save            - Performs a forced save.");
                            Console.WriteLine("shutdown        - Performs a forced save then shuts down the server.");
                            Console.WriteLine("shutdown nosave - Shuts down the server without saving.");
                            Console.WriteLine("restart         - Sends a message to players informing them that the server is");
                            Console.WriteLine("                      restarting, performs a forced save, then shuts down and");
                            Console.WriteLine("                      restarts the server.");
                            Console.WriteLine("restart nosave  - Restarts the server without saving.");
                            Console.WriteLine("online          - Shows a list of every person online:");
                            Console.WriteLine("                      Account, Char Name, IP.");
                            Console.WriteLine("bc <message>    - Type this command and your message after it. It will then be");
                            Console.WriteLine("                      sent to all players.");
                            Console.WriteLine("sc <message>    - Type this command and your message after it.It will then be ");
                            Console.WriteLine("                      sent to all staff.");
                            Console.WriteLine("hear            - Copies all local speech to this console:");
                            Console.WriteLine("                      Char Name (Region name): Speech.");
                            Console.WriteLine("pages           - Shows all the pages in the page queue,you type the page");
                            Console.WriteLine("                      number ,then you type your response to the player.");
                            Console.WriteLine("ban <playername>- Kicks and bans the users account.");
                            Console.WriteLine("kick <playername>- Kicks the user.");
                            Console.WriteLine("list or help    - Shows this list.");
                            Console.WriteLine(" ");
                            break;
                        }
                }
            }
            if (!paging)
                ThreadPool.QueueUserWorkItem(new WaitCallback(ConsoleListen));
        }
    }
}

#region UOAvocation - Console Watchdog [01-01]

namespace Server
{
    public class ConsoleCommandListener
    {
        public static void Initialize()
        {
            EventSink.Command += new CommandEventHandler(cmd_Event);
        }

        public static void cmd_Event(CommandEventArgs e)
        {
            Console.WriteLine(e.Mobile.Name + String.Format(" ({0}): [", ((Account)e.Mobile.Account).Username) + e.Command + " " + e.ArgString);
        }
    }

    class Statistics
    {
        public class Config
        {
            public static bool Enabled = true;                            // Is this system enabled?
            public static bool ConsoleReport = true;                      // Should we report statistics on console?
            public static int Interval = 5;                               // What's the statistics update interval, in minutes?
            public static AccessLevel CanSeeStats = AccessLevel.Player;   // What's the level required to see statistics in-game?
            public static AccessLevel CanUpdateStats = AccessLevel.Seer;  // What's the level required to update statistics in-game?
        }

        public static TimeSpan ShardAge { get { return m_ShardAge; } }
        public static TimeSpan Uptime { get { return m_Uptime; } }
        public static TimeSpan TotalGameTime { get { return m_TotalGameTime; } }
        public static DateTime LastRestart { get { return m_LastRestart; } }
        public static DateTime LastStatsUpdate { get { return m_LastStatsUpdate; } }
        public static int ActiveAccounts { get { return m_ActiveAccounts; } }
        public static int ActiveStaffMembers { get { return m_ActiveStaffMembers; } }
        public static int ActiveGuilds { get { return m_ActiveGuilds; } }
        public static int ActiveParties { get { return m_ActiveParties; } }
        public static int PlayersInParty { get { return m_PlayersInParty; } }
        public static int PlayerHouses { get { return m_PlayerHouses; } }
        public static int PlayerGold { get { return m_PlayerGold; } }
        public static int PlayersOnline { get { return m_PlayersOnline; } }
        public static int StaffOnline { get { return m_StaffOnline; } }

        private static TimeSpan m_ShardAge;
        private static TimeSpan m_Uptime;
        private static TimeSpan m_TotalGameTime;
        private static DateTime m_LastRestart;
        private static DateTime m_LastStatsUpdate;
        private static int m_ActiveAccounts;
        private static int m_ActiveStaffMembers;
        private static int m_ActiveGuilds;
        private static int m_ActiveParties;
        private static int m_PlayersInParty;
        private static int m_PlayerHouses;
        private static int m_PlayerGold;
        private static int m_PlayersOnline;
        private static int m_StaffOnline;

        private static List<String> StatsList = new List<String>();

        public static void Initialize()
        {
            if (Config.Enabled)
            {
                CommandSystem.Register("Statistics", Config.CanSeeStats, new CommandEventHandler(SeeStatistics_OnCommand));
                CommandSystem.Register("UpdateStatistics", Config.CanUpdateStats, new CommandEventHandler(UpdateStatistics_OnCommand));
                Timer.DelayCall(TimeSpan.Zero, TimeSpan.FromMinutes(Config.Interval), CollectStats);
            }
        }

        [Usage("Statistics")]
        [Description("Shows shard statistics.")]
        private static void SeeStatistics_OnCommand(CommandEventArgs e)
        {
            Mobile m = e.Mobile;
            m.CloseGump(typeof(StatisticsGump));
            m.SendGump(new StatisticsGump());
        }

        [Usage("UpdateStatistics")]
        [Description("Updates shard statistics.")]
        private static void UpdateStatistics_OnCommand(CommandEventArgs e)
        {
            Mobile m = e.Mobile;
            Console.WriteLine("Statistics: {0} ({1}) has updated statistics in-game.", m.RawName, m.AccessLevel.ToString());
            CollectStats();
            m.SendMessage(68, "Statistics updated successful.");
            m.CloseGump(typeof(StatisticsGump));
            m.SendGump(new StatisticsGump());
        }

        private static void CollectStats()
        {
            Stopwatch watch = Stopwatch.StartNew();

            m_StaffOnline = 0;
            m_PlayersOnline = 0;
            m_PlayersInParty = 0;
            m_PlayerHouses = 0;
            m_PlayerGold = 0;
            m_ActiveStaffMembers = 0;
            m_TotalGameTime = TimeSpan.Zero;
            StatsList.Clear();

            List<Party> parties = new List<Party>();
            DateTime shardCreation = DateTime.Now;

            foreach (Item i in World.Items.Values)
            {
                if (i is Multis.BaseHouse)
                    m_PlayerHouses++;
            }

            foreach (Mobile m in World.Mobiles.Values)
            {
                if (m is PlayerMobile)
                {
                    if (m.AccessLevel == AccessLevel.Player)
                    {
                        m_PlayerGold += m.TotalGold + m.BankBox.TotalGold;
                    }
                    else
                    {
                        m_ActiveStaffMembers++;
                    }
                }
            }

            foreach (NetState ns in NetState.Instances)
            {
                Mobile m = ns.Mobile;

                if (m != null)
                {
                    if (m.AccessLevel == AccessLevel.Player)
                    {
                        m_PlayersOnline++;
                    }
                    else
                    {
                        m_StaffOnline++;
                    }

                    Party p = Party.Get(m);

                    if (p != null)
                    {
                        m_PlayersInParty++;

                        if (!parties.Contains(p))
                            parties.Add(p);
                    }
                }
            }

            foreach (Account a in Accounts.GetAccounts())
            {
                m_TotalGameTime += a.TotalGameTime;

                if (a.Created < shardCreation)
                    shardCreation = a.Created;
            }

            m_ShardAge = DateTime.Now - shardCreation;
            m_Uptime = DateTime.Now - Clock.ServerStart;
            m_LastRestart = Clock.ServerStart;
            m_LastStatsUpdate = DateTime.Now;
            m_ActiveAccounts = Accounts.Count;
            m_ActiveGuilds = Guild.List.Count;
            m_ActiveParties = parties.Count;

            StatsList.Add(String.Format("Shard Age: {0:n0} days, {1:n0} hours and {2:n0} minutes", m_ShardAge.Days, m_ShardAge.Hours, m_ShardAge.Minutes));
            StatsList.Add(String.Format("Total Game Time: {0:n0} hours and {1:n0} minutes", m_TotalGameTime.TotalHours, m_TotalGameTime.Minutes));
            StatsList.Add(String.Format("Last Restart: {0}", m_LastRestart));
            StatsList.Add(String.Format("Uptime: {0:n0} days, {1:n0} hours and {2:n0} minutes", m_Uptime.Days, m_Uptime.Hours, m_Uptime.Minutes));
            StatsList.Add(String.Format("Active Accounts: {0:n0} [{1:n0} Players Online]", m_ActiveAccounts, m_PlayersOnline));
            StatsList.Add(String.Format("Active Staff Members: {0:n0} [{1:n0} Online]", m_ActiveStaffMembers, m_StaffOnline));
            StatsList.Add(String.Format("Active Parties: {0:n0} [{1:n0} Players]", m_ActiveParties, m_PlayersInParty));
            StatsList.Add(String.Format("Active Guilds: {0:n0}", m_ActiveGuilds));
            StatsList.Add(String.Format("Player Houses: {0:n0}", m_PlayerHouses));
            StatsList.Add(String.Format("Player Gold: {0:n0}", m_PlayerGold));

            watch.Stop();

            if (Config.ConsoleReport)
            {
                Console.WriteLine("");
                Console.WriteLine("===========================================");
                Console.WriteLine("| Statistics Report | {0} |", m_LastStatsUpdate);
                Console.WriteLine("===========================================");
                Console.WriteLine("");
                foreach (String stat in StatsList)
                {
                    Console.WriteLine("* {0}", stat);
                }
                Console.WriteLine("");
                Console.WriteLine("=============================");
                Console.WriteLine("| Generated in {0:F2} seconds |", watch.Elapsed.TotalSeconds);
                Console.WriteLine("=============================");
                Console.WriteLine("");

            }
        }

        public class StatisticsGump : Gump
        {
            public StatisticsGump()
                : base(110, 100)
            {
                Closable = true;
                Dragable = true;
                Disposable = true;
                Resizable = false;

                AddPage(0);

                AddBackground(0, 0, 420, 250, 5054);

                AddImageTiled(10, 10, 400, 20, 2624);
                AddAlphaRegion(10, 10, 400, 20);
                AddLabel(15, 10, 73, Misc.ServerList.ServerName + " Statistics - Last Update: " + m_LastStatsUpdate);

                AddImageTiled(10, 40, 400, 200, 2624);
                AddAlphaRegion(10, 40, 400, 200);
                AddHtml(15, 40, 395, 200, String.Join("<br>", StatsList.ToArray()), false, true);
            }
        }
    }
}

#endregion Edited By: A.A.S.R