#region Script Header
/*
 *                             Gumps.cs
 *                     --------------------------
 * This script was written by Shadow Wolf.
 * Do with this script as you wish except for removing this header.
 * If you remove this header I revoke all permission for use of this 
 * script and you will be considered a theif.
 *
 * Last Updated: March 22, 2006  08:55:00 Eastern Time
 */
#endregion

using System;
using System.Collections;
using System.IO;
using Server;
using Server.Gumps;

namespace ShadowWolf
{
    public class MobileGenGump : Gump
    {
        private const int LabelHue2 = 0x40;
        private string[] m_SkillEntry;
        private string[] m_StatEntry;
        private Mobile from;

        public MobileGenGump(Mobile m, string[] StatEntry, string[] SkillEntry ) : base(25, 25)
        {
            m.CloseGump(typeof(MobileGenGump));
            m_StatEntry = StatEntry;
            m_SkillEntry = SkillEntry;
            from = m;
            MakeGenGump();
        }

        private void MakeGenGump()
        {
            this.Closable = false;
            this.Disposable = true;
            this.Dragable = true;
            this.Resizable = false;

            this.AddPage(0);
            this.AddImage(40, 40, 10851);
            this.AddLabel(200, 60, 0x40, @"Mobile Script Generator");
            this.AddImage(204, 77, 60987);

            this.AddLabel(65, 165, 0x40, @"Namespace");
            this.AddImageTiled(140, 165, 100, 20, 9304);
            this.AddTextEntry(140, 165, 100, 20, 0x480, 0, m_StatEntry[40] );

            this.AddLabel(100, 140, 0x40, @"Name");
            this.AddImageTiled(140, 140, 100, 20, 9304);
            this.AddTextEntry(140, 140, 100, 20, 0x480, 1, m_StatEntry[0] );

            this.AddLabel(75, 190, 0x40, @"Cor Name");
            this.AddImageTiled(140, 190, 100, 20, 9304);
            this.AddTextEntry(140, 190, 100, 20, 0x480, 2, m_StatEntry[1] );

            this.AddLabel(66, 230, 0x40, @"Special Thanks To:");
            this.AddLabel(75, 305, 0x40, @"RunUO Community");

            this.AddButton(350, 250, 247, 248, 1, GumpButtonType.Reply, 0);
            this.AddButton(349, 275, 241, 242, 0, GumpButtonType.Reply, 0);

            this.AddButton(350, 140, 4005, 4007, 2, GumpButtonType.Reply, 0);
            this.AddLabel(392, 140, 1151, @"Skills");

            this.AddButton(350, 165, 4005, 4007, 3, GumpButtonType.Reply, 0);
            this.AddLabel(390, 165, 1151, @"Stats");

            this.AddButton(350, 190, 4005, 4007, 4, GumpButtonType.Reply, 0);
            this.AddLabel(390, 190, 1151, @"Help");
        }

        public override void OnResponse(Server.Network.NetState sender, RelayInfo info)
        {
            switch (info.ButtonID)
            {
                case 0: break;
                case 1:
                    foreach (TextRelay text in info.TextEntries)
                    {
                        switch (text.EntryID)
                        {
                            case 0: m_StatEntry[40] = text.Text; break;
                            case 1: m_StatEntry[0] = text.Text; break;
                            case 2: m_StatEntry[1] = text.Text; break;
                        }
                    }
                    MobileGen.GenerateMobile( from, m_StatEntry, m_SkillEntry ); break;

                case 2: from.SendGump(new MobileSkillsGump(from, m_StatEntry, m_SkillEntry ));
                        from.SendGump(new MobileGenGump(from, m_StatEntry, m_SkillEntry )); break;

                case 3: from.SendGump(new MobileStatsGump(from, m_StatEntry, m_SkillEntry ));
                        from.SendGump(new MobileGenGump(from, m_StatEntry, m_SkillEntry)); break;

                case 4: from.SendGump(new GenHelpGump());
                        from.SendGump(new MobileGenGump(from, m_StatEntry, m_SkillEntry)); break;
            }
        }
    }

    public class MobileStatsGump : Gump
    {
        private const int LabelHue = 0x40;
        private string[] m_StatEntry;
        private string[] m_SkillEntry;
        private Mobile from;

        public MobileStatsGump(Mobile m, string[] StatEntry, string[] SkillEntry ) : base(25, 25)
        {
            m_StatEntry = StatEntry;
            m_SkillEntry = SkillEntry;
            from = m;
            MakeStatsGump();
        }

        private void MakeStatsGump()
        {
            this.Closable = false;
            this.Disposable = true;
            this.Dragable = true;
            this.Resizable = false;
            this.AddPage(0);

            this.AddBackground(25, 25, 490, 625, 9200);
            this.AddLabel(170, 35, 61, @"Mobile Generator Stats Section");

            // Row One
            this.AddLabel(30, 50, LabelHue, @"Namespace");
            this.AddTextEntry(120, 50, 130, 20, 0x480, 0, m_StatEntry[40] );

            this.AddLabel(30, 75, LabelHue, @"Name");
            this.AddTextEntry(120, 75, 130, 20, 0x480, 1, m_StatEntry[0] );

            this.AddLabel(30, 100, LabelHue, @"Cor Name");
            this.AddTextEntry(120, 100, 130, 20, 0x480, 2, m_StatEntry[1] );

            this.AddLabel(30, 125, LabelHue, @"Title");
            this.AddTextEntry(120, 125, 130, 20, 0x480, 3, m_StatEntry[2] );

            this.AddLabel(30, 150, LabelHue, @"AIType");
            this.AddTextEntry(120, 150, 130, 20, 0x480, 4, m_StatEntry[3] );

            this.AddLabel(30, 175, LabelHue, @"FightMode");
            this.AddTextEntry(120, 175, 130, 20, 0x480, 5, m_StatEntry[4] );

            this.AddLabel(30, 200, LabelHue, @"Range Percept");
            this.AddTextEntry(120, 200, 110, 20, 0x480, 6, m_StatEntry[5] );

            this.AddLabel(30, 225, LabelHue, @"Range Fight");
            this.AddTextEntry(120, 225, 110, 20, 0x480, 7, m_StatEntry[6] );

            this.AddLabel(30, 250, LabelHue, @"Act. Spd");
            this.AddTextEntry(120, 250, 110, 20, 0x480, 8, m_StatEntry[7] );

            this.AddLabel(30, 275, LabelHue, @"Pass. Spd");
            this.AddTextEntry(120, 275, 110, 20, 0x480, 9, m_StatEntry[8] );

            this.AddLabel(30, 300, LabelHue, @"Body Mod");
            this.AddTextEntry(120, 300, 110, 20, 0x480, 10, m_StatEntry[9] );

            this.AddLabel(30, 325, LabelHue, @"Base Sound");
            this.AddTextEntry(120, 325, 110, 20, 0x480, 11, m_StatEntry[10] );

            this.AddLabel(30, 350, LabelHue, @"Kills");
            this.AddTextEntry(120, 350, 110, 20, 0x480, 12, m_StatEntry[11] );

            this.AddLabel(30, 375, LabelHue, @"Min Str");
            this.AddTextEntry(120, 375, 110, 20, 0x480, 13, m_StatEntry[12] );

            this.AddLabel(30, 400, LabelHue, @"Max Str");
            this.AddTextEntry(120, 400, 110, 20, 0x480, 14, m_StatEntry[13] );

            this.AddLabel(30, 425, LabelHue, @"Min Dex");
            this.AddTextEntry(120, 425, 110, 20, 0x480, 15, m_StatEntry[14] );

            this.AddLabel(30, 450, LabelHue, @"Max Dex");
            this.AddTextEntry(120, 450, 110, 20, 0x480, 16, m_StatEntry[15] );

            this.AddLabel(30, 475, LabelHue, @"Min Int");
            this.AddTextEntry(120, 475, 110, 20, 0x480, 17, m_StatEntry[16] );

            this.AddLabel(30, 500, LabelHue, @"Max Int");
            this.AddTextEntry(120, 500, 110, 20, 0x480, 18, m_StatEntry[17] );

            this.AddLabel(30, 525, LabelHue, @"Min Hits");
            this.AddTextEntry(120, 525, 110, 20, 0x480, 19, m_StatEntry[18] );

            this.AddLabel(30, 550, LabelHue, @"Max Hits");
            this.AddTextEntry(120, 550, 110, 20, 0x480, 20, m_StatEntry[19] );

            this.AddLabel(30, 575, LabelHue, @"Min Dam");
            this.AddTextEntry(120, 575, 110, 20, 0x480, 21, m_StatEntry[20] );

            this.AddLabel(30, 600, LabelHue, @"Max Dam");
            this.AddTextEntry(120, 600, 110, 20, 0x480, 22, m_StatEntry[21] );

            this.AddLabel(30, 625, LabelHue, @"Phys Dam");
            this.AddTextEntry(120, 625, 110, 20, 0x480, 23, m_StatEntry[22] );

            // Row Two
            this.AddLabel(290, 50, LabelHue, @"Fire Dam");
            this.AddTextEntry(380, 50, 70, 20, 0x480, 24, m_StatEntry[23] );

            this.AddLabel(290, 75, LabelHue, @"Cold Dam");
            this.AddTextEntry(380, 75, 70, 20, 0x480, 25, m_StatEntry[24] );

            this.AddLabel(290, 100, LabelHue, @"Energy Dam");
            this.AddTextEntry(380, 100, 70, 20, 0x480, 26, m_StatEntry[25] );

            this.AddLabel(290, 125, LabelHue, @"Poison Dam");
            this.AddTextEntry(380, 125, 70, 20, 0x480, 27, m_StatEntry[26] );

            this.AddLabel(290, 150, LabelHue, @"Min Phys Resist");
            this.AddTextEntry(410, 150, 70, 20, 0x480, 28, m_StatEntry[27] );

            this.AddLabel(290, 175, LabelHue, @"Max Phys Resist");
            this.AddTextEntry(410, 175, 70, 20, 0x480, 29, m_StatEntry[28] );

            this.AddLabel(290, 200, LabelHue, @"Min Fire Resist");
            this.AddTextEntry(410, 200, 70, 20, 0x480, 30, m_StatEntry[29] );

            this.AddLabel(290, 225, LabelHue, @"Max Fire Resist");
            this.AddTextEntry(410, 225, 70, 20, 0x480, 31, m_StatEntry[30] );

            this.AddLabel(290, 250, LabelHue, @"Min Cold Resist");
            this.AddTextEntry(410, 250, 70, 20, 0x480, 32, m_StatEntry[31] );

            this.AddLabel(290, 275, LabelHue, @"Max Cold Resist");
            this.AddTextEntry(410, 275, 70, 20, 0x480, 33, m_StatEntry[32] );

            this.AddLabel(290, 300, LabelHue, @"Min Energy Resist");
            this.AddTextEntry(410, 300, 70, 20, 0x480, 34, m_StatEntry[33] );

            this.AddLabel(290, 325, LabelHue, @"Max Energy Resist");
            this.AddTextEntry(410, 325, 70, 20, 0x480, 35, m_StatEntry[34] );

            this.AddLabel(290, 350, LabelHue, @"Min Poison Resist");
            this.AddTextEntry(410, 350, 70, 20, 0x480, 36, m_StatEntry[35] );

            this.AddLabel(290, 375, LabelHue, @"Max Poison Resist");
            this.AddTextEntry(410, 375, 70, 20, 0x480, 37, m_StatEntry[36] );

            this.AddLabel(290, 400, LabelHue, @"Fame");
            this.AddTextEntry(380, 400, 70, 20, 0x480, 38, m_StatEntry[37] );

            this.AddLabel(290, 425, LabelHue, @"Karma");
            this.AddTextEntry(380, 425, 70, 20, 0x480, 39, m_StatEntry[38] );

            this.AddLabel(290, 450, LabelHue, @"Virtual Armor");
            this.AddTextEntry(380, 450, 70, 20, 0x480, 40, m_StatEntry[39] );

            // Gump Buttons
            this.AddButton(290, 570, 4020, 4021, 0, GumpButtonType.Reply, 0);
            this.AddLabel(320, 570, LabelHue, @"Cancel");

            this.AddButton(290, 600, 4005, 4006, 1, GumpButtonType.Reply, 0);
            this.AddLabel(320, 600, LabelHue, @"Apply");
        }

        public override void OnResponse(Server.Network.NetState sender, RelayInfo info)
        {
            if (info.ButtonID == 0)
            {
                from.SendGump(new MobileGenGump( from, m_StatEntry, m_SkillEntry ) );
                return;
            }

            foreach (TextRelay text in info.TextEntries)
            {
                switch (text.EntryID)
                {
                    case 0: m_StatEntry[40] = text.Text; break;
                    case 1: m_StatEntry[0] = text.Text; break;
                    case 2: m_StatEntry[1] = text.Text; break;
                    case 3: m_StatEntry[2] = text.Text; break;
                    case 4: m_StatEntry[3] = text.Text; break;
                    case 5: m_StatEntry[4] = text.Text; break;
                    case 6: m_StatEntry[5] = text.Text; break;
                    case 7: m_StatEntry[6] = text.Text; break;
                    case 8: m_StatEntry[7] = text.Text; break;
                    case 9: m_StatEntry[8] = text.Text; break;
                    case 10: m_StatEntry[9] = text.Text; break;
                    case 11: m_StatEntry[10] = text.Text; break;
                    case 12: m_StatEntry[11] = text.Text; break;
                    case 13: m_StatEntry[12] = text.Text; break;
                    case 14: m_StatEntry[13] = text.Text; break;
                    case 15: m_StatEntry[14] = text.Text; break;
                    case 16: m_StatEntry[15] = text.Text; break;
                    case 17: m_StatEntry[16] = text.Text; break;
                    case 18: m_StatEntry[17] = text.Text; break;
                    case 19: m_StatEntry[18] = text.Text; break;
                    case 20: m_StatEntry[19] = text.Text; break;
                    case 21: m_StatEntry[20] = text.Text; break;
                    case 22: m_StatEntry[21] = text.Text; break;
                    case 23: m_StatEntry[22] = text.Text; break;
                    case 24: m_StatEntry[23] = text.Text; break;
                    case 25: m_StatEntry[24] = text.Text; break;
                    case 26: m_StatEntry[25] = text.Text; break;
                    case 27: m_StatEntry[26] = text.Text; break;
                    case 28: m_StatEntry[27] = text.Text; break;
                    case 29: m_StatEntry[28] = text.Text; break;
                    case 30: m_StatEntry[29] = text.Text; break;
                    case 31: m_StatEntry[30] = text.Text; break;
                    case 32: m_StatEntry[31] = text.Text; break;
                    case 33: m_StatEntry[32] = text.Text; break;
                    case 34: m_StatEntry[33] = text.Text; break;
                    case 35: m_StatEntry[34] = text.Text; break;
                    case 36: m_StatEntry[35] = text.Text; break;
                    case 37: m_StatEntry[36] = text.Text; break;
                    case 38: m_StatEntry[37] = text.Text; break;
                    case 39: m_StatEntry[38] = text.Text; break;
                    case 40: m_StatEntry[39] = text.Text; break;
                }
            }

            from.SendGump(new MobileGenGump(from, m_StatEntry, m_SkillEntry ));
        }
    }

    public class MobileSkillsGump : Gump
    {
        private const int EntryHue = 0x480;
        private string[] m_StatEntry;
        private string[] m_SkillEntry;
        private Mobile from;

        public MobileSkillsGump(Mobile m, string[] StatEntry, string[] SkillEntry ) : base(25, 25)
        {
            m_StatEntry = StatEntry;
            m_SkillEntry = SkillEntry;
            from = m;
            MakeSkillsGump();
        }

        private void MakeSkillsGump()
        {
            this.Closable = false;
            this.Disposable = true;
            this.Dragable = true;
            this.Resizable = false;
            this.AddPage(0);

            this.AddBackground(25, 25, 490, 500, 9200);
            this.AddLabel(175, 35, 64, @"Mobile Generator Skills Section");

            this.AddLabel(30, 65, 64, @"Min Anatomy");
            this.AddTextEntry(120, 65, 125, 20, EntryHue, 0, m_SkillEntry[0] );
            this.AddLabel(290, 65, 64, @"Max Anatomy");
            this.AddTextEntry(380, 65, 125, 20, EntryHue, 1, m_SkillEntry[1] );

            this.AddLabel(30, 90, 64, @"Min Archery");
            this.AddTextEntry(120, 90, 125, 20, EntryHue, 2, m_SkillEntry[2] );
            this.AddLabel(290, 90, 64, @"Max Archery");
            this.AddTextEntry(380, 90, 125, 20, EntryHue, 3, m_SkillEntry[3] );


            this.AddLabel(30, 115, 64, @"Min EvalInt");
            this.AddTextEntry(120, 115, 125, 20, EntryHue, 4, m_SkillEntry[4] );
            this.AddLabel(290, 115, 64, @"Max EvalInt");
            this.AddTextEntry(380, 115, 125, 20, EntryHue, 5, m_SkillEntry[5] );

            this.AddLabel(30, 140, 64, @"Min Fencing");
            this.AddTextEntry(120, 140, 125, 20, EntryHue, 6, m_SkillEntry[6] );
            this.AddLabel(290, 140, 64, @"Max Fencing");
            this.AddTextEntry(380, 140, 125, 20, EntryHue, 7, m_SkillEntry[7] );

            this.AddLabel(30, 165, 64, @"Max Focus");
            this.AddTextEntry(120, 165, 125, 20, EntryHue, 8, m_SkillEntry[8] );
            this.AddLabel(290, 165, 64, @"Max Focus");
            this.AddTextEntry(380, 165, 125, 20, EntryHue, 9, m_SkillEntry[9] );

            this.AddLabel(30, 190, 64, @"Min Healing");
            this.AddTextEntry(120, 190, 125, 20, EntryHue, 10, m_SkillEntry[10] );
            this.AddLabel(290, 190, 64, @"Max Healing");
            this.AddTextEntry(380, 190, 125, 20, EntryHue, 11, m_SkillEntry[11] );

            this.AddLabel(30, 215, 64, @"Min Macing");
            this.AddTextEntry(120, 215, 125, 20, EntryHue, 12, m_SkillEntry[12] );
            this.AddLabel(290, 215, 64, @"Max Macing");
            this.AddTextEntry(380, 215, 125, 20, EntryHue, 13, m_SkillEntry[13] );

            this.AddLabel(30, 240, 64, @"Min Magery");
            this.AddTextEntry(120, 240, 125, 20, EntryHue, 14, m_SkillEntry[14] );
            this.AddLabel(290, 240, 64, @"Max Magery");
            this.AddTextEntry(380, 240, 125, 20, EntryHue, 15, m_SkillEntry[15] );

            this.AddLabel(30, 265, 64, @"Min Med");
            this.AddTextEntry(120, 265, 125, 20, EntryHue, 16, m_SkillEntry[16] );
            this.AddLabel(290, 265, 64, @"Max Med");
            this.AddTextEntry(380, 265, 125, 20, EntryHue, 17, m_SkillEntry[17] );

            this.AddLabel(30, 290, 64, @"Min Necro");
            this.AddTextEntry(120, 290, 125, 20, EntryHue, 18, m_SkillEntry[18] );
            this.AddLabel(290, 290, 64, @"Max Necro");
            this.AddTextEntry(380, 290, 125, 20, EntryHue, 19, m_SkillEntry[19] );

            this.AddLabel(30, 315, 64, @"Min Parry");
            this.AddTextEntry(120, 315, 125, 20, EntryHue, 20, m_SkillEntry[20] );
            this.AddLabel(290, 315, 64, @"Max Parry");
            this.AddTextEntry(380, 315, 125, 20, EntryHue, 21, m_SkillEntry[21] );

            this.AddLabel(30, 340, 64, @"Min M Resist");
            this.AddTextEntry(120, 340, 125, 20, EntryHue, 22, m_SkillEntry[22] );
            this.AddLabel(290, 340, 64, @"Max M Resist");
            this.AddTextEntry(380, 340, 125, 20, EntryHue, 23, m_SkillEntry[23] );

            this.AddLabel(30, 365, 64, @"Min Swords");
            this.AddTextEntry(120, 365, 125, 20, EntryHue, 24, m_SkillEntry[24] );
            this.AddLabel(290, 365, 64, @"Max Swords");
            this.AddTextEntry(380, 365, 125, 20, EntryHue, 25, m_SkillEntry[25] );

            this.AddLabel(30, 390, 64, @"Min Tactics");
            this.AddTextEntry(120, 390, 125, 20, EntryHue, 26, m_SkillEntry[26] );
            this.AddLabel(290, 390, 64, @"Max Tactics");
            this.AddTextEntry(380, 390, 125, 20, EntryHue, 27, m_SkillEntry[27] );

            this.AddLabel(30, 415, 64, @"Min Wrestling");
            this.AddTextEntry(120, 415, 125, 20, EntryHue, 28, m_SkillEntry[28] );
            this.AddLabel(290, 415, 64, @"Max Wrestling");
            this.AddTextEntry(380, 415, 125, 20, EntryHue, 29, m_SkillEntry[29] );

            // Gump Buttons
            this.AddButton(390, 490, 4020, 4021, 0, GumpButtonType.Reply, 0);
            this.AddLabel(425, 490, 64, @"Cancel");

            this.AddButton(60, 490, 4005, 4006, 1, GumpButtonType.Reply, 0);
            this.AddLabel(95, 490, 64, @"Apply");

        }

        public override void OnResponse(Server.Network.NetState sender, RelayInfo info)
        {
            if (info.ButtonID == 0)
            {
                from.SendGump(new MobileGenGump(from, m_StatEntry, m_SkillEntry ));
                return;
            }

            foreach (TextRelay text in info.TextEntries)
            {
                switch (text.EntryID)
                {
                    case 0: m_SkillEntry[0] = text.Text; break;
                    case 1: m_SkillEntry[1] = text.Text; break;
                    case 2: m_SkillEntry[2] = text.Text; break;
                    case 3: m_SkillEntry[3] = text.Text; break;
                    case 4: m_SkillEntry[4] = text.Text; break;
                    case 5: m_SkillEntry[5] = text.Text; break;
                    case 6: m_SkillEntry[6] = text.Text; break;
                    case 7: m_SkillEntry[7] = text.Text; break;
                    case 8: m_SkillEntry[8] = text.Text; break;
                    case 9: m_SkillEntry[9] = text.Text; break;
                    case 10: m_SkillEntry[10] = text.Text; break;
                    case 11: m_SkillEntry[11] = text.Text; break;
                    case 12: m_SkillEntry[12] = text.Text; break;
                    case 13: m_SkillEntry[13] = text.Text; break;
                    case 14: m_SkillEntry[14] = text.Text; break;
                    case 15: m_SkillEntry[15] = text.Text; break;
                    case 16: m_SkillEntry[16] = text.Text; break;
                    case 17: m_SkillEntry[17] = text.Text; break;
                    case 18: m_SkillEntry[18] = text.Text; break;
                    case 19: m_SkillEntry[19] = text.Text; break;
                    case 20: m_SkillEntry[20] = text.Text; break;
                    case 21: m_SkillEntry[21] = text.Text; break;
                    case 22: m_SkillEntry[22] = text.Text; break;
                    case 23: m_SkillEntry[23] = text.Text; break;
                    case 24: m_SkillEntry[24] = text.Text; break;
                    case 25: m_SkillEntry[25] = text.Text; break;
                    case 26: m_SkillEntry[26] = text.Text; break;
                    case 27: m_SkillEntry[27] = text.Text; break;
                    case 28: m_SkillEntry[28] = text.Text; break;
                    case 29: m_SkillEntry[29] = text.Text; break;
                }
            }

            from.SendGump(new MobileGenGump(from, m_StatEntry, m_SkillEntry ));
        }
    }

    public class GenHelpGump : Gump
    {
        public GenHelpGump()
            : base(25, 25)
        {
            MakeHelpGump();
        }

        private void MakeHelpGump()
        {
            this.Closable = true;
            this.Disposable = true;
            this.Dragable = true;
            this.Resizable = false;

            this.AddPage(0);
            this.AddImageTiled(62, 51, 527, 430, 2081);
            this.AddHtml(74, 80, 236, 367, @"On the main page there are several buttons.  The Okay button will generate the mobiles code for you after you have selected the mobiles stats and skills.  The Skills button opens a gump that allows you to customize the mobile's skills.  The Stats button opens a gump that allows you to customize the mobile's stats.  The Help button opens this gump.

    There are three text fields that you may enter text into.  The first ( Namespace ) allows you to customize what namespace will be used with the mobile's script.  If you do not know what this is its best to just leave it with the predefined namespace.  There is a second text field on the Stats gump that allows you to enter a longer namespace if required.

    The Second ( Name )allows you to enter a short name for the mobile.  There is a second name text field on the Stats gump that allows for a longer name if required.

    The Third ( Cor Name ) allows you to enter the name of the mobile's corpse.  There is a second name text field on the Stats gump that allows for a longer corpse name if required", (bool)false, (bool)true);
            this.AddHtml(340, 80, 236, 367, @"Stats Page:
    On this page you can customize your mobiles stats  Almost all of the values on this page are numbers but some of them like AIType and FightMode are not.  Make sure you specify a VALID FightMode and AIType.  You can find a list of these in the text document ( Mobile Generator Script ) included with the download of this script.

    Skills Page:
    On this page you can specify the values of each skill for your mobile.  For convenence ive have not added the useless skills that really have no use on a mobile( cooking, crafting skills ).  If you want some of skills that I left out on your mobile you will have to add them manually.

    Well that seems to be about all I have to say, Enjoy the script.", (bool)false, (bool)true);
            this.AddLabel(241, 55, 64, @"Mobile Generator Help Page");
            this.AddLabel(192, 455, 64, @"Mobile Generator Created By Shadow Wolf");
        }
    }
}