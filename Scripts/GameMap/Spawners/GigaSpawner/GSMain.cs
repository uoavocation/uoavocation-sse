using System;
using System.IO;
using System.Collections;
using Server;
using Server.Items;
using Server.Commands;
using Server.Gumps;

namespace Server.GigaSpawners
{
	public class GSMain
	{
		public static Hashtable GSHash = new Hashtable();
		private static bool Errors = true;// True enables error logging
		public static bool Mods = true;// True enables modification logging
		public static bool Debug = false;// True enables modification logging
		#region Igon - Boundry
		private const int VisID = 3546;
		public const int itemidx = 6752;
		public const int itemidy = 6706;
		#endregion
		
		public static void Initialize()
		{
			CommandSystem.Register( "GigaSpawner", AccessLevel.Player, new CommandEventHandler( GigaSpawner_OnCommand ) );
			
		}
		
		#region Show and Hide spawners
		public static void GigaVis( )
		{
			foreach ( DictionaryEntry de in GSHash )
			{
				if ( ((GigaSpawner)de.Value).ItemID == 7955 )
					((GigaSpawner)de.Value).ItemID = VisID;
			}
		}
		public static void GigaNoVis( )
		{
			foreach ( DictionaryEntry de in GSHash )
			{
				if ( ((GigaSpawner)de.Value).ItemID == VisID )
					((GigaSpawner)de.Value).ItemID = 7955;
			}
		}
		#endregion
		
		#region Save Gump Command
		[Usage( "GigaSpawner" )]
		[Description( "Opens GigaSpawner main menu." )]
		private static void GigaSpawner_OnCommand( CommandEventArgs e )
		{
			e.Mobile.SendGump( new GigaSaveGump( e.Mobile ) );
		}
		#endregion
		
		public static void Add( GigaSpawner gs )
		{
			if ( GSHash.ContainsKey( gs.Serial ) )
				return;
			GSHash.Add( gs.Serial, gs );
		}
		
		public static void Remove( GigaSpawner gs )
		{
			if ( GSHash.ContainsKey( gs.Serial ) )
				GSHash.Remove( gs.Serial );
		}
		
		public static GigaSpawner[] FindByMap( Map map )
		{
			ArrayList list = new ArrayList();
			foreach ( DictionaryEntry de in GSHash )
			{
				if ( ((GigaSpawner)de.Value).Map == map )
					list.Add( de.Value );
			}
			return (GigaSpawner[])list.ToArray( typeof(GigaSpawner) );
		}
		
		public static GigaSpawner[] GetInBounds( Rectangle2D area )
		{
			ArrayList list = new ArrayList();
			int w = 0;
			int h = 0;
			foreach ( DictionaryEntry de in GSHash )
			{
				w = area.X + area.Width;
				h = area.Y + area.Height;
				if ( (((GigaSpawner)de.Value).Location.X >= area.X && ((GigaSpawner)de.Value).Location.X <= w) && (((GigaSpawner)de.Value).Location.Y >= area.Y && ((GigaSpawner)de.Value).Location.Y <= h) )
					list.Add( de.Value );
			}
			return (GigaSpawner[])list.ToArray( typeof(GigaSpawner) );
		}
		
		public static GigaSpawner[] All()
		{
			ArrayList list = new ArrayList( GSHash.Values );
			return (GigaSpawner[])list.ToArray( typeof(GigaSpawner) );
		}
		
		public static Point3D RandomFacetLoc( Map map )
		{
			GigaSpawner[] gs = FindByMap(map);
			Point3D loc = Point3D.Zero;
			try
			{
				int x;
				//Try 20 times to get a valid spawner
				for ( int i = 0 ; i < 20; i++ )
				{
					x = Utility.Random( gs.Length );
					if ( gs[x].Parent == null && !gs[x].IsExcluded )
					{
						loc = GetSpawnPosition( gs[x] );
						return loc;
					}
				}
			}
			catch
			{
				return loc;
			}
			return loc;
		}
		
		public static Point3D RandomGlobalLoc( ref Map map )
		{
			GigaSpawner[] gs = All();
			Point3D loc = Point3D.Zero;
			int x;
			for ( int i = 0 ; i < 20; i++ )
			{
				x = Utility.Random( gs.Length );
				if ( gs[x].Parent == null && !gs[x].IsExcluded )
				{
					loc = GetSpawnPosition( gs[x] );
					map = gs[x].Map;
					return loc;
				}
			}
			return loc;
		}
		
		public static Point3D GetSpawnPosition( GigaSpawner gs )
		{
			Map map = gs.Map;
			
			if ( map == null )
				return gs.Location;
			
			// Try 10 times to find a Spawnable location.
			for ( int i = 0; i < 10; i++ )
			{
				int x = gs.SpawnArea.X + (Utility.Random( gs.SpawnArea.Width ) );
				int y = gs.SpawnArea.Y + (Utility.Random( gs.SpawnArea.Height ) );
				int z = map.GetAverageZ( x, y );
				
				if ( map.CanSpawnMobile( new Point2D( x, y ), gs.Z ) )
					return new Point3D( x, y, gs.Z );
				else if ( map.CanSpawnMobile( new Point2D( x, y ), z ) )
					return new Point3D( x, y, z );
			}
			
			return gs.Location;
		}
		
		public static void ToggleActive( bool on )
		{
			GigaSpawner[] allgs = All();
			foreach( GigaSpawner gs in allgs )
			{
				if( gs == null || gs.Deleted )
					continue;
				if( on )
					gs.Start();
				else
					gs.Stop();
			}
		}
		
		public static void Backup( Map map, string FileName, Mobile from, bool confirmed )
		{
			Backup( map, FileName, from, confirmed, new Rectangle2D(0,0,0,0), false );
		}
		
		public static void Backup( Map map, string FileName, Mobile from, bool confirmed, Rectangle2D bounds, bool area )
		{
			#region Save Binary
			if( !Directory.Exists("GigaSpawner") )
				Directory.CreateDirectory("GigaSpawner");
			FileName = FileName.Replace(".gsw","");
			FileName = FileName.Replace(".gsm","");
			FileName = FileName.Replace(".bin","");
				if( map == null )
					FileName = FileName + ".gsw";
				else
					FileName = FileName + ".gsm";
			string path = @"GigaSpawner\" + FileName;
			if( File.Exists(path) && !confirmed )
			{
				from.SendGump( new GSComfirmGump(3,0,FileName,map));
				return;
			}
			string message = "";
			DateTime dt = DateTime.Now;
			int len = 0;
			try
			{
				Console.Write("Saving...");
				GigaSpawner[] GS = null;
				bool world = false;
				if ( map == null )
				{
					GS = All();
					world = true;
				}
				else if(area)
					GS = GetInBounds(bounds);
				else
					GS = FindByMap(map);
				len = GS.Length;
				//Check for Invalid Entries
				foreach(GigaSpawner gs in GS)
				{
					if( gs.InvalidEntries )
					{
						message = "There are invalid entries in some spawners. Check your list and fix them before saving.";
						LogWrite(String.Format("Account: {0} Character: {1} Action: Save", from.Account,from), true);
						LogWrite(message,false);
						from.SendGump( new GigaSaveGump(from,message,FileName) );
						return;
					}
				}
				using(FileStream m_FileStream = new FileStream( path, FileMode.OpenOrCreate, FileAccess.Write ))
				{
					BinaryFileWriter writer = new BinaryFileWriter( m_FileStream,true);
					// Global save/load vars
					writer.Write((bool)world);
					writer.Write((int)len);
					writer.Write( (int) 2 ); // version
					string uid = System.Guid.NewGuid().ToString();
					writer.Write( uid );
					// Spawner specific vars
					lock(GS)
					{
						foreach( GigaSpawner spawner in GS )
						{
							#region Version 2 vars
							writer.Write((string)spawner.Name);
							#endregion
							#region Version 1 vars
							writer.Write((int)spawner.SpawnRange);
							#endregion
							#region Version 0 vars
							writer.Write((int)spawner.HomeRange);
							writer.Write((int)spawner.SPTriggerRange);
							writer.Write((int)spawner.WalkTriggerRange);
							writer.Write((int)spawner.Team);
							writer.Write( spawner.WayPoint );
							writer.Write((Rectangle2D)spawner.SpawnArea);
							writer.Write((string)spawner.Keyword);
							writer.Write( spawner.MinDelay );
							writer.Write( spawner.MaxDelay );
							writer.Write( (int)spawner.SpawnList.Length );
							for ( int i=0; i < spawner.SpawnList.Length; i++ )
							{
								writer.Write( (string)spawner.SpawnList[i] );
								writer.Write( (int)spawner.SpawnAmount[i] );
								writer.Write( (string)spawner.ItemsList[i] );
							}
							writer.Write( (int) spawner.Flags );
							writer.Write((Point3D)spawner.Location);
							if ( world )
								writer.Write((Map)spawner.Map);
							#endregion
						}
					}
					writer.Close();
					m_FileStream.Close();
				}
			}
			catch(Exception err)
			{
				Console.WriteLine("Failed");
				message = err.Message;
				Console.WriteLine( err.Message );
				LogWrite(String.Format("Account: {0} Character: {1} Action: Save", from.Account,from), true);
				LogWrite(message,false);
				from.SendGump( new GigaSaveGump(from,message,FileName) );
				return;
			}
			
			TimeSpan ts = DateTime.Now - dt;
			message = String.Format("Saved {0} GigaSpawners in {1:00}:{2:00}.{3}", len.ToString(), ts.Minutes, ts.Seconds, ts.Milliseconds );
			Console.WriteLine("");
			Console.WriteLine(message);
			if( Mods )
			{
				LogWrite(String.Format("Account: {0} Character: {1} Action: Save", from.Account,from), true, 2);
				LogWrite(message,false,2);
			}
			from.SendGump( new GigaSaveGump(from,message,FileName) );
			#endregion
		}
		
		public static void LoadBackup( Map map, string FileName, Mobile from, bool confirm )
		{
			#region Load Binary
			if( !Directory.Exists("GigaSpawner") )
				Directory.CreateDirectory("GigaSpawner");
			string path = @"GigaSpawner\" + FileName;
			DateTime dt = DateTime.Now;
			string message="";
			int gslen = 0;
			Console.Write("Loading...");
			try
			{
				if ( File.Exists( path ) )
				{
					using ( FileStream m_FileStream = new FileStream( path, FileMode.Open, FileAccess.Read) )
					{
						BinaryReader m_BinaryReader = new BinaryReader(m_FileStream);
						BinaryFileReader reader = new BinaryFileReader(m_BinaryReader);
						// Global save/load vars
						bool world = reader.ReadBool();
						gslen = reader.ReadInt();
						int version = reader.ReadInt();
						string uid = "";
						if( version > 0 )
							uid = reader.ReadString();
						if( (!world && map == null) || (world && map != null) )
						{
							reader.Close();
							m_BinaryReader.Close();
							m_FileStream.Close();
							if( world )
								message = "Can't use world save file with load map";
							else
								message = "Can't use map save file with load world";
							LogWrite(String.Format("Account: {0} Character: {1} Action: Load", from.Account,from), true);
							LogWrite(message,false);
							Console.WriteLine("Failed! Improper file type used for type of import.");
							from.SendGump( new GigaSaveGump(from,message,FileName) );
							return;
						}
						if( !confirm )
						{
							from.SendGump( new GSComfirmGump(1,gslen,FileName,map));
							reader.Close();
							m_BinaryReader.Close();
							m_FileStream.Close();
							return;
						}
						// Spawner specific vars
						GigaSpawner gs;
						for ( int x=0; x < gslen; x++ )
						{
							gs = new GigaSpawner();
							if( uid != "" )
								gs.UID = uid;
							#region Version 2
							if( version > 1 )
								gs.Name = reader.ReadString();
							#endregion
							#region Version 1
							if( version > 0 )
								gs.SpawnRange = reader.ReadInt();
							#endregion
							#region Version 0
							gs.HomeRange = reader.ReadInt();
							gs.SPTriggerRange = reader.ReadInt();
							gs.WalkTriggerRange = reader.ReadInt();
							gs.Team = reader.ReadInt();
							gs.WayPoint = reader.ReadItem() as WayPoint;
							gs.SpawnArea = reader.ReadRect2D();
							gs.Keyword = reader.ReadString();
							gs.MinDelay = reader.ReadTimeSpan();
							gs.MaxDelay = reader.ReadTimeSpan();
							//Spawn Amount Array
							int len = reader.ReadInt();
							gs.SpawnList = new string[len];
							gs.SpawnAmount = new int[len];
							gs.ItemsList = new string[len];
							for ( int i=0; i < gs.SpawnList.Length; i++ )
							{
								gs.SpawnList[i] = reader.ReadString();
								gs.SpawnAmount[i] = reader.ReadInt();
								gs.ItemsList[i] = reader.ReadString();
							}
							gs.Flags = (SpawnerFlags)reader.ReadInt();
							gs.Location = reader.ReadPoint3D();
							if ( world )
								gs.Map = reader.ReadMap();
							else
								gs.Map = map;
							Add(gs);
							gs = null;
							#endregion
						}
						reader.Close();
						m_BinaryReader.Close();
						m_FileStream.Close();
					}
				}
				else
				{
					message = "File not found! - " + path;
					Console.WriteLine("Failed! File not found! - {0}", path);
					LogWrite(String.Format("Account: {0} Character: {1} Action: Load", from.Account,from), true);
					LogWrite(message,false);
					from.SendGump( new GigaSaveGump(from,message,FileName) );
					return;
				}
			}
			catch(Exception err)
			{
				Console.WriteLine("Failed!");
				Console.WriteLine(err.Message);
				message = "Load error: " + err.Message;
				LogWrite(String.Format("Account: {0} Character: {1} Action: Load", from.Account,from), true);
				LogWrite(message,false);
				from.SendGump( new GigaSaveGump(from,message,FileName) );
				return;
			}
			TimeSpan ts = DateTime.Now - dt;
			message = String.Format("Loaded {0} GigaSpawners in {1:00}:{2:00}.{3}", gslen.ToString(), ts.Minutes, ts.Seconds, ts.Milliseconds );
			Console.WriteLine("");
			Console.WriteLine(message);
			if( Mods )
			{
				LogWrite(String.Format("Account: {0} Character: {1} Action: Load", from.Account,from), true, 2);
				LogWrite(message,false,2);
			}
			from.SendGump( new GigaSaveGump(from,message,FileName) );
			#endregion
		}
		
		public static void UnLoadSpawnFile(string filename, Mobile from, bool confirm)
		{
			if( !Directory.Exists("GigaSpawner") )
				Directory.CreateDirectory("GigaSpawner");
			string path = @"GigaSpawner\" + filename;
			string msg = "";
			if( !File.Exists(path) )
			{
				msg = String.Format("File: {0}.bin doesn't exist.",filename);
				LogWrite(String.Format("Account: {0} Character: {1} Action: UnLoad", from.Account,from), true);
				LogWrite(msg,false);
				from.SendGump( new GigaSaveGump(from,msg,filename) );
				return;
			}
			ArrayList toDelete = new ArrayList();
			GigaSpawner[] GS = All();
			string uid = GetUID(filename);
			if( uid == "" )
			{
				msg = String.Format("File {0}.bin created with previous version. No UID found.",filename);
				LogWrite(String.Format("Account: {0} Character: {1} Action: UnLoad", from.Account,from), true);
				LogWrite(msg,false);
				from.SendGump( new GigaSaveGump(from,msg,filename) );
				return;
			}
			foreach( GigaSpawner gs in GS )
			{
				if( gs.UID == uid )
					toDelete.Add(gs);
			}
			if( toDelete.Count == 0 )
			{
				msg = "No GigaSpawners found that were loaded from that file. \nReasons: \nGigaSpawners already unloaded. \nOriginal file has been modified after it's been loaded. \nFile loaded before this feature was implemented.";
				LogWrite(String.Format("Account: {0} Character: {1} Action: UnLoad", from.Account,from), true);
				LogWrite(msg,false);
				from.SendGump( new GigaSaveGump(from,msg,filename) );
				return;
			}
			if( !confirm )
			{
				from.SendGump( new GSComfirmGump(2,toDelete.Count,filename,null));
				return;
			}
			for( int i = 0; i < toDelete.Count; i++ )
				((GigaSpawner)toDelete[i]).Delete();
			msg = String.Format("{0} GigaSpawners successfully unloaded from file {1}.", toDelete.Count, filename);
			toDelete.Clear();
			if( Mods )
			{
				LogWrite(String.Format("Account: {0} Character: {1} Action: UnLoad", from.Account,from), true, 2);
				LogWrite(msg,false,2);
			}
			from.SendGump( new GigaSaveGump(from,msg,filename) );
		}
		
		private static string GetUID(string filename)
		{
			if( !Directory.Exists("GigaSpawner") )
				Directory.CreateDirectory("GigaSpawner");
			string path = @"GigaSpawner\" + filename;
			if( !File.Exists(path) )
				return "";
			using ( FileStream m_FileStream = new FileStream( path, FileMode.Open, FileAccess.Read) )
			{
				BinaryReader m_BinaryReader = new BinaryReader(m_FileStream);
				BinaryFileReader reader = new BinaryFileReader(m_BinaryReader);
				reader.ReadBool();
				reader.ReadInt();
				if(reader.ReadInt() > 0)
					return reader.ReadString();
				reader.Close();
				m_BinaryReader.Close();
				m_FileStream.Close();
			}
			return "";
		}
		
		public static void GigaGumpClose( Mobile from )
		{
			from.CloseGump( typeof( GigaMainGump ) );
			from.CloseGump( typeof( GigaOpsGump ) );
			from.CloseGump( typeof( GigaOpsGump2 ) );
			from.CloseGump( typeof( GigaListGump ) );
			from.CloseGump( typeof( SpawnEntryMenu ) );
			from.CloseGump( typeof( SpawnEntryEditor ) );
			from.CloseGump( typeof( ItemEntryEditor ) );
			from.CloseGump( typeof( GigaSaveGump ) );
			from.CloseGump( typeof( GigaModByGump ) );
			from.CloseGump( typeof( GSFileList ) );
		}
		
		public static void LogWrite( string msg, bool NewEntry )
		{
			LogWrite( msg,NewEntry,1);
		}
		public static void LogWrite( string msg, bool NewEntry, byte Err )
		{
			if( (Err == 1 && !Errors) || (Err == 2 && !Mods) || (Err == 3 && !Debug) )
				return;
			
			if( !Directory.Exists("GigaSpawner") )
				Directory.CreateDirectory("GigaSpawner");
			if( !Directory.Exists(@"GigaSpawner\Logs") )
				Directory.CreateDirectory(@"GigaSpawner\Logs");
			if( !Directory.Exists(@"GigaSpawner\Logs\Debug") && Debug )
				Directory.CreateDirectory(@"GigaSpawner\Logs\Debug");
			if( !Directory.Exists(@"GigaSpawner\Logs\Errors") && Errors )
				Directory.CreateDirectory(@"GigaSpawner\Logs\Errors");
			if( !Directory.Exists(@"GigaSpawner\Logs\Mods") && Mods )
				Directory.CreateDirectory(@"GigaSpawner\Logs\Mods");
			string file = String.Format(@"GigaSpawner\Logs\Errors\Errors_{0:D}.log",DateTime.Today);
			if( Err == 2 )
				file = String.Format(@"GigaSpawner\Logs\Mods\GigaSpawner_{0:D}.log",DateTime.Today);
			if( Err == 3 )
				file = String.Format(@"GigaSpawner\Logs\Debug\Debug_{0:D}.log",DateTime.Today);
			using(StreamWriter w = File.AppendText(file))
			{
				if( NewEntry )
				{
					w.WriteLine("::::::::::::::::::::::::::::::::::::::::::::::::");
					w.WriteLine("Log Entry : ");
					w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
					w.WriteLine("");
				}
				w.WriteLine("\t{0}", msg);
				w.Flush();
				w.Close();
			}
		}
		
	}
}
