using System;
using System.Collections;
using Server;
using Server.Network;
using Server.GigaSpawners;

namespace Server.Gumps
{
	public class SpawnEntryMenu : Gump
	{
		private GigaSpawner m_gs;
		private ArrayList All_List = new ArrayList(GSMain.GSHash.Values);
		
		public SpawnEntryMenu( GigaSpawner gs ) : base(0, 0)
		{
			if( gs == null || gs.Deleted )
				return;
			m_gs = gs;
			AddPage(0);
			AddBackground(50, 50, 665, 255, 3000);
			AddHtml ( 50, 60, 665, 20, String.Format( "<center>Spawn Entry Menu - {0}</center>", gs.Name ), false, false );
			AddLabel(405, 90, 0, "Amount:");
			AddLabel(165, 90, 0, "Spawn Entries:");
			AddLabel(480, 90, 0, "Add items to backpack:");
			
			if ( gs.SpawnedAmount > 0 )
			{
				if( !gs.InContainer )
				{
					AddButton(160, 250,  4006, 4007,  -3, GumpButtonType.Reply, 0);
					AddLabel(195, 250, 0, "Bring to Home");
				}
				AddButton(55, 280,  4006, 4007,  -4, GumpButtonType.Reply, 0);
				AddLabel(90, 280, 0, "De-Spawn");
			}
			
			if ( gs.Active )
			{
				AddButton( 55, 250,  4006, 40076, -5, GumpButtonType.Reply, 0);
				AddLabel(90, 250, 0, "Re-Spawn");
			}
			
			AddButton(160, 275, gs.Active ? 10830 : 10850, gs.Active ? 10850 : 10830, -6, GumpButtonType.Reply, 0);
			AddLabel(195, 280, gs.Active ? 67 : 32,  gs.Active ? " Active" : "Inactive");
			
			AddButton(295, 250, 4030, 4031, -7, GumpButtonType.Reply, 0);// Spawner List
			AddLabel(330, 250, 0, "List");
			
			AddButton(465, 275, 238, 240, -1, GumpButtonType.Reply, 0);//Apply
			AddButton(540, 275, 247, 248, -2, GumpButtonType.Reply, 0);//OK
			AddButton(615, 275, 242, 241, 0, GumpButtonType.Reply, 0);//Cancel
			int pages = gs.SpawnList.Length / 5 + 2; // 5 listings per page and add 1 page in case they need to go the 5 entries
			int te = 1; int id = 0;
			for( int p = 0; p < pages; p++ )
			{
				AddPage(p + 1);
				for( int i = 0; i < 5; i++ )
				{
					id = i + 5 * p;
					AddLabel(65, 120 + 25 * i, 0, string.Format("Spawn Entry {0}:", 1 + id) );
					AddBackground(165, 120 + 25 * i, 186, 20, 9200);
					AddBackground(480, 120 + 25 * i, 175, 20, 9200);
					AddBackground(400, 120 + 25 * i, 70, 20, 9200);
					AddTextEntry(165, 120 + 25 * i, 183, 20, 0, te, (id < gs.SpawnList.Length) ? gs.SpawnList[id] : "");
					AddButton(360, 120 + 25 * i, 4011, 4013, te, GumpButtonType.Reply, 0);
					te++;
					AddTextEntry(405, 120 + 25 * i, 65, 20, 0, te, (id < gs.SpawnAmount.Length) ? gs.SpawnAmount[id].ToString() : "");
					te++;
					AddTextEntry(480, 120 + 25 * i, 175, 20, 0, te, (id < gs.ItemsList.Length) ? gs.ItemsList[id] : "");
					AddButton(665, 120 + 25 * i, 4011, 4013, te, GumpButtonType.Reply, 0);
					te++;
				}
				if( p != 0 )
					AddButton(295, 275, 4015, 4016, 0, GumpButtonType.Page, p );
				if( p + 1 != pages )
					AddButton(400, 275, 4006, 4007, 0, GumpButtonType.Page, p + 2);
				AddLabel(335, 275, 0, String.Format("{0:00} of {1:00}", p+1, pages ));
			}
		}
		
		public override void OnResponse( NetState sender, RelayInfo info )
		{
			Mobile from = sender.Mobile;
			GSMain.GigaGumpClose(from);
			int myIndex = 0;
			int gotopage = -1;
			switch( info.ButtonID )
			{
				case -3:
					{
						m_gs.BringToHome();
						goto case -100;
					}
				case -4:
					{
						m_gs.DeSpawn();
						goto case -100;
					}
				case -5:
					{
						m_gs.ReSpawn();
						goto case -100;
					}
				case -6:
					{
						if ( !m_gs.Active )
						{
							GSMain.LogWrite(String.Format("Account: {0} Character: {1} has activated spawner '{2}' at {3} Map: {4}",from.Account, from, m_gs.Name, m_gs.Location, m_gs.Map.ToString()),true,2);
							m_gs.Start();
						}
						else
						{
							m_gs.Stop();
						}
						m_gs.AddModifiedBy(from);
						goto case -100;
					}
				case -7:
					{
						//List
						try
						{
							myIndex = All_List.IndexOf( m_gs );
							gotopage = myIndex / 15;
						}
						catch ( Exception err )
						{
							Console.WriteLine( err.ToString() );
						}
						from.SendGump( new GigaListGump( from, All_List, gotopage >= 0 ? gotopage : 0 ) );
						return;
					}
				case -100:
					{
						from.SendGump( new SpawnEntryMenu(m_gs) );
						return;
					}
				case 0:
					{
						from.SendGump( new GigaMainGump(m_gs,from) );
						return;
					}
			}
			string[] order = new string[info.TextEntries.Length];
			foreach( TextRelay tr in info.TextEntries )
				order[tr.EntryID - 1] = tr.Text;
			
			if( info.ButtonID < 0 )
			{
				int len = 0;
				for (int i=0; i < order.Length; i+= 3) //See which is the first blank spawn entry
				{
					if( order[i] != "" )
						len = i + 3;
					else if ( order[i+1] != "" && m_gs.ActivityScaled )
						len = i + 3;
				}
				string[] se = new string[len/3];
				int[] am = new int[len/3];
				string[] items = new string[len/3];
				int amount = 0;
				for (int i=0; i < len; i+=3 )
				{
					try
					{
						amount = Utility.ToInt32( order[i+1] );
					}
					catch
					{
						amount = 0;
					}
					if( m_gs.ActivityScaled && !m_gs.LinearProgression )
						se[i/3] = order[0];
					else
						se[i/3] = order[i];
					am[i/3] = amount;
					items[i/3] = order[i+2];
				}
				m_gs.NewSpawnList( se, am, items, from );
				m_gs.AddModifiedBy(from);
				if( info.ButtonID == -1 )
					from.SendGump( new SpawnEntryMenu(m_gs) );
				else
					from.SendGump( new GigaMainGump(m_gs,from) );
				return;
			}
			int rem = 0;
			Math.DivRem(info.ButtonID - 1, 3, out rem);
			if( rem == 0 )
				from.SendGump( new SpawnEntryEditor( m_gs, order[info.ButtonID - 1], ((info.ButtonID - 1) / 3 ) ));
			else
				from.SendGump( new ItemEntryEditor( m_gs, order[info.ButtonID - 1], ((info.ButtonID - 1) / 3 ) ));
		}
	}
}
