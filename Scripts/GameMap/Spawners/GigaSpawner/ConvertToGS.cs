using System;
using System.IO;
using System.Collections;
using Server;
using Server.Mobiles;
using Server.GigaSpawners;
using Server.Items;
using Server.Commands;
using Server.Gumps;

namespace Server.Scripts.Commands
{
	
	public class Gigatest
	{
		
		public static void Initialize()
		{
			CommandSystem.Register( "ConvertToGS", AccessLevel.Player, new CommandEventHandler( ConvertToGS_OnCommand ) );
		}
		
		[Usage( "ConvertToGS <spawner system> <filename>" )]
		[Description( "Converts all spawners from selected system to GigaSpawners" )]
		private static void ConvertToGS_OnCommand( CommandEventArgs e )
		{
			Mobile from = e.Mobile;
			if ( e.Length != 2 && File.Exists( e.GetString(1).ToLower() ) )
			{
				from.SendMessage( 0, "You must supply a spawner system and filename to convert spawners" );
				return;
			}
			if( !Directory.Exists(@"Export\GigaSpawner") )
				Directory.CreateDirectory(@"Export\GigaSpawner");
			string type = e.GetString(0).ToLower();
			string filename = e.GetString(1).ToLower();
			if ( File.Exists( Path.Combine( @"Export\GigaSpawner", filename ) ) || File.Exists( Path.Combine( @"Export\GigaSpawner", filename + ".gsw" ) ) )
			{
				from.SendMessage( 0, "That file already exist, try again using a different name." );
				return;
			}

			if ( type == "spawner" )
			{
				DistroToGSW( from, filename );
			}
			else if ( type == "ps" )
			{
				MapToGSW( from, filename );
			}
			else
			{
				from.SendMessage("That type of spawner is not supported at this time.");
				return;
			}
		}
		public static void DistroToGSW( Mobile from, string filename )
		{
			ArrayList temp = new ArrayList();
			foreach( Item item in World.Items.Values )
			{
				if( item is Spawner )
					temp.Add( (Spawner)item );
			}
			if( temp.Count == 0 )
			{
				from.SendMessage( "There aren't any Distro Spwners to be converted." );
				return;
			}

			filename = filename.Replace( ".gsw", "" );
			if( !Directory.Exists(@"Export\GigaSpawner") )
                Directory.CreateDirectory(@"Export\GigaSpawner");
            string GSPath = Path.Combine(@"Export\GigaSpawner", filename + ".gsw");
			if( File.Exists( GSPath ) )
				File.Delete( GSPath );
			DateTime begin = DateTime.Now;
			string spawnentry = "";
			int len = 0;

			from.SendMessage( "Exporting Distro Spawners to {0}...", filename );
			#region Distro Spawner Conversion
			Console.Write("Saving...");
			bool world = true;
			len = temp.Count;
            if (!Directory.Exists(@"Export\GigaSpawner"))
                Directory.CreateDirectory(@"Export\GigaSpawner");
			using(FileStream m_FileStream = new FileStream( GSPath, FileMode.OpenOrCreate, FileAccess.Write ))
			{
				BinaryFileWriter writer = new BinaryFileWriter( m_FileStream,true);
				// Global save/load vars
				writer.Write((bool)world);
				writer.Write((int)len);
				writer.Write( (int) 2 ); // version
				writer.Write( System.Guid.NewGuid().ToString());
				// Spawner specific vars
				foreach( Spawner spawner in temp )
				{
					spawnentry = "";
					writer.Write((string)spawner.Name);
					writer.Write((int)10);
					writer.Write((int)spawner.HomeRange);
					writer.Write((int)10);
					writer.Write((int)5);
					writer.Write((int)spawner.Team);
					writer.Write( spawner.WayPoint );
					writer.Write( new Rectangle2D(0,0,0,0) );
					writer.Write( "" );
					writer.Write( spawner.MinDelay );
					writer.Write( spawner.MaxDelay );
 
                    for (int i = 0; i < spawner.SpawnNames.Count; i++)
                        spawnentry = spawnentry + (string)spawner.SpawnNames[i] + ';';
 
					writer.Write( 1 );
					writer.Write( spawnentry );
					writer.Write( spawner.Count );
					writer.Write( "" );
				
					writer.Write( 0 );
					writer.Write((Point3D)spawner.Location);
					writer.Write((Map)spawner.Map);
				}
				writer.Close();
				m_FileStream.Close();
			}
			TimeSpan dt1 = DateTime.Now - begin;
			from.SendMessage( "Done exporting {0} Distro Spawners to {1}.gsw. Time elapsed {2}m{3}s.{4}ms", len.ToString(), filename, dt1.Minutes , dt1.Seconds , dt1.Milliseconds );
			Console.WriteLine("Saved {3} Distro Spawners in {0}m{1}s.{2}ms.", dt1.Minutes , dt1.Seconds , dt1.Milliseconds, len.ToString() );
			from.SendGump( new GigaSaveGump( from, "Select Load World to load your converted spawners", filename + ".gsw" ) );
			#endregion
		}

		#region Creates .gsw form Neruns .map
		public static void MapToGSW( Mobile from, string filename )
		{
			filename = filename.Replace( ".map", "" );
            if (!Directory.Exists(@"Export\GigaSpawner"))
                Directory.CreateDirectory(@"Export\GigaSpawner");
            string PSPath = Path.Combine(@"Export\GigaSpawner", filename + ".map");
            string GSPath = Path.Combine(@"Export\GigaSpawner", filename + ".gsw");

			if( !File.Exists( PSPath ) )
			{
				from.SendMessage( "The file {0} does not exist", PSPath );
				return;
			}

			from.SendMessage( "Exporting Premium Spawners to {0}...", filename );

			if( File.Exists( GSPath ) )
				File.Delete( GSPath );

			DateTime begin = DateTime.Now;
			bool world = true;
			int gslen = 0;
			Map m_map = Map.Internal;
			WayPoint waypoint = null;
			Point3D loc;
			int overridemap = -1;
			int overridemintime = -1;
			int overridemaxtime = -1;
			int spawnermap;

			using ( StreamReader ip = new StreamReader( PSPath ) )
			{
				string line;
				using(FileStream m_FileStream = new FileStream( GSPath, FileMode.OpenOrCreate, FileAccess.Write ))
				{
					BinaryFileWriter writer = new BinaryFileWriter( m_FileStream,true);
					// Global save/load vars
					writer.Write((bool)world); //World Spawn
					long lenpos = writer.Position;// Get file position to fill in later
					writer.Write( 0 ); // Number of Spawners in the file
					writer.Write( (int) 2 ); // version
					writer.Write( System.Guid.NewGuid().ToString());// UID
					while ( (line = ip.ReadLine()) != null )
					{
						if( line.IndexOf( ":" ) > 0 )
							line = line.Replace( ':', ';' );
						if( line.StartsWith( "+" ) )
							line = line.Replace( "+", "+ " );
						if( line.StartsWith( "+ bird" ) )
							line = line.Replace( "+ bird", "+ birds" );
						
						string[] split = line.Split( ' ' );
						if ( split.Length == 2  )
						{
							if ( split[0].ToLower() == "overridemap" )
								overridemap = int.Parse( split[1] );
							
							if ( split[0].ToLower() == "overridemintime" )
								overridemintime = int.Parse( split[1] );
							
							if ( split[0].ToLower() == "overridemaxtime" )
								overridemaxtime = int.Parse( split[1] );
						}
						else if ( ( split.Length == 11 || split.Length == 12 ) && ( line.StartsWith( "*" ) || line.StartsWith( "+" ) ) )
						{
							TimeSpan ts_min = TimeSpan.Zero;
							TimeSpan ts_max = TimeSpan.Zero;
							
							if ( overridemap == -1 )
								spawnermap = int.Parse( split[5] );
							else
								spawnermap = overridemap;
							
							switch( spawnermap )
							{
								//Case 0 loads spawners in fel and trammel
								case 0: case 1:
									{
										m_map = Map.Felucca;
										break;
									}
								case 2:
									{
										m_map = Map.Trammel;
										break;
									}
								case 3:
									{
										m_map = Map.Ilshenar;
										break;
									}
								case 4:
									{
										m_map = Map.Malas;
										break;
									}
								case 5:
									{
										try
										{
											m_map = Map.Parse("Tokuno");
										}
										catch { }
										break;
									}
							}
							
							switch( split[0].ToLower() )
							{
								case "##"://Comment
									break;
									
								case "*": case "+": //Spawner
								{
									loc = new Point3D( int.Parse( split[2] ), int.Parse( split[3] ), int.Parse( split[4] ) );
									if ( overridemintime == -1 )
										ts_min = TimeSpan.FromMinutes( Utility.ToDouble( split[6] ) );
									else
										ts_min = TimeSpan.FromMinutes(overridemintime );
									
									if ( overridemaxtime == -1 )
										ts_max = TimeSpan.FromMinutes( Utility.ToDouble( split[7] ) );
									else
										ts_min = TimeSpan.FromMinutes( overridemaxtime );
									writer.Write( (string)"PStoGSConvert" );//Spawner Name
									writer.Write( int.Parse( split[9] ) );//Spawn Range
									writer.Write( int.Parse( split[8] ) );// HomeRange
									writer.Write((int)10);// SPTriggerRange
									writer.Write((int)5);// WalkTriggerRange
									writer.Write((int)0);// Team
									writer.Write( waypoint );// WayPoint
									writer.Write( new Rectangle2D(0,0,0,0) );// SpawnArea
									writer.Write( "" );// Keyword
									writer.Write( ts_min );// MinDelay
									writer.Write( ts_max );// MaxDelay
									writer.Write( 1 );// 
									writer.Write( (string)split[1] );// Spawnentry
									
									if ( split.Length == 12 )
										writer.Write( int.Parse( split[11] ) );// Amount
									else if ( split.Length == 11 )
										writer.Write( int.Parse( split[10] ) );// Amount
									
									writer.Write( "" );// ItemsList
									writer.Write( 0 );// Flags
									writer.Write((Point3D)loc );// Location
									writer.Write( m_map );// Map
									gslen ++;
									#region Loading Spawners in Tram if a 0 map was specified
									if ( spawnermap == 0 )
									{
										loc = new Point3D( int.Parse( split[2] ), int.Parse( split[3] ), int.Parse( split[4] ) );
										if ( overridemintime == -1 )
											ts_min = TimeSpan.FromMinutes( Utility.ToDouble( split[6] ) );
										else
											ts_min = TimeSpan.FromMinutes(overridemintime );
										
										if ( overridemaxtime == -1 )
											ts_max = TimeSpan.FromMinutes( Utility.ToDouble( split[7] ) );
										else
											ts_min = TimeSpan.FromMinutes( overridemaxtime );
										writer.Write( (string)"PStoGSConvert" );//Spawner Name
										writer.Write( int.Parse( split[9] ) );//Spawn Range
										writer.Write( int.Parse( split[8] ) );// HomeRange
										writer.Write((int)10);// SPTriggerRange
										writer.Write((int)5);// WalkTriggerRange
										writer.Write((int)0);// Team
										writer.Write( waypoint );// WayPoint
										writer.Write( new Rectangle2D(0,0,0,0) );// SpawnArea
										writer.Write( "" );// Keyword
										writer.Write( ts_min );// MinDelay
										writer.Write( ts_max );// MaxDelay
										writer.Write( 1 );// 
										writer.Write( (string)split[1] );// Spawnentry
										
										if ( split.Length == 12 )
											writer.Write( int.Parse( split[11] ) );// Amount
										else if ( split.Length == 11 )
											writer.Write( int.Parse( split[10] ) );// Amount
										
										writer.Write( "" );// ItemsList
										writer.Write( 0 );// Flags
										writer.Write((Point3D)loc );// Location
										writer.Write( Map.Trammel );// Map
										gslen ++;
									}
									#endregion
									break;
								}
							}
						}
					}
					writer.UnderlyingStream.Seek(lenpos,SeekOrigin.Begin);
					writer.Write(gslen);
					writer.Close();
					m_FileStream.Close();
				}
				TimeSpan dt1 = DateTime.Now - begin;
				from.SendMessage( "Done converting {0} Premium Spawners to {1}.gsw. Time elapsed {2}m{3}s.{4}ms", gslen.ToString(), filename, dt1.Minutes , dt1.Seconds , dt1.Milliseconds );
				Console.WriteLine("Saved {3} Premium Spawners in {0}m{1}s.{2}ms.", dt1.Minutes , dt1.Seconds , dt1.Milliseconds, gslen.ToString() );
			}
			from.SendGump(new GigaSaveGump( from, "Select Load World to load your converted spawners", filename + ".gsw" ) );
		}
		#endregion
	}
}