using System;
using System.IO;
using System.Collections;
using Server;
using Server.Network;
using Server.GigaSpawners;

namespace Server.Gumps
{
	public class GigaSaveGump : Gump
	{
		private ArrayList All_List = new ArrayList(GSMain.GSHash.Values);
		
		public GigaSaveGump( Mobile from ) : this( from, "" )
		{
		}
		public GigaSaveGump( Mobile from, string message ) : this( from, message, "" )
		{
		}
		
		public GigaSaveGump( Mobile from, string message, string filename ) : base(50, 50)
		{
			AddBackground(50, 50, 435, 370, 3000);
			AddBackground(150, 245, 200, 20, 9300);
			AddHtml( 80, 280, 380, 100, message, true, true);
			AddButton(80, 105, 2445, 2445, 1, GumpButtonType.Reply, 0);// Save Map
			AddButton(80, 155, 2445, 2445, 3, GumpButtonType.Reply, 0);// Save World
			AddButton(80, 205, 2445, 2445, 6, GumpButtonType.Reply, 0);// Save Area
			AddButton(240, 105, 2445, 2445, 2, GumpButtonType.Reply, 0);// Load Map
			AddButton(240, 155, 2445, 2445, 4, GumpButtonType.Reply, 0);// Load World
			AddButton(240, 205, 2445, 2445, 5, GumpButtonType.Reply, 0);// Unload File
			AddButton(370, 245, 4030, 4031, -1, GumpButtonType.Reply, 0);// File List
			AddHtml( 50, 60, 430, 20, "<center>GigaSpawner Load/Save</center>", false, false);
			AddHtml( 80, 105, 105, 20, "<center>Save Map</center>", false, false);
			AddHtml( 80, 155, 105, 20, "<center>Save World</center>", false, false);
			AddHtml( 80, 205, 105, 20, "<center>Save Area</center>", false, false);
			AddHtml( 240, 105, 105, 20, "<center>Load Map</center>", false, false);
			AddHtml( 240, 155, 105, 20, "<center>Load World</center>", false, false);
			AddHtml( 240, 205, 105, 20, "<center>Unload File</center>", false, false);
			AddRadio(380, 105, 210, 211, (from.Map==Map.Felucca), 0);
			AddRadio(380, 130, 210, 211, (from.Map==Map.Trammel), 1);
			AddRadio(380, 155, 210, 211, (from.Map==Map.Ilshenar), 2);
			AddRadio(380, 180, 210, 211, (from.Map==Map.Malas), 3);
			AddRadio(380, 205, 210, 211, (from.Map==Map.Tokuno), 4);
			AddLabel(405, 105, 0, "Felucca");
			AddLabel(405, 130, 0, "Trammel");
			AddLabel(405, 155, 0, "Ilshenar");
			AddLabel(405, 180, 0, "Malas");
			AddLabel(405, 205, 0, "Tokuno");
			AddLabel(80, 245, 0, "File Name:");
			AddLabel(405, 245, 0, "File List");
			AddTextEntry(150, 245, 200, 20, 0, 0, filename == "" ? from.Map.ToString() : filename );this.AddPage(0);
			AddButton(80, 390, 4030, 4031, -2, GumpButtonType.Reply, 0);// Spawner List
			AddLabel(115, 390, 0, "GigaSpawner List");
		}
		
		public override void OnResponse( NetState sender, RelayInfo info )
		{
			Mobile from = sender.Mobile;
			GSMain.GigaGumpClose(from);
			Map map = from.Map;
			if (info.IsSwitched(0))
				map = Map.Felucca;
			if (info.IsSwitched(1))
				map = Map.Trammel;
			if (info.IsSwitched(2))
				map = Map.Ilshenar;
			if (info.IsSwitched(3))
				map = Map.Malas;
			if (info.IsSwitched(4))
				map = Map.Tokuno;
			
			switch( info.ButtonID )
			{
				case -2:// GS List Gump
					{
						from.SendGump( new GigaListGump( from, All_List, 0 ) );
						break;
					}
				case -1:// File List Gump
					{
						from.SendGump( new GSFileList(from) );
						break;
					}
				case 1:// Map Export
					{
						GSMain.Backup(map, info.TextEntries[0].Text, from, false);
						break;
					}
				case 2:// Map Import
					{
						GSMain.LoadBackup(map, info.TextEntries[0].Text, from, false);
						break;
					}
				case 3:// Global Export
					{
						GSMain.Backup(null, info.TextEntries[0].Text, from, false);
						break;
					}
				case 4:// Global Import
					{
						GSMain.LoadBackup(null, info.TextEntries[0].Text, from, false);
						break;
					}
				case 5:// UnLoad file
					{
						GSMain.UnLoadSpawnFile(info.TextEntries[0].Text, from, false);
						break;
					}
				case 6:// Export Area
					{
						BoundingBoxPicker.Begin( from, new BoundingBoxCallback( AreaSave ), info.TextEntries[0].Text + ".bin" );
						break;
					}
			}
		}
		
		private static void AreaSave( Mobile from, Map map, Point3D start, Point3D end, object state )
		{
			GSMain.Backup(map, (string)state, from, false, new Rectangle2D(start,end),true);
		}
	}
	public class GSComfirmGump : Gump
	{
		private int m_type;
		private Map m_map;
		private string m_file;
		private object retobj;
		
		public GSComfirmGump(int type, int amount, string file, Map map ) : this( type, amount, file, map, null )
		{
		}
		
		public GSComfirmGump(int type, int amount, string file, Map map, object gumpobj ) : base(0, 0)
		{
			m_type = type;
			m_map = map;
			m_file = file;
			retobj = gumpobj;
			AddBackground(50, 50, 280, 200, 3000);
			AddButton(90, 210, 247, 248, 1, GumpButtonType.Reply, 0);
			AddButton(215, 210, 242, 241, 0, GumpButtonType.Reply, 0);
			string msg = "";
			switch( type )
			{
				case 1:
					{
				msg = String.Format("You are about to import {0} GigaSpawners. Do you want to continue?",amount.ToString());
						break;
					}
				case 2:
					{
				msg = String.Format("You are about to remove {0} GigaSpawners. Do you want to continue?",amount.ToString());
						break;
					}
				case 3:
					{
				msg = String.Format("The file {0} already exists. Do you want to overwrite?",file);
						break;
					}
				case 4:
					{
						msg = String.Format("You are about to delete the template file {0}. Do you want to continue?",file);
						break;
					}
			}
			AddHtml( 70, 90, 227, 109, msg, false, false);
			AddHtml( 50, 60, 280, 20, "<center>Confirm</center>", false, false);
		}
		
		public override void OnResponse( NetState sender, RelayInfo info )
		{
			Mobile from = sender.Mobile;
			GSMain.GigaGumpClose(from);
			string message = "";
			if( info.ButtonID == 1 )
			{
				switch( m_type )
				{
					case 1:
				{
					GSMain.LoadBackup(m_map, m_file, from, true);
					GSMain.LogWrite(String.Format("Account: {0} Character: {1} has loaded {2}", from.AccessLevel,from,m_file), false,2);
							break;
				}
					case 2:
				{
					GSMain.UnLoadSpawnFile(m_file, from, true);
					GSMain.LogWrite(String.Format("Account: {0} Character: {1} has unloaded {2}", from.AccessLevel,from,m_file), false,2);
							break;
				}
					case 3:
				{
					GSMain.Backup(m_map, m_file, from, true);
					GSMain.LogWrite(String.Format("Account: {0} Character: {1} has overwritten {2}", from.AccessLevel,from,m_file), false,2);
							break;
						}
					case 4:
						{
							GSTemplateBook gstb = (GSTemplateBook)retobj;
							try
							{
								File.Delete(@"GigaSpawner\" + gstb.FileName + ".gst");
								File.Delete(@"GigaSpawner\" + gstb.FileName + ".idx");
								gstb.Clear();
							}
							catch( Exception err )
							{
								from.SendMessage("Couldn't delete file. Check error log for error.");
								GSMain.LogWrite(String.Format("Error Deleting file {0}. Error message: {1}", gstb.FileName, err.Message),true);
							}
							finally
							{
								from.SendGump(new GSTemplateGump(gstb,from));
							}
							break;
						}
				}
				return;
			}
			if( m_type == 4 )
				from.SendGump( new GSTemplateGump( (GSTemplateBook)retobj, from ));
			else
			from.SendGump( new GigaSaveGump(from,message) );
		}
	}
	
	public class GSFileList : Gump
	{
		private string[] files;
		private int type;
		private object retobj;
		
		public GSFileList(Mobile from) : this( from, "" )
		{
		}
		public GSFileList(Mobile from, string ext) : this( from, ext, 1, null )
		{
		}
		
		public GSFileList(Mobile from, string ext, int gumptype, object gumpobj) : base( 0,0 )
		{
			type = gumptype;
			retobj = gumpobj;
			if( ext == "" )
				ext = "*.bin;*.gsm;*.gsw";
			files = GetFiles(ext);
			if( files.Length == 0 && gumptype == 1 )
			{
				GSMain.GigaGumpClose(from);
				from.SendGump( new GigaSaveGump(from, "No Files found to list.") );
				return;
			}
			if( files.Length == 0 && gumptype == 2 )
			{
				GSMain.GigaGumpClose(from);
				from.SendMessage("No file to list...Specify a file name to create it.");
				from.SendGump( new GSTemplateGump((GSTemplateBook)retobj, from ));
				return;
			}
			for( int i = 0; i < files.Length; i++ )
				files[i] = files[i].ToLower().Replace("gigaspawner\\", "");
			AddPage(0);
			AddBackground(50, 50, 300, 220, 3000);
			AddHtml( 50, 50, 300, 20, "<center>File List</center>", false, false);
			AddBackground(60, 80, 280, 150, 3500);
			int pages = files.Length / 5 + 1;
			for( int i = 0; i < pages; i++ )
			{
				AddPage(i+1);
				for( int x = 0; x < 5 && ( (x + i * 5) < files.Length ); x++ )
				{
					AddButton(120, 95 + x * 25, 1204, 1204, (x + 1 + i * 5), GumpButtonType.Reply, 0);
					AddBackground(85, 95 + x * 25, 230, 20, 9200);
					AddHtml( 85, 95 + x * 25, 230, 20, String.Format( "<center>{0}</center>", files[(x + i  * 5)]), false, false);
				}
				if( i != 0 )
					AddButton(100, 235, 4014, 4015, 0, GumpButtonType.Page, i);
				if( i != pages - 1 )
					AddButton(250, 235, 4005, 4006, 0, GumpButtonType.Page, i + 2);
			}
		}
		
		public override void OnResponse( NetState sender, RelayInfo info )
		{
			Mobile from = sender.Mobile;
			GSMain.GigaGumpClose(from);
			if( info.ButtonID == 0 )
			{
				if( type == 1 ) //SaveGump
				{
					from.SendGump( new GigaSaveGump(from) );
					return;
				}
				else if( type == 2 ) //Template Gump
				{
					if( retobj is GSTemplateBook )
						from.SendGump( new GSTemplateGump((GSTemplateBook)retobj, from ));
					return;
				}
			}
			if( type == 1 ) //SaveGump
			{
				from.SendGump(new GigaSaveGump( from, "", files[info.ButtonID - 1] ));
				return;
			}
			else if( type == 2 ) //Template Gump
			{
				if( retobj is GSTemplateBook )
				{
					GSTemplateBook gstb = (GSTemplateBook)retobj;
					gstb.FileName = files[info.ButtonID - 1].Replace(".gst","");
					GSTemplate.GSTempLoad( gstb );
					from.SendGump( new GSTemplateGump(gstb, from ));
				}
				return;
			}
		}
		
		private string[] GetFiles(string extension)
		{
			if( !Directory.Exists("GigaSpawner") )
				Directory.CreateDirectory("GigaSpawner");
			ArrayList files = new ArrayList();
			string[] exts = extension.Split(';');
			foreach( string ext in exts )
				files.AddRange(Directory.GetFiles("GigaSpawner", ext));
			
			return (string[])files.ToArray(typeof(string));
		}
	}
}
