/* Created by Hammerhand*/

using System;
using Server;
using Server.Items;
using Server.Targeting;
using Server.Network;
using Server.Commands;
using Server.Engines.Harvest;
using Server.Mobiles;
using Server.Regions;
using Server.Gumps;

      namespace Server.Gumps
    {
       public class OldArcheologistGump : Gump 
       { 
       public static void Initialize()
       {
           CommandSystem.Register("OldArcheologist", AccessLevel.GameMaster, new CommandEventHandler(OldArcheologistGump_OnCommand)); 
    }
      private static void OldArcheologistGump_OnCommand( CommandEventArgs e ) 
    {
      e.Mobile.SendGump( new OldArcheologistGump( e.Mobile ) ); }
           public OldArcheologistGump(Mobile owner)
               : base(50, 50) 
    {

          AddPage( 0 );AddImageTiled(  54, 33, 369, 400, 2624 );
          AddAlphaRegion( 54, 33, 369, 400 );
          AddImageTiled( 416, 39, 44, 389, 203 );

          AddImage( 97, 49, 9005 );
          AddImageTiled( 58, 39, 29, 390, 10460 );
          AddImageTiled( 412, 37, 31, 389, 10460 );
          AddLabel( 140, 60, 0x34, "The old Archeologist" );

          AddHtml( 107, 140, 300, 230, " < BODY > " + 
"<BASEFONT COLOR=YELLOW>Greetings my friend, I am John Aubrey, an Archeologist by trade.<BR>" +
"<BASEFONT COLOR=YELLOW>Recently on a dig, I was attacked by a band of grave robbers and they stole everything..<BR>" +
"<BASEFONT COLOR=YELLOW>By now the artifacts will be long gone, but if you can, I need my logbook returned. <BR>" +
"<BASEFONT COLOR=YELLOW>In it are sketches, descriptions and measurements<BR>" +
"<BASEFONT COLOR=YELLOW>of my discoveries and the only documentation of the discoveries.<BR>" +
"<BASEFONT COLOR=YELLOW>Please, find those fiends and rid the world of them. If you can recover my logbook<BR>" +
"<BASEFONT COLOR=YELLOW>and return it to me, I'd be most grateful. In return, I will give you this pack.. <BR>" +
"<BASEFONT COLOR=YELLOW>Inside is a book to learn the trade and Archeological Brushes so that maybe you<BR>" +
"<BASEFONT COLOR=YELLOW>can search that area for more relics of the past. I'm getting too old<BR>" +
"<BASEFONT COLOR=YELLOW>for it now and I know there are more artifacts out there. Please help me.<BR>" +
"</BODY>", false, true);

          AddImage( 430, 9, 10441);
          AddImageTiled( 40, 38, 17, 391, 9263 );
          AddImage( 6, 25, 10421 );
          AddImage( 34, 12, 10420 );
          AddImageTiled( 94, 25, 342, 15, 10304 );
          AddImageTiled( 40, 427, 415, 16, 10304 );
          AddImage( -10, 314, 10402 );
          AddImage( 56, 150, 10411 );
          AddImage( 155, 120, 2103 );
          AddImage( 136, 84, 96 );
          AddButton( 225, 390, 0xF7, 0xF8, 0, GumpButtonType.Reply, 0 ); }

      public override void OnResponse( NetState state, RelayInfo info ){ Mobile from = state.Mobile;
          switch ( info.ButtonID ) { case 0:{ break; 
          }
        }
      }
    }
 }
