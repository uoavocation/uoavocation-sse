using System;
using System.Collections;
using Server;
using Server.Spells;
using Server.Mobiles;
using Server.Network;

namespace Server.Engines.Doom
{
    public class LeverPuzzleStatue : Item
    {
        private LeverPuzzleController m_Controller;

        public LeverPuzzleStatue(int[] dat, LeverPuzzleController controller)
            : base(dat[0])
        {
            m_Controller = controller;
            Hue = 0x44E;
            Movable = false;
        }
        public override void OnAfterDelete()
        {
            if (m_Controller != null && !m_Controller.Deleted)
                m_Controller.Delete();
        }
        public LeverPuzzleStatue(Serial serial)
            : base(serial)
        {
        }
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
            writer.Write(m_Controller);
        }
        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
            m_Controller = reader.ReadItem() as LeverPuzzleController;
        }
    }
}