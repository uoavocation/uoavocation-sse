/* Created by Hammerhand*/

using System;
using Server;
using Server.Items;
using Server.Targeting;
using Server.Network;
using Server.Commands;
using Server.Engines.Harvest;
using Server.Mobiles;
using Server.Regions;
using Server.Gumps;

namespace Server.Items
{
	public class ArcheologistsPack : Backpack
	{
		public override string DefaultName
		{
			get { return "an Archeologists Pack"; }
		}

		[Constructable]
		public ArcheologistsPack() : this( 1 )
		{
			Movable = true;
			Hue = 1072;
		}

		[Constructable]
		public ArcheologistsPack( int amount )
		{
            DropItem(new ArcheologistsBrush());
            DropItem(new ArcheologistsBrush());
            DropItem(new ArcheologyBook());

		}

        public ArcheologistsPack(Serial serial)
            : base(serial)
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}