using System;
using Server;

namespace Server.Items
{
	public class FourLeafClover : WarMace
	{
		public override int InitMinHits{ get{ return 255; } }
		public override int InitMaxHits{ get{ return 255; } }

		[Constructable]
		public FourLeafClover()
		{
			Hue = 1436;
			Name = "leprechaun's four leaf clover";
			Attributes.WeaponSpeed = 30;
			Attributes.WeaponDamage = 50;
			Attributes.Luck = 1000;
			WeaponAttributes.UseBestSkill = 1;
			LootType = LootType.Blessed;
			Identified = true;
		}

		public FourLeafClover( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 );
		}
		
		public override void Deserialize(GenericReader reader)
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}
}