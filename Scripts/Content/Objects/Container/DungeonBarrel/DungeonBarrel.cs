using System;
using System.Collections;
using Server.Multis;

namespace Server.Items
{
    public class DungeonBarrel : Container, IChopable // Trapable Container
	{
		//public override int LabelNumber{ get{ return 1041064; } } // a trash barrel
        
		public override int DefaultMaxWeight{ get{ return 0; } } // A value of 0 signals unlimited weight

        //try to keep it closed
        public override void OnDoubleClick( Mobile from)
        {
            from.SendLocalizedMessage(501747); // It appears to be locked.
        }

		public override int DefaultGumpID{ get{ return 0x3E; } }
		public override int DefaultDropSound{ get{ return 0x42; } }

		public override Rectangle2D Bounds
		{
			get{ return new Rectangle2D( 33, 36, 109, 112 ); }
		}

		[Constructable]
		public DungeonBarrel() : base( 0xFAE) //0xE77 )  
		{
			Name = "Sealed Barrel";  //change the name
            Hue = 0x02c;             //set the color
            //Locked = true; 
            Movable = false;
		}

        public DungeonBarrel(Serial serial) : base(serial)
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

 		}

		public void OnChop( Mobile from )
        //public override void OnDoubleClick(Mobile from) I'd rather had this work, oh well.
			{
                Effects.SendLocationEffect( Location, Map, 0x3728, 20, 10); //smoke or dust
                Effects.PlaySound( Location, Map, 0x11C );
                
                switch (Utility.Random(10))  //picks one of the following
                {
                    case 0:
					    Effects.SendLocationEffect( from, from.Map, 0x113A, 20, 10 ); //Posion Player
					    from.PlaySound( 0x231 );
					    from.ApplyPoison( from, Poison.Regular );
					    break;
                    case 1:
                        Effects.SendLocationEffect(from, from.Map, 0x3709, 30);//Burn Player
                        from.PlaySound(0x54);
                        AOS.Damage(from, from, Utility.RandomMinMax(10, 40), 0, 100, 0, 0, 0);
                        break;
                    case 2:
                        new BarrelLid().MoveToWorld(Location, Map);
                        break;
                    case 3:
                        new BarrelHoops().MoveToWorld(Location, Map);
                        break;
                    case 4:
                        new BarrelStaves().MoveToWorld(Location, Map);
                        break;
                    case 5:
                        Gold g = new Gold(Utility.Random(500)); //Random amount of gold 0 - 500
                        g.MoveToWorld(Location, Map);
                        break;
                    case 6:
                        new CurePotion().MoveToWorld(Location, Map);
                        break;
                    case 7:
                        new GreaterCurePotion().MoveToWorld(Location, Map);
                        break;
                    case 8:
                        new HealPotion().MoveToWorld(Location, Map);
                        break;
                    case 9:
                        new GreaterHealPotion().MoveToWorld(Location, Map);
                        break;
                }
                Destroy();				
			}
    }
}
	