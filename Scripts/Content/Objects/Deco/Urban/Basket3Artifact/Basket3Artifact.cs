using System;
using Server;

namespace Server.Items
{
    public class Basket3WestArtifact : BaseDecorationContainerArtifact
    {
        public override int ArtifactRarity { get { return 1; } }

        [Constructable]
        public Basket3WestArtifact()
            : base(0x24D9)
        {
        }

        public Basket3WestArtifact(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.WriteEncodedInt(0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadEncodedInt();
        }
    }

    public class Basket3NorthArtifact : BaseDecorationContainerArtifact
    {
        public override int ArtifactRarity { get { return 1; } }

        [Constructable]
        public Basket3NorthArtifact()
            : base(0x24DA)
        {
        }

        public Basket3NorthArtifact(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.WriteEncodedInt(0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadEncodedInt();
        }
    }
}
