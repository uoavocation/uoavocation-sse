using System;
using Server;
using Server.Misc;
using Server.Items;
using Server.Gumps;
using Server.Network;
using Server.Commands;
using Server.Accounting;
using Server.Mobiles;
using Server.Engines.BulkOrders;

namespace Server.Gumps
{
    public class CreateBODGump : Gump
	{
		private Type m_ItemType;
		private bool m_RequireExceptional;
		private BODType m_DeedType;
		private BulkMaterialType m_Material;
		private int m_AmountCur, m_AmountMax;
		private int m_Number;
		private int m_Graphic;
	
		
		public static void Initialize()
		{
			CommandSystem.Register( "CreateBOD", AccessLevel.GameMaster, new CommandEventHandler( CreateBOD_OnCommand ) );
			
		}

		[Usage( "CreateBOD" )]
		[Description( "Create Specific BlackSmith BOD" )]
        private static void CreateBOD_OnCommand(CommandEventArgs e)
		{

			Mobile from = e.Mobile;
            from.CloseGump(typeof(CreateBODGump));
            from.SendGump(new CreateBODGump(from));
           
		}
		
		public CreateBODGump(Mobile from) : base(0, 0)
		{
			
			Closable=true;
			Disposable=true;
			Dragable=true;
			Resizable=false;
			AddPage(1);
			
			AddBackground(100, 50, 340, 250, 2600);
			AddLabel(194, 68, 0, "Smith / Tailor BOD Maker");

			AddLabel(179, 115, 0, "Smithy Items");
			AddLabel(273, 115, 0, "Tailoring Items");
			AddBackground(183, 142, 63, 60, 9250);
			AddBackground(281, 142, 63, 60, 9250);
			AddItem(195, 165, 5091);
			AddItem(285, 162, 3997);
			AddButton(193, 215, 2141, 2142, 0, GumpButtonType.Page, 10);//smithy
			AddButton(291, 215, 2141, 2142, 0, GumpButtonType.Page, 20);//tailoring
			
			AddPage(10); // Types of BlackSmithy
			AddBackground(100, 50, 250, 460, 2600);
			AddLabel(150, 70, 0, @"Pick the type of BOD"); 
			AddLabel(210, 103, 0, @"Ringmail");
			AddButton(170, 100, 2152, 2153, 0, GumpButtonType.Page, 101);
			AddLabel(210, 143, 0, @"Chainmail");
			AddButton(170, 140, 2152, 2153, 0, GumpButtonType.Page, 102);
			AddLabel(210, 183, 0, @"Plate");
			AddButton(170, 180, 2152, 2153, 0, GumpButtonType.Page, 103);
			AddLabel(210, 223, 0, @"Shields");
			AddButton(170, 220, 2152, 2153, 0, GumpButtonType.Page, 104);
			AddLabel(210, 263, 0, @"Axes");
			AddButton(170, 260, 2152, 2153, 0, GumpButtonType.Page, 105);
			AddLabel(210, 303, 0, @"Maces");
			AddButton(170, 300, 2152, 2153, 0, GumpButtonType.Page, 106);
			AddLabel(210, 343, 0, @"Swords ");
			AddButton(170, 340, 2152, 2153, 0, GumpButtonType.Page, 107);
			AddLabel(210, 383, 0, @"Fencin & Polearms ");
			AddButton(170, 380, 2152, 2153, 0, GumpButtonType.Page, 108);

			
			AddPage(101);//RingMail
			AddBackground(100, 50, 227, 257, 9250);
			AddLabel(130, 70, 0, @"Pick Ringmail Armor Piece");
			AddButton(130, 100, 2152, 2153, 0, GumpButtonType.Page, 1000);
			AddItem(180, 100, 5099);
			AddLabel(250, 102, 0, @"Gloves");
			AddButton(130, 140, 2152, 2153, 0, GumpButtonType.Page, 1001);
			AddItem(180, 135, 5104);
			AddLabel(250, 140, 0, @"Leggings");
			AddButton(130, 180, 2152, 2153, 0, GumpButtonType.Page, 1002);
			AddItem(180, 180, 5102);
			AddLabel(250, 185, 0, @"Sleeves");
			AddButton(130, 220, 2152, 2153, 0, GumpButtonType.Page, 1003);
			AddItem(180, 220, 5100);
			AddLabel(250, 222, 0, @"Tunic");
			
			AddPage(102);  //ChainMail
			AddBackground(100, 50, 227, 257, 9250);
			AddLabel(130, 70, 0, @"Pick Chainmail Armor Piece");
			AddButton(130, 100, 2152, 2153, 0, GumpButtonType.Page, 2000);
			AddItem(180, 100, 5051);
			AddLabel(250, 102, 0, @"Coif");
			AddButton(130, 140, 2152, 2153, 0, GumpButtonType.Page, 2001);
			AddItem(180, 135, 5054);
			AddLabel(250, 140, 0, @"Leggings");
			AddButton(130, 180, 2152, 2153, 0, GumpButtonType.Page, 2002);
			AddItem(180, 180, 5055);
			AddLabel(250, 185, 0, @"Tunic");
			
			AddPage(103);  // Platemail 
			AddBackground(100, 50, 250, 365, 9250);
			AddLabel(130, 70, 0, @"Pick Platemail Armor Piece");
			AddButton(130, 100, 2152, 2153, 0, GumpButtonType.Page, 3000);
			AddItem(180, 100, 5136);
			AddLabel(250, 100, 0, @"Arms");
			AddButton(130, 140, 2152, 2153, 0, GumpButtonType.Page, 3001);
			AddItem(180, 135, 5140);
			AddLabel(250, 140, 0, @"Gloves");
			AddButton(130, 180, 2152, 2153, 0, GumpButtonType.Page, 3002);
			AddItem(180, 175, 5137);
			AddLabel(250, 180, 0, @"Leggings");
			AddButton(130, 220, 2152, 2153, 0, GumpButtonType.Page, 3003);
			AddItem(180, 220, 5141);
			AddLabel(250, 220, 0, @"Tunic");
			AddButton(130, 260, 2152, 2153, 0, GumpButtonType.Page, 3004);
			AddItem(180, 262, 5139);
			AddLabel(250, 265, 0, @"Gorget");
			AddButton(130, 300, 2152, 2153, 0, GumpButtonType.Page, 3005);
			AddItem(180, 298, 5138);
			AddLabel(250, 297, 0, @"Helm");
			AddButton(130, 340, 2152, 2153, 0, GumpButtonType.Page, 3006);
			AddItem(180, 340, 7172);
			AddLabel(250, 340, 0, @"Female Tunic");
			
			AddPage(104);  // Shields
			AddBackground(100, 50, 300, 340, 9250);
			AddLabel(130, 70, 0, @"Pick Shield");
			AddButton(130, 100, 2152, 2153, 0, GumpButtonType.Page, 4000);
			AddItem(180, 100, 0x1B72);
			AddLabel(250, 100, 0, @"Bronze Shield");
			AddButton(130, 140, 2152, 2153, 0, GumpButtonType.Page, 4001);
			AddItem(180, 135, 0x1B73);
			AddLabel(250, 140, 0, @"Buckler");
			AddButton(130, 180, 2152, 2153, 0, GumpButtonType.Page, 4002);
			AddItem(180, 175, 0x1B76);
			AddLabel(250, 180, 0, @"Heater Shield");
			AddButton(130, 220, 2152, 2153, 0, GumpButtonType.Page, 4003);
			AddItem(180, 220, 0x1B74);
			AddLabel(250, 220, 0, @"Metal Kite Shield");
			AddButton(130, 260, 2152, 2153, 0, GumpButtonType.Page, 4004);
			AddItem(180, 262, 0x1B7B);
			AddLabel(250, 265, 0, @"Metal Shield");
			AddButton(130, 300, 2152, 2153, 0, GumpButtonType.Page, 4005);
			AddItem(180, 298, 0x1B78);
			AddLabel(250, 297, 0, @"Wooden Kite Shield");
			
			AddPage(105);  // Axes
			AddBackground(100, 50, 300, 340, 9250);
			AddLabel(130, 70, 0, @"Pick Axe Type");
			AddButton(130, 100, 2152, 2153, 0, GumpButtonType.Page, 5000);
			AddItem(180, 100, 0x0F49);
			AddLabel(250, 100, 0, @"Axe");
			AddButton(130, 140, 2152, 2153, 0, GumpButtonType.Page, 5001);
			AddItem(180, 135, 0x0F47);
			AddLabel(250, 140, 0, @"Battle Axe");
			AddButton(130, 180, 2152, 2153, 0, GumpButtonType.Page, 5002);
			AddItem(180, 175, 0x0F4B);
			AddLabel(250, 180, 0, @"Double Axe");
			AddButton(130, 220, 2152, 2153, 0, GumpButtonType.Page, 5003);
			AddItem(180, 220, 0x0F45);
			AddLabel(250, 220, 0, @"Executioner Axe");
			AddButton(130, 260, 2152, 2153, 0, GumpButtonType.Page, 5004);
			AddItem(180, 262, 0x13FB);
			AddLabel(250, 265, 0, @"Large Battle Axe");
			AddButton(130, 300, 2152, 2153, 0, GumpButtonType.Page, 5005);
			AddItem(180, 298, 0x1443);
			AddLabel(250, 297, 0, @"Two Handed Axe");
			
			AddPage(106);  // Maces
			AddBackground(100, 50, 300, 340, 9250);
			AddLabel(130, 70, 0, @"Pick Mace Type");
			AddButton(130, 100, 2152, 2153, 0, GumpButtonType.Page, 5006);
			AddItem(180, 100, 0x13B0);
			AddLabel(250, 100, 0, @"WarAxe");
			AddButton(130, 140, 2152, 2153, 0, GumpButtonType.Page, 5007);
			AddItem(180, 135, 0x143D);
			AddLabel(250, 140, 0, @"Hammerpick");
			AddButton(130, 180, 2152, 2153, 0, GumpButtonType.Page, 5008);
			AddItem(180, 175, 0x0F5C);
			AddLabel(250, 180, 0, @"Mace");
			AddButton(130, 220, 2152, 2153, 0, GumpButtonType.Page, 5009);
			AddItem(180, 220, 0x143B);
			AddLabel(250, 220, 0, @"Maul");
			AddButton(130, 260, 2152, 2153, 0, GumpButtonType.Page, 5010);
			AddItem(180, 262, 0x1439);
			AddLabel(250, 265, 0, @"WarHammer");
			AddButton(130, 300, 2152, 2153, 0, GumpButtonType.Page, 5011);
			AddItem(180, 298, 0x1407);
			AddLabel(250, 297, 0, @"WarMace");
			
			AddPage(107);  // Swords
			AddBackground(100, 50, 300, 340, 9250);
			AddLabel(130, 70, 0, @"Pick Sword Type");
			AddButton(130, 100, 2152, 2153, 0, GumpButtonType.Page, 5012);
			AddItem(180, 100, 0x0F5E);
			AddLabel(250, 100, 0, @"Broadsword");
			AddButton(130, 140, 2152, 2153, 0, GumpButtonType.Page, 5013);
			AddItem(180, 135, 0x1441);
			AddLabel(250, 140, 0, @"Cutlass");
			AddButton(130, 180, 2152, 2153, 0, GumpButtonType.Page, 5014);
			AddItem(180, 175, 0x13FF);
			AddLabel(250, 180, 0, @"Katana");
			AddButton(130, 220, 2152, 2153, 0, GumpButtonType.Page, 5015);
			AddItem(160, 220, 0x0F61);
			AddLabel(250, 220, 0, @"Longsword");
			AddButton(130, 260, 2152, 2153, 0, GumpButtonType.Page, 5016);
			AddItem(180, 262, 0x13B6);
			AddLabel(250, 265, 0, @"Scimitar");
			AddButton(130, 300, 2152, 2153, 0, GumpButtonType.Page, 5017);
			AddItem(180, 298, 0x13B9);
			AddLabel(250, 297, 0, @"Vikingsword");
			
			AddPage(108);  // Fencing & PoleArms
			AddBackground(100, 50, 300, 340, 9250);
			AddLabel(130, 70, 0, @"Pick Fencing or PoleArm Type");
			AddButton(130, 100, 2152, 2153, 0, GumpButtonType.Page, 5018);
			AddItem(180, 100, 3922);
			AddLabel(250, 100, 0, @"Dagger");
			AddButton(130, 140, 2152, 2153, 0, GumpButtonType.Page, 5019);
			AddItem(180, 135, 5123);
			AddLabel(250, 140, 0, @"ShortSpear");
			AddButton(130, 180, 2152, 2153, 0, GumpButtonType.Page, 5020);
			AddItem(140, 175, 3938);
			AddLabel(250, 180, 0, @"Spear");
			AddButton(130, 220, 2152, 2153, 0, GumpButtonType.Page, 5021);
			AddItem(180, 220, 5125);
			AddLabel(250, 220, 0, @"WarFork");
			AddButton(130, 260, 2152, 2153, 0, GumpButtonType.Page, 5022);
			AddItem(180, 262, 5121);
			AddLabel(250, 265, 0, @"Kryss");
			AddButton(130, 300, 2152, 2153, 0, GumpButtonType.Page, 5023);
			AddItem(150, 298, 3917);
			AddLabel(250, 297, 0, @"Bardiche");
			AddButton(130, 340, 2152, 2153, 0, GumpButtonType.Page, 5024);
			AddItem(180, 338, 5182);
			AddLabel(250, 338, 0, @"Halberd");
			
			AddPage(20); // Types of Tailor BODs
			AddBackground(100, 50, 250, 460, 2600);
			AddLabel(150, 70, 0, @"Pick the type of BOD"); 
			AddLabel(210, 103, 0, @"Leather Armor");
			AddButton(170, 100, 2152, 2153, 0, GumpButtonType.Page, 200);
			AddLabel(210, 143, 0, @"Studded Armor");
			AddButton(170, 140, 2152, 2153, 0, GumpButtonType.Page, 201);
			AddLabel(210, 183, 0, @"Bone Armor");
			AddButton(170, 180, 2152, 2153, 0, GumpButtonType.Page, 202);
			AddLabel(210, 223, 0, @"Female Armor");
			AddButton(170, 220, 2152, 2153, 0, GumpButtonType.Page, 203);
			AddLabel(210, 263, 0, @"Hats");
			AddButton(170, 260, 2152, 2153, 0, GumpButtonType.Page, 204);
			AddLabel(210, 303, 0, @"Shirts");
			AddButton(170, 300, 2152, 2153, 0, GumpButtonType.Page, 205);
			AddLabel(210, 343, 0, @"Pants ");
			AddButton(170, 340, 2152, 2153, 0, GumpButtonType.Page, 206);
			AddLabel(210, 383, 0, @"Shoes ");
			AddButton(170, 380, 2152, 2153, 0, GumpButtonType.Page, 207);
			AddLabel(210, 423, 0, @"Misc ");
			AddButton(170, 420, 2152, 2153, 0, GumpButtonType.Page, 208);
			
			AddPage(200);  // Leather Armor
			AddBackground(100, 50, 300, 340, 9250);
			AddLabel(130, 70, 0, @"Pick Leather Armor Piece");
			AddButton(130, 100, 2152, 2153, 0, GumpButtonType.Page, 6000);
			AddItem(180, 100, 5063);
			AddLabel(250, 100, 0, @"Leather Gorget");
			AddButton(130, 140, 2152, 2153, 0, GumpButtonType.Page, 6001);
			AddItem(180, 135, 7609);
			AddLabel(250, 140, 0, @"Leather Cap");
			AddButton(130, 180, 2152, 2153, 0, GumpButtonType.Page, 6002);
			AddItem(180, 175, 5062);
			AddLabel(250, 180, 0, @"Leather Gloves");
			AddButton(130, 220, 2152, 2153, 0, GumpButtonType.Page, 6003);
			AddItem(180, 220, 5069);
			AddLabel(250, 220, 0, @"Leather Arms");
			AddButton(130, 260, 2152, 2153, 0, GumpButtonType.Page, 6004);
			AddItem(180, 262, 5067);
			AddLabel(250, 265, 0, @"Leather Legs");
			AddButton(130, 300, 2152, 2153, 0, GumpButtonType.Page, 6005);
			AddItem(180, 298, 5068);
			AddLabel(250, 297, 0, @"Leather Chest");
			
			AddPage(201);  // Studded Armor
			AddBackground(100, 50, 300, 340, 9250);
			AddLabel(130, 70, 0, @"Pick Studded Armor Piece");
			AddButton(130, 100, 2152, 2153, 0, GumpButtonType.Page, 6006);
			AddItem(180, 100, 5078);
			AddLabel(250, 100, 0, @"Studded Gorget");
			AddButton(130, 140, 2152, 2153, 0, GumpButtonType.Page, 6007);
			AddItem(180, 135, 5077);
			AddLabel(250, 140, 0, @"Studded Gloves");
			AddButton(130, 180, 2152, 2153, 0, GumpButtonType.Page, 6008);
			AddItem(180, 175, 5084);
			AddLabel(250, 180, 0, @"Studded Arms");
			AddButton(130, 220, 2152, 2153, 0, GumpButtonType.Page, 6009);
			AddItem(180, 220, 5082);
			AddLabel(250, 220, 0, @"Studded Legs");
			AddButton(130, 260, 2152, 2153, 0, GumpButtonType.Page, 6010);
			AddItem(180, 262, 5083);
			AddLabel(250, 265, 0, @"Studded Chest");
			
			AddPage(202);  // Bone Armor
			AddBackground(100, 50, 300, 340, 9250);
			AddLabel(130, 70, 0, @"Pick Bone Armor Piece");
			AddButton(130, 100, 2152, 2153, 0, GumpButtonType.Page, 6011);
			AddItem(180, 100, 5201);
			AddLabel(250, 100, 0, @"Bone Helm");
			AddButton(130, 140, 2152, 2153, 0, GumpButtonType.Page, 6012);
			AddItem(180, 135, 5200);
			AddLabel(250, 140, 0, @"Bone Gloves");
			AddButton(130, 180, 2152, 2153, 0, GumpButtonType.Page, 6013);
			AddItem(180, 175, 5198);
			AddLabel(250, 180, 0, @"Bone Arms");
			AddButton(130, 220, 2152, 2153, 0, GumpButtonType.Page, 6014);
			AddItem(180, 220, 5202);
			AddLabel(250, 220, 0, @"Bone Legs");
			AddButton(130, 260, 2152, 2153, 0, GumpButtonType.Page, 6015);
			AddItem(180, 262, 5199);
			AddLabel(250, 265, 0, @"Bone Chest");
			
			AddPage(203);  // Female Armor
			AddBackground(100, 50, 300, 340, 9250);
			AddLabel(130, 70, 0, @"Pick Female Armor Piece");
			AddButton(130, 100, 2152, 2153, 0, GumpButtonType.Page, 6016);
			AddItem(180, 100, 7168);
			AddLabel(250, 100, 0, @"Leather Shorts");
			AddButton(130, 140, 2152, 2153, 0, GumpButtonType.Page, 6017);
			AddItem(180, 135, 7176);
			AddLabel(250, 140, 0, @"Leather Skirt");
			AddButton(130, 180, 2152, 2153, 0, GumpButtonType.Page, 6018);
			AddItem(180, 175, 7178);
			AddLabel(250, 180, 0, @"Leather Bustier");
			AddButton(130, 220, 2152, 2153, 0, GumpButtonType.Page, 6019);
			AddItem(180, 220, 7180);
			AddLabel(250, 220, 0, @"Studded Bustier");
			AddButton(130, 260, 2152, 2153, 0, GumpButtonType.Page, 6020);
			AddItem(180, 262, 7174);
			AddLabel(250, 265, 0, @"Leather Chest");
			AddButton(130, 300, 2152, 2153, 0, GumpButtonType.Page, 6021);
			AddItem(180, 302, 7170);
			AddLabel(250, 305, 0, @"Studded Chest");
			
			AddPage(204);  // Hats
			AddBackground(100, 50, 300, 580, 9250);
			AddLabel(130, 70, 0, @"Pick  a Hat");
			AddButton(130, 100, 2152, 2153, 0, GumpButtonType.Page, 6022);
			AddItem(180, 100, 5440);
			AddLabel(250, 100, 0, @"Bandana");
			AddButton(130, 140, 2152, 2153, 0, GumpButtonType.Page, 6023);
			AddItem(180, 135, 5444);
			AddLabel(250, 140, 0, @"SkullCap");
			AddButton(130, 180, 2152, 2153, 0, GumpButtonType.Page, 6024);
			AddItem(180, 175, 5907);
			AddLabel(250, 180, 0, @"Floppy Hat");
			AddButton(130, 220, 2152, 2153, 0, GumpButtonType.Page, 6025);
			AddItem(180, 220, 5908);
			AddLabel(250, 220, 0, @"Wide Brim Hat");
			AddButton(130, 260, 2152, 2153, 0, GumpButtonType.Page, 6026);
			AddItem(180, 262, 5909);
			AddLabel(250, 265, 0, @"Cap");
			AddButton(130, 300, 2152, 2153, 0, GumpButtonType.Page, 6027);
			AddItem(180, 302, 5910);
			AddLabel(250, 305, 0, @"Tall Straw Hat");
			AddButton(130, 340, 2152, 2153, 0, GumpButtonType.Page, 6028);
			AddItem(180, 342, 5911);
			AddLabel(250, 345, 0, @"Straw Hat");
			AddButton(130, 380, 2152, 2153, 0, GumpButtonType.Page, 6029);
			AddItem(180, 383, 5912);
			AddLabel(250, 380, 0, @"Wizards hat");
			AddButton(130, 420, 2152, 2153, 0, GumpButtonType.Page, 6030);
			AddItem(180, 423, 5913);
			AddLabel(250, 420, 0, @"Bonnet");
			AddButton(130, 460, 2152, 2153, 0, GumpButtonType.Page, 6031);
			AddItem(180, 462, 5914);
			AddLabel(250, 460, 0, @"Feathered Hat");
			AddButton(130, 500, 2152, 2153, 0, GumpButtonType.Page, 6032);
			AddItem(180, 502, 5915);
			AddLabel(250, 500, 0, @"Tricorne Hat");
			AddButton(130, 540, 2152, 2153, 0, GumpButtonType.Page, 6033);
			AddItem(180, 542, 5916);
			AddLabel(250, 540, 0, @"Jester Hat");
			
			AddPage(205);  // Shirts
			AddBackground(100, 50, 300, 500, 9250);
			AddLabel(130, 70, 0, @"Pick  a Shirt Type");
			AddButton(130, 100, 2152, 2153, 0, GumpButtonType.Page, 6034);
			AddItem(180, 100, 8059);
			AddLabel(250, 100, 0, @"Doublet");
			AddButton(130, 140, 2152, 2153, 0, GumpButtonType.Page, 6035);
			AddItem(180, 135, 5399);
			AddLabel(250, 140, 0, @"Shirt");
			AddButton(130, 180, 2152, 2153, 0, GumpButtonType.Page, 6036);
			AddItem(180, 175, 7933);
			AddLabel(250, 180, 0, @"Fancy Shirt");
			AddButton(130, 220, 2152, 2153, 0, GumpButtonType.Page, 6037);
			AddItem(180, 220, 8097);
			AddLabel(250, 220, 0, @"Tunic");
			AddButton(130, 260, 2152, 2153, 0, GumpButtonType.Page, 6038);
			AddItem(180, 262, 8189);
			AddLabel(250, 265, 0, @"Surcoat");
			AddButton(130, 300, 2152, 2153, 0, GumpButtonType.Page, 6039);
			AddItem(180, 302, 7937);
			AddLabel(250, 305, 0, @"Plain Dress");
			AddButton(130, 340, 2152, 2153, 0, GumpButtonType.Page, 6040);
			AddItem(180, 342, 7936);
			AddLabel(250, 345, 0, @"Fancy Dress");
			AddButton(130, 380, 2152, 2153, 0, GumpButtonType.Page, 6041);
			AddItem(180, 383, 5397);
			AddLabel(250, 380, 0, @"Cloak");
			AddButton(130, 420, 2152, 2153, 0, GumpButtonType.Page, 6042);
			AddItem(180, 423, 7939);
			AddLabel(250, 420, 0, @"Robe");
			AddButton(130, 460, 2152, 2153, 0, GumpButtonType.Page, 6043);
			AddItem(180, 462, 8095);
			AddLabel(250, 460, 0, @"Jester Suit");
			
			AddPage(206); //Pants
			AddBackground(100, 50, 227, 257, 9250);
			AddLabel(130, 70, 0, @"Pick Pants Type");
			AddButton(130, 100, 2152, 2153, 0, GumpButtonType.Page, 6044);
			AddItem(180, 100, 5422);
			AddLabel(250, 102, 0, @"Short Pants");
			AddButton(130, 140, 2152, 2153, 0, GumpButtonType.Page, 6045);
			AddItem(180, 135, 5433);
			AddLabel(250, 140, 0, @"Long Pants");
			AddButton(130, 180, 2152, 2153, 0, GumpButtonType.Page, 6046);
			AddItem(180, 180, 5431);
			AddLabel(250, 185, 0, @"Kilt");
			AddButton(130, 220, 2152, 2153, 0, GumpButtonType.Page, 6047);
			AddItem(180, 220, 5398);
			AddLabel(250, 222, 0, @"Skirt");
			
			AddPage(207); //Shoes
			AddBackground(100, 50, 227, 257, 9250);
			AddLabel(130, 70, 0, @"Pick Shoe Type");
			AddButton(130, 100, 2152, 2153, 0, GumpButtonType.Page, 6048);
			AddItem(180, 100, 5899);
			AddLabel(250, 102, 0, @"Boots");
			AddButton(130, 140, 2152, 2153, 0, GumpButtonType.Page, 6049);
			AddItem(180, 135, 5905);
			AddLabel(250, 140, 0, @"Thigh Boots");
			AddButton(130, 180, 2152, 2153, 0, GumpButtonType.Page, 6050);
			AddItem(180, 180, 5903);
			AddLabel(250, 185, 0, @"Shoes");
			AddButton(130, 220, 2152, 2153, 0, GumpButtonType.Page, 6051);
			AddItem(180, 220, 5901);
			AddLabel(250, 222, 0, @"Sandals");
			
			AddPage(208); //Misc
			AddBackground(100, 50, 227, 257, 9250);
			AddLabel(130, 70, 0, @"Pick Misc Type");
			AddButton(130, 100, 2152, 2153, 0, GumpButtonType.Page, 6052);
			AddItem(180, 100, 5441);
			AddLabel(250, 102, 0, @"Body Sash");
			AddButton(130, 140, 2152, 2153, 0, GumpButtonType.Page, 6053);
			AddItem(180, 135, 5435);
			AddLabel(250, 140, 0, @"Half Apron");
			AddButton(130, 180, 2152, 2153, 0, GumpButtonType.Page, 6054);
			AddItem(180, 180, 5437);
			AddLabel(250, 185, 0, @"Full Apron");
			
		AddPage(1000); // Amount and Colors for Ringmail Gloves
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 10, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 11, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 12, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 13, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 14, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 15, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 16, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 17, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 18, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 19, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 20, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 21, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 22, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 23, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 24, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 25, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 26, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 27, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 28, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 29, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 30, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 31, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 32, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 33, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 34, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 35, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 36, GumpButtonType.Reply,0);
			
		AddPage(1001); // Amount and Colors for Ringmail Legs
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 110, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 111, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 112, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 113, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 114, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 115, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 116, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 117, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 118, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 119, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 120, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 121, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 122, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 123, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 124, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 125, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 126, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 127, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 128, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 129, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 130, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 131, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 132, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 133, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 134, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 135, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 136, GumpButtonType.Reply,0);
			
		AddPage(1002); // Amount and Colors for Ringmail Arms
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 210, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 211, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 212, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 213, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 214, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 215, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 216, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 217, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 218, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 219, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 220, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 221, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 222, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 223, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 224, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 225, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 226, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 227, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 228, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 229, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 230, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 231, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 232, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 233, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 234, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 235, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 236, GumpButtonType.Reply,0);
			
		AddPage(1003); // Amount and Colors for Ringmail Tunic
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 310, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 311, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 312, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 313, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 314, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 315, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 316, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 317, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 318, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 319, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 320, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 321, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 322, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 323, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 324, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 325, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 326, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 327, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 328, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 329, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 330, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 331, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 332, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 333, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 334, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 335, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 336, GumpButtonType.Reply,0);
			
			AddPage(2000); // Amount and Colors for Chainmail Coif
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 410, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 411, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 412, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 413, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 414, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 415, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 416, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 417, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 418, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 419, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 420, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 421, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 422, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 423, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 424, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 425, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 426, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 427, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 428, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 429, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 430, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 431, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 432, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 433, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 434, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 435, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 436, GumpButtonType.Reply,0);
			
			AddPage(2001); // Amount and Colors for Chainmail Leggings
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 510, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 511, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 512, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 513, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 514, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 515, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 516, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 517, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 518, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 519, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 520, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 521, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 522, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 523, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 524, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 525, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 526, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 527, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 528, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 529, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 530, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 531, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 532, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 533, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 534, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 535, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 536, GumpButtonType.Reply,0);
			
			AddPage(2002); // Amount and Colors for Chainmail Tunic
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 610, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 611, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 612, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 613, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 614, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 615, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 616, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 617, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 618, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 619, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 620, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 621, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 622, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 623, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 624, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 625, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 626, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 627, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 628, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 629, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 630, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 631, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 632, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 633, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 634, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 635, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 636, GumpButtonType.Reply,0);
			
			AddPage(3000); // Amount and Colors for Platemail Arms
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 710, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 711, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 712, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 713, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 714, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 715, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 716, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 717, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 718, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 719, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 720, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 721, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 722, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 723, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 724, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 725, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 726, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 727, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 728, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 729, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 730, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 731, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 732, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 733, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 734, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 735, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 736, GumpButtonType.Reply,0);
			
			AddPage(3001); // Amount and Colors for Platemail Gloves
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 810, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 811, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 812, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 813, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 814, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 815, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 816, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 817, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 818, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 819, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 820, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 821, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 822, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 823, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 824, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 825, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 826, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 827, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 828, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 829, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 830, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 831, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 832, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 833, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 834, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 835, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 836, GumpButtonType.Reply,0);
			
			AddPage(3002); // Amount and Colors for Platemail Leggings
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 910, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 911, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 912, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 913, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 914, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 915, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 916, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 917, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 918, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 919, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 920, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 921, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 922, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 923, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 924, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 925, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 926, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 927, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 928, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 929, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 930, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 931, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 932, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 933, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 934, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 935, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 936, GumpButtonType.Reply,0);
			
			AddPage(3003); // Amount and Colors for Plate Chest
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 1010, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 1011, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 1012, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 1013, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 1014, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 1015, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 1016, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 1017, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 1018, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 1019, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 1020, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 1021, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 1022, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 1023, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 1024, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 1025, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 1026, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 1027, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 1028, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 1029, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 1030, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 1031, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 1032, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 1033, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 1034, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 1035, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 1036, GumpButtonType.Reply,0);
			
			AddPage(3004); // Amount and Colors for Plate Gorget
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 1110, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 1111, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 1112, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 1113, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 1114, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 1115, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 1116, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 1117, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 1118, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 1119, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 1120, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 1121, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 1122, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 1123, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 1124, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 1125, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 1126, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 1127, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 1128, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 1129, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 1130, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 1131, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 1132, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 1133, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 1134, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 1135, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 1136, GumpButtonType.Reply,0);
			
			AddPage(3005); // Amount and Colors for Plate Helm
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 1210, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 1211, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 1212, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 1213, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 1214, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 1215, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 1216, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 1217, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 1218, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 1219, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 1220, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 1221, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 1222, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 1223, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 1224, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 1225, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 1226, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 1227, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 1228, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 1229, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 1230, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 1231, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 1232, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 1233, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 1234, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 1235, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 1236, GumpButtonType.Reply,0);
			
			AddPage(3006); // Amount and Colors for Female Plate
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 1310, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 1311, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 1312, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 1313, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 1314, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 1315, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 1316, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 1317, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 1318, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 1319, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 1320, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 1321, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 1322, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 1323, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 1324, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 1325, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 1326, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 1327, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 1328, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 1329, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 1330, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 1331, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 1332, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 1333, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 1334, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 1335, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 1336, GumpButtonType.Reply,0);
			
			AddPage(4000); // Amount and Colors for Bronze Shield
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 1410, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 1411, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 1412, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 1413, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 1414, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 1415, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 1416, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 1417, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 1418, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 1419, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 1420, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 1421, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 1422, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 1423, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 1424, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 1425, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 1426, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 1427, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 1428, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 1429, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 1430, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 1431, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 1432, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 1433, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 1434, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 1435, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 1436, GumpButtonType.Reply,0);
			
			AddPage(4001); // Amount and Colors for Buckler
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 1510, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 1511, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 1512, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 1513, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 1514, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 1515, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 1516, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 1517, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 1518, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 1519, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 1520, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 1521, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 1522, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 1523, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 1524, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 1525, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 1526, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 1527, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 1528, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 1529, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 1530, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 1531, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 1532, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 1533, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 1534, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 1535, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 1536, GumpButtonType.Reply,0);
			
			AddPage(4002); // Amount and Colors for heater shield
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 1610, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 1611, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 1612, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 1613, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 1614, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 1615, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 1616, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 1617, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 1618, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 1619, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 1620, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 1621, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 1622, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 1623, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 1624, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 1625, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 1626, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 1627, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 1628, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 1629, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 1630, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 1631, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 1632, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 1633, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 1634, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 1635, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 1636, GumpButtonType.Reply,0);
			
			AddPage(4003); // Amount and Colors for Metal Kite Shield
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 1710, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 1711, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 1712, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 1713, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 1714, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 1715, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 1716, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 1717, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 1718, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 1719, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 1720, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 1721, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 1722, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 1723, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 1724, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 1725, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 1726, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 1727, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 1728, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 1729, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 1730, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 1731, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 1732, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 1733, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 1734, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 1735, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 1736, GumpButtonType.Reply,0);
			
			AddPage(4004); // Amount and Colors for Metal Shield
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 1810, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 1811, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 1812, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 1813, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 1814, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 1815, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 1816, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 1817, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 1818, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 1819, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 1820, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 1821, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 1822, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 1823, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 1824, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 1825, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 1826, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 1827, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 1828, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 1829, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 1830, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 1831, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 1832, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 1833, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 1834, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 1835, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 1836, GumpButtonType.Reply,0);
			
			AddPage(4005); // Amount and Colors for Wooden Kite Shield
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 1910, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 1911, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 1912, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 1913, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 1914, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 1915, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 1916, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 1917, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 1918, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 1919, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 1920, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 1921, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 1922, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 1923, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 1924, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 1925, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 1926, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 1927, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 1928, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 1929, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 1930, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 1931, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 1932, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 1933, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 1934, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 1935, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 1936, GumpButtonType.Reply,0);
			
			AddPage(5000); // Amount and Colors for Axes
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 2010, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 2011, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 2012, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 2013, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 2014, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 2015, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 2016, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 2017, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 2018, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 2019, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 2020, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 2021, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 2022, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 2023, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 2024, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 2025, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 2026, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 2027, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 2028, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 2029, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 2030, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 2031, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 2032, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 2033, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 2034, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 2035, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 2036, GumpButtonType.Reply,0);
			
			AddPage(5001); // Amount and Colors for Double Axe	
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 2110, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 2111, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 2112, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 2113, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 2114, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 2115, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 2116, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 2117, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 2118, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 2119, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 2120, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 2121, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 2122, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 2123, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 2124, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 2125, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 2126, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 2127, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 2128, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 2129, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 2130, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 2131, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 2132, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 2133, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 2134, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 2135, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 2136, GumpButtonType.Reply,0);
			
			AddPage(5002); // Amount and Colors for Executioner Axe
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 2210, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 2211, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 2212, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 2213, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 2214, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 2215, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 2216, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 2217, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 2218, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 2219, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 2220, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 2221, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 2222, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 2223, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 2224, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 2225, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 2226, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 2227, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 2228, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 2229, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 2230, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 2231, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 2232, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 2233, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 2234, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 2235, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 2236, GumpButtonType.Reply,0);
			
			AddPage(5004); // Amount and Colors for Large Battle Axe
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 2310, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 2311, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 2312, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 2313, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 2314, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 2315, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 2316, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 2317, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 2318, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 2319, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 2320, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 2321, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 2322, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 2323, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 2324, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 2325, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 2326, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 2327, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 2328, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 2329, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 2330, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 2331, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 2332, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 2333, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 2334, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 2335, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 2336, GumpButtonType.Reply,0);
			
			AddPage(5000); // Amount and Colors for Two handed Axe
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 2410, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 2411, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 2412, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 2413, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 2414, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 2415, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 2416, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 2417, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 2418, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 2419, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 2420, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 2421, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 2422, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 2423, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 2424, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 2425, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 2426, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 2427, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 2428, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 2429, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 2430, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 2431, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 2432, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 2433, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 2434, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 2435, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 2436, GumpButtonType.Reply,0);
			
			AddPage(5006); // Amount and Colors for WarAxe
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 2510, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 2511, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 2512, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 2513, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 2514, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 2515, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 2516, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 2517, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 2518, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 2519, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 2520, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 2521, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 2522, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 2523, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 2524, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 2525, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 2526, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 2527, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 2528, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 2529, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 2530, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 2531, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 2532, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 2533, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 2534, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 2535, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 2536, GumpButtonType.Reply,0);
			
			AddPage(5007); // Amount and Colors for HammerPick
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 2610, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 2611, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 2612, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 2613, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 2614, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 2615, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 2616, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 2617, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 2618, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 2619, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 2620, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 2621, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 2622, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 2623, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 2624, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 2625, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 2626, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 2627, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 2628, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 2629, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 2630, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 2631, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 2632, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 2633, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 2634, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 2635, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 2636, GumpButtonType.Reply,0);
			
			AddPage(5008); // Amount and Colors for Mace
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 2710, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 2711, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 2712, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 2713, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 2714, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 2715, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 2716, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 2717, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 2718, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 2719, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 2720, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 2721, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 2722, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 2723, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 2724, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 2725, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 2726, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 2727, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 2728, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 2729, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 2730, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 2731, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 2732, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 2733, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 2734, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 2735, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 2736, GumpButtonType.Reply,0);
			
			AddPage(5009); // Amount and Colors for Maul
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 2810, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 2811, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 2812, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 2813, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 2814, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 2815, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 2816, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 2817, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 2818, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 2819, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 2820, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 2821, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 2822, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 2823, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 2824, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 2825, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 2826, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 2827, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 2828, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 2829, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 2830, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 2831, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 2832, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 2833, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 2834, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 2835, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 2836, GumpButtonType.Reply,0);
			
			AddPage(5010); // Amount and Colors for WarHammer
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 2910, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 2911, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 2912, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 2913, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 2914, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 2915, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 2916, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 2917, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 2918, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 2919, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 2920, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 2921, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 2922, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 2923, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 2924, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 2925, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 2926, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 2927, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 2928, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 2929, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 2930, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 2931, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 2932, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 2933, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 2934, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 2935, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 2936, GumpButtonType.Reply,0);
			
			AddPage(5011); // Amount and Colors for WarMace
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 3010, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 3011, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 3012, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 3013, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 3014, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 3015, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 3016, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 3017, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 3018, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 3019, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 3020, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 3021, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 3022, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 3023, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 3024, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 3025, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 3026, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 3027, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 3028, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 3029, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 3030, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 3031, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 3032, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 3033, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 3034, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 3035, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 3036, GumpButtonType.Reply,0);
			
			AddPage(5012); // Amount and Colors for BroadSword
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 3110, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 3111, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 3112, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 3113, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 3114, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 3115, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 3116, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 3117, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 3118, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 3119, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 3120, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 3121, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 3122, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 3123, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 3124, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 3125, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 3126, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 3127, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 3128, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 3129, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 3130, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 3131, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 3132, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 3133, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 3134, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 3135, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 3136, GumpButtonType.Reply,0);
			
			AddPage(5013); // Amount and Colors for Cutlass
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 3210, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 3211, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 3212, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 3213, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 3214, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 3215, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 3216, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 3217, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 3218, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 3219, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 3220, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 3221, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 3222, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 3223, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 3224, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 3225, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 3226, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 3227, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 3228, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 3229, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 3230, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 3231, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 3232, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 3233, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 3234, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 3235, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 3236, GumpButtonType.Reply,0);
			
			AddPage(5014); // Amount and Colors for Katana
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 3310, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 3311, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 3312, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 3313, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 3314, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 3315, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 3316, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 3317, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 3318, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 3319, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 3320, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 3321, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 3322, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 3323, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 3324, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 3325, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 3326, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 3327, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 3328, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 3329, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 3330, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 3331, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 3332, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 3333, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 3334, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 3335, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 3336, GumpButtonType.Reply,0);
			
			AddPage(5015); // Amount and Colors for Longsword
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 3410, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 3411, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 3412, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 3413, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 3414, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 3415, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 3416, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 3417, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 3418, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 3419, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 3420, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 3421, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 3422, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 3423, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 3424, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 3425, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 3426, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 3427, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 3428, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 3429, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 3430, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 3431, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 3432, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 3433, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 3434, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 3435, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 3436, GumpButtonType.Reply,0);
			
			AddPage(5016); // Amount and Colors for Scimitar
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 3510, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 3511, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 3512, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 3513, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 3514, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 3515, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 3516, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 3517, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 3518, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 3519, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 3520, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 3521, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 3522, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 3523, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 3524, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 3525, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 3526, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 3527, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 3528, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 3529, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 3530, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 3531, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 3532, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 3533, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 3534, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 3535, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 3536, GumpButtonType.Reply,0);
			
			AddPage(5017); // Amount and Colors for Viking Sword
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 3610, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 3611, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 3612, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 3613, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 3614, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 3615, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 3616, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 3617, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 3618, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 3619, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 3620, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 3621, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 3622, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 3623, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 3624, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 3625, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 3626, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 3627, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 3628, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 3629, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 3630, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 3631, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 3632, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 3633, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 3634, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 3635, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 3636, GumpButtonType.Reply,0);
			
			AddPage(5018); // Amount and Colors for Dagger
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 3710, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 3711, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 3712, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 3713, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 3714, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 3715, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 3716, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 3717, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 3718, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 3719, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 3720, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 3721, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 3722, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 3723, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 3724, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 3725, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 3726, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 3727, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 3728, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 3729, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 3730, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 3731, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 3732, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 3733, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 3734, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 3735, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 3736, GumpButtonType.Reply,0);
			
			AddPage(5019); // Amount and Colors for Short Spear
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 3810, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 3811, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 3812, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 3813, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 3814, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 3815, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 3816, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 3817, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 3818, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 3819, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 3820, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 3821, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 3822, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 3823, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 3824, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 3825, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 3826, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 3827, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 3828, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 3829, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 3830, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 3831, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 3832, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 3833, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 3834, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 3835, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 3836, GumpButtonType.Reply,0);
			
			AddPage(5039); // Amount and Colors for Spear
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 3910, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 3911, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 3912, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 3913, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 3914, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 3915, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 3916, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 3917, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 3918, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 3919, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 3920, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 3921, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 3922, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 3923, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 3924, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 3925, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 3926, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 3927, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 3928, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 3929, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 3930, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 3931, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 3932, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 3933, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 3934, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 3935, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 3936, GumpButtonType.Reply,0);
			
			AddPage(5021); // Amount and Colors for War Fork
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 4010, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 4011, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 4012, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 4013, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 4014, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 4015, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 4016, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 4017, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 4018, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 4019, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 4020, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 4021, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 4022, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 4023, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 4024, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 4025, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 4026, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 4027, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 4028, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 4029, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 4030, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 4031, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 4032, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 4033, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 4034, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 4035, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 4036, GumpButtonType.Reply,0);
			
			AddPage(5022); // Amount and Colors for Kryss
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 4110, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 4111, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 4112, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 4113, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 4114, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 4115, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 4116, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 4117, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 4118, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 4119, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 4120, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 4121, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 4122, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 4123, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 4124, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 4125, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 4126, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 4127, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 4128, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 4129, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 4130, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 4131, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 4132, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 4133, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 4134, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 4135, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 4136, GumpButtonType.Reply,0);
			
			AddPage(5023); // Amount and Colors for Bardiche
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 4210, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 4211, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 4212, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 4213, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 4214, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 4215, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 4216, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 4217, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 4218, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 4219, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 4220, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 4221, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 4222, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 4223, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 4224, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 4225, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 4226, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 4227, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 4228, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 4229, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 4230, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 4231, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 4232, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 4233, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 4234, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 4235, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 4236, GumpButtonType.Reply,0);
			
			AddPage(5024); // Amount and Colors for Halberd
			AddBackground(100, 50, 650, 575, 2600);
			AddLabel(300, 75, 0, @"Choose Ingot Type and Quantity"); 
			//Iron BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Iron 10");
			AddButton(150, 120, 2152, 2153, 4310, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Iron 15");
			AddButton(150, 160, 2152, 2153, 4311, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Iron 20");
			AddButton(150, 200, 2152, 2153, 4312, GumpButtonType.Reply,0); 
			//Dull Copper BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Dull Copper 10");
			AddButton(150, 280, 2152, 2153, 4313, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Dull Copper 15");
			AddButton(150, 320, 2152, 2153, 4314, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Dull Copper 20");
			AddButton(150, 360, 2152, 2153, 4315, GumpButtonType.Reply,0); 
			//Shadow Iron BODs
			AddBackground(130, 420, 175, 150, 9250);
			AddLabel(190, 443, 0, @"Shadow 10");
			AddButton(150, 440, 2152, 2153, 4316, GumpButtonType.Reply,0); 
			AddLabel(190, 483, 0, @"Shadow 15");
			AddButton(150, 480, 2152, 2153, 4317, GumpButtonType.Reply,0); 
			AddLabel(190, 523, 0, @"Shadow 20");
			AddButton(150, 520, 2152, 2153, 4318, GumpButtonType.Reply,0);
			//Copper BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Copper 10");
			AddButton(345, 120, 2152, 2153, 4319, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Copper 15");
			AddButton(345, 160, 2152, 2153, 4320, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Copper 20");
			AddButton(345, 200, 2152, 2153, 4321, GumpButtonType.Reply,0);
			//Bronze BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Bronze 10");
			AddButton(345, 280, 2152, 2153, 4322, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Bronze 15");
			AddButton(345, 320, 2152, 2153, 4323, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Bronze 20");
			AddButton(345, 360, 2152, 2153, 4324, GumpButtonType.Reply,0); 
			//Golden BODs
			AddBackground(325, 420, 175, 150, 9250);
			AddLabel(385, 443, 0, @"Golden 10");
			AddButton(345, 440, 2152, 2153, 4325, GumpButtonType.Reply,0); 
			AddLabel(385, 483, 0, @"Golden 15");
			AddButton(345, 480, 2152, 2153, 4326, GumpButtonType.Reply,0); 
			AddLabel(385, 523, 0, @"Golden 20");
			AddButton(345, 520, 2152, 2153, 4327, GumpButtonType.Reply,0);
			//Aggy BODs
			AddBackground(525, 100, 175, 150, 9250);
			AddLabel(585, 123, 0, @"Agapite 10");
			AddButton(545, 120, 2152, 2153, 4328, GumpButtonType.Reply,0); 
			AddLabel(585, 163, 0, @"Agapite 15");
			AddButton(545, 160, 2152, 2153, 4329, GumpButtonType.Reply,0); 
			AddLabel(585, 203, 0, @"Agapite 20");
			AddButton(545, 200, 2152, 2153, 4330, GumpButtonType.Reply,0);
			//Verite BODs
			AddBackground(525, 260, 175, 150, 9250);
			AddLabel(585, 283, 0, @"Verite 10");
			AddButton(545, 280, 2152, 2153, 4331, GumpButtonType.Reply,0); 
			AddLabel(585, 323, 0, @"Verite 15");
			AddButton(545, 320, 2152, 2153, 4332, GumpButtonType.Reply,0); 
			AddLabel(585, 363, 0, @"Verite 20");
			AddButton(545, 360, 2152, 2153, 4333, GumpButtonType.Reply,0); 
			//Valorite BODs
			AddBackground(525, 420, 175, 150, 9250);
			AddLabel(585, 443, 0, @"Valorite 10");
			AddButton(545, 440, 2152, 2153, 4334, GumpButtonType.Reply,0); 
			AddLabel(585, 483, 0, @"Valorite 15");
			AddButton(545, 480, 2152, 2153, 4335, GumpButtonType.Reply,0); 
			AddLabel(585, 523, 0, @"Valorite 20");
			AddButton(545, 520, 2152, 2153, 4336, GumpButtonType.Reply,0);
			
	AddPage(6000); // Amount and Leather Type for Leather Gorget 
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 4410, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 4411, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 4412, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 4413, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 4414, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 4415, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 4416, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 4417, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 4418, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 4419, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 4420, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 4421, GumpButtonType.Reply,0);
			
AddPage(6001); // Amount and Leather Type for Cap
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 4510, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 4511, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 4512, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 4513, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 4514, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 4515, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 4516, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 4517, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 4518, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 4519, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 4520, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 4521, GumpButtonType.Reply,0); 
			
AddPage(6002); // Amount and Leather Type for leather Gloves 
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 4610, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 4611, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 4612, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 4613, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 4614, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 4615, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 4616, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 4617, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 4618, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 4619, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 4620, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 4621, GumpButtonType.Reply,0); 
			
AddPage(6003); // Amount and Leather Type for Leather Arms
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 4710, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 4711, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 4712, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 4713, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 4714, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 4715, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 4716, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 4717, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 4718, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 4719, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 4720, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 4721, GumpButtonType.Reply,0); 
AddPage(6004); // Amount and Leather Type for Leather Legs
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 4810, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 4811, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 4812, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 4813, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 4814, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 4815, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 4816, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 4817, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 4818, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 4819, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 4820, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 4821, GumpButtonType.Reply,0); 
			
AddPage(6005); // Amount and Leather Type for Leather Chest
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 4910, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 4911, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 4912, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 4913, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 4914, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 4915, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 4916, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 4917, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 4918, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 4919, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 4920, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 4921, GumpButtonType.Reply,0);
			
AddPage(6006); // Amount and Leather Type for Studded Gorget
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 5010, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 5011, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 5012, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 5013, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 5014, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 5015, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 5016, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 5017, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 5018, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 5019, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 5020, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 5021, GumpButtonType.Reply,0); 
			
AddPage(6007); // Amount and Leather Type for Studded Gloves
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 5110, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 5111, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 5112, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 5113, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 5114, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 5115, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 5116, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 5117, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 5118, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 5119, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 5120, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 5121, GumpButtonType.Reply,0); 
			
AddPage(6008); // Amount and Leather Type for Studded Arms
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 5210, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 5211, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 5212, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 5213, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 5214, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 5215, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 5216, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 5217, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 5218, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 5219, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 5220, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 5221, GumpButtonType.Reply,0); 
			
AddPage(6009); // Amount and Leather Type for Studded Legs
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 5310, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 5311, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 5312, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 5313, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 5314, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 5315, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 5316, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 5317, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 5318, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 5319, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 5320, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 5321, GumpButtonType.Reply,0); 
			
AddPage(6010); // Amount and Leather Type for Studded Chest
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 5410, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 5411, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 5412, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 5413, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 5414, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 5415, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 5416, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 5417, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 5418, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 5419, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 5420, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 5421, GumpButtonType.Reply,0); 
			
AddPage(6011); // Amount and Leather Type for Bone Helm
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 5510, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 5511, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 5512, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 5513, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 5514, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 5515, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 5516, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 5517, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 5518, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 5519, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 5520, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 5521, GumpButtonType.Reply,0); 
			
AddPage(6012); // Amount and Leather Type for Bone Gloves
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 5610, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 5611, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 5612, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 5613, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 5614, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 5615, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 5616, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 5617, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 5618, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 5619, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 5620, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 5621, GumpButtonType.Reply,0); 
			
AddPage(6013); // Amount and Leather Type for Bone Arms
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 5710, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 5711, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 5712, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 5713, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 5714, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 5715, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 5716, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 5717, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 5718, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 5719, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 5720, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 5721, GumpButtonType.Reply,0); 
			
AddPage(6014); // Amount and Leather Type for Bone Legs
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 5810, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 5811, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 5812, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 5813, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 5814, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 5815, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 5816, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 5817, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 5818, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 5819, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 5820, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 5821, GumpButtonType.Reply,0); 
			
AddPage(6015); // Amount and Leather Type for Bone Chest
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 5910, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 5911, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 5912, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 5913, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 5914, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 5915, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 5916, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 5917, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 5918, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 5919, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 5920, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 5921, GumpButtonType.Reply,0); 
			
AddPage(6016); // Amount and Leather Type for Leather Shorts
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 6010, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 6011, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 6012, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 6013, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 6014, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 6015, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 6016, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 6017, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 6018, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 6019, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 6020, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 6021, GumpButtonType.Reply,0); 
			
AddPage(6017); // Amount and Leather Type for Leather Skirt
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 6110, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 6111, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 6112, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 6113, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 6114, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 6115, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 6116, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 6117, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 6118, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 6119, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 6120, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 6121, GumpButtonType.Reply,0); 
			
AddPage(6018); // Amount and Leather Type for Leather Bustier
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 6210, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 6211, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 6212, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 6213, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 6214, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 6215, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 6216, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 6217, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 6218, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 6219, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 6220, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 6221, GumpButtonType.Reply,0); 
			
AddPage(6019); // Amount and Leather Type for Studded Bustier
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 6310, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 6311, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 6312, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 6313, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 6314, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 6315, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 6316, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 6317, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 6318, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 6319, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 6320, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 6321, GumpButtonType.Reply,0); 
			
AddPage(6020); // Amount and Leather Type for Female Leather Chest
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 6410, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 6411, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 6412, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 6413, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 6414, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 6415, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 6416, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 6417, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 6418, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 6419, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 6420, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 6421, GumpButtonType.Reply,0); 
			
AddPage(6021); // Amount and Leather Type for Female Studded Chest
			AddBackground(100, 50, 450, 400, 2600);
			AddLabel(300, 75, 0, @"Choose Quantity"); 
			//Leather BODs
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"Leather 10");
			AddButton(150, 120, 2152, 2153, 6510, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"Leather 15");
			AddButton(150, 160, 2152, 2153, 6511, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"Leather 20");
			AddButton(150, 200, 2152, 2153, 6512, GumpButtonType.Reply,0); 
			//Spined BODs
			AddBackground(130, 260, 175, 150, 9250);
			AddLabel(190, 283, 0, @"Spined 10");
			AddButton(150, 280, 2152, 2153, 6513, GumpButtonType.Reply,0); 
			AddLabel(190, 323, 0, @"Spined 15");
			AddButton(150, 320, 2152, 2153, 6514, GumpButtonType.Reply,0); 
			AddLabel(190, 363, 0, @"Spined 20");
			AddButton(150, 360, 2152, 2153, 6515, GumpButtonType.Reply,0); 
			//Horned BODs
			AddBackground(325, 100, 175, 150, 9250);
			AddLabel(385, 123, 0, @"Horned 10");
			AddButton(345, 120, 2152, 2153, 6516, GumpButtonType.Reply,0); 
			AddLabel(385, 163, 0, @"Horned 15");
			AddButton(345, 160, 2152, 2153, 6517, GumpButtonType.Reply,0); 
			AddLabel(385, 203, 0, @"Horned 20");
			AddButton(345, 200, 2152, 2153, 6518, GumpButtonType.Reply,0);
			//Barbed BODs
			AddBackground(325, 260, 175, 150, 9250);
			AddLabel(385, 283, 0, @"Barbed 10");
			AddButton(345, 280, 2152, 2153, 6519, GumpButtonType.Reply,0); 
			AddLabel(385, 323, 0, @"Barbed 15");
			AddButton(345, 320, 2152, 2153, 6520, GumpButtonType.Reply,0); 
			AddLabel(385, 363, 0, @"Barbed 20");
			AddButton(345, 360, 2152, 2153, 6521, GumpButtonType.Reply,0); 
			
			AddPage(6022); // Amount and Cloth for Bandana
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 6601, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 6602, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 6603, GumpButtonType.Reply,0); 
			
			AddPage(6023); // Amount and Cloth for SkullCap
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 6701, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 6702, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 6703, GumpButtonType.Reply,0); 
			
			AddPage(6024); // Amount and Cloth for Floppyhat
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 6801, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 6802, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 6803, GumpButtonType.Reply,0); 
			
			AddPage(6025); // Amount and Cloth for Wide Brim hat
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 6901, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 6902, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 6903, GumpButtonType.Reply,0); 
			
			AddPage(6026); // Amount and Cloth for Cap
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 7001, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 7002, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 7003, GumpButtonType.Reply,0); 
			
			AddPage(6027); // Amount and Cloth for Tall Straw hat
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 7101, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 7102, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 7103, GumpButtonType.Reply,0); 
			
			AddPage(6028); // Amount and Cloth for Straw Hat
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 7201, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 7202, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 7203, GumpButtonType.Reply,0); 
			
			AddPage(6029); // Amount and Cloth for Wizards Hat
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 7301, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 7302, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 7303, GumpButtonType.Reply,0); 
			
			AddPage(6030); // Amount and Cloth for Bonnet
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 7401, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 7402, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 7403, GumpButtonType.Reply,0); 
			
			AddPage(6031); // Amount and Cloth for Feathered Hat
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 7501, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 7502, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 7503, GumpButtonType.Reply,0); 
			
			AddPage(6032); // Amount and Cloth for Tricorne Hat
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 7601, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 7602, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 7603, GumpButtonType.Reply,0); 
			
			AddPage(6033); // Amount and Cloth for Jester Hat
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 7701, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 7702, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 7703, GumpButtonType.Reply,0); 
			
			AddPage(6034); // Amount and Cloth for Doublet
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 7801, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 7802, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 7803, GumpButtonType.Reply,0); 
			
			AddPage(6035); // Amount and Cloth for Shirt
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 7901, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 7902, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 7903, GumpButtonType.Reply,0); 
			
			AddPage(6036); // Amount and Cloth for Fancy Shirt
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 8001, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 8002, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 8003, GumpButtonType.Reply,0); 
			
			AddPage(6037); // Amount and Cloth for Tunic
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 8101, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 8102, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 8103, GumpButtonType.Reply,0); 
			
			AddPage(6038); // Amount and Cloth for Surcoat
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 8201, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 8202, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 8203, GumpButtonType.Reply,0); 
			
			AddPage(6039); // Amount and Cloth for Plain Dress
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 8301, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 8302, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 8303, GumpButtonType.Reply,0); 
			
			AddPage(6040); // Amount and Cloth for Fancy Dress
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 8401, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 8402, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 8403, GumpButtonType.Reply,0); 
			
			AddPage(6041); // Amount and Cloth for Cloak
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 8501, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 8502, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 8503, GumpButtonType.Reply,0); 
			
			AddPage(6042); // Amount and Cloth for Robe
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 8601, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 8602, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 8603, GumpButtonType.Reply,0); 
			
			AddPage(6043); // Amount and Cloth for Jester Suit
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 8701, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 8702, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 8703, GumpButtonType.Reply,0); 
			
			AddPage(6044); // Amount and Cloth for Short Pants
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 8801, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 8802, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 8803, GumpButtonType.Reply,0); 
			
			AddPage(6045); // Amount and Cloth for Long Pants
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 8901, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 8902, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 8903, GumpButtonType.Reply,0); 
			
			AddPage(6046); // Amount and Cloth for Kilt
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 9001, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 9002, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 9003, GumpButtonType.Reply,0); 
			
			AddPage(6047); // Amount and Cloth for Skirt
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 9101, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 9102, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 9103, GumpButtonType.Reply,0); 
			
			AddPage(6048); // Amount and Cloth for Boots
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 9201, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 9202, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 9203, GumpButtonType.Reply,0); 
			
			AddPage(6049); // Amount and Cloth for Thigh Boots
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 9301, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 9302, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 9303, GumpButtonType.Reply,0); 
			
			AddPage(6050); // Amount and Cloth for Shoes
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 9401, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 9402, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 9403, GumpButtonType.Reply,0); 
			
			AddPage(6051); // Amount and Cloth for Sandles
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 9501, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 9502, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 9503, GumpButtonType.Reply,0); 
			
			AddPage(6052); // Amount and Cloth for BodySash
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 9601, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 9602, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 9603, GumpButtonType.Reply,0); 
			
			AddPage(6053); // Amount and Cloth for Half Apron
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 9701, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 9702, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 9703, GumpButtonType.Reply,0); 
			
			AddPage(6054); // Amount and Cloth for Full Apron
			AddBackground(100, 50, 250, 250, 2600);
			AddLabel(170, 75, 0, @"Choose Quantity"); 
			AddBackground(130, 100, 175, 150, 9250);
			AddLabel(190, 123, 0, @"10");
			AddButton(150, 120, 2152, 2153, 9801, GumpButtonType.Reply,0); 
			AddLabel(190, 163, 0, @"15");
			AddButton(150, 160, 2152, 2153, 9802, GumpButtonType.Reply,0); 
			AddLabel(190, 203, 0, @"20");
			AddButton(150, 200, 2152, 2153, 9803, GumpButtonType.Reply,0); 
		}
		
			
		public override void OnResponse( NetState sender, RelayInfo info )
		{
		
			 Mobile from = sender.Mobile;
			
			switch( info.ButtonID )
			{
				case 10:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.None ));break;}
				case 11:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.None ));break;}
				case 12:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.None ));break;}
				case 13:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.DullCopper ));break;}
				case 14:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.DullCopper ));break;}
				case 15:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.DullCopper ));break;}
				case 16:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.ShadowIron ));break;}
				case 17:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.ShadowIron ));break;}
				case 18:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.ShadowIron ));break;}
				case 19:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Copper ));break;}
				case 20:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Copper ));break;}
				case 21:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Copper ));break;}
				case 22:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Bronze ));break;}
				case 23:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Bronze ));break;}
				case 24:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Bronze ));break;}
				case 25:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Gold ));break;}
				case 26:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Gold ));break;}
				case 27:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Gold ));break;}
				case 28:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Agapite ));break;}
				case 29:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Agapite ));break;}
				case 30:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Agapite ));break;}
				case 31:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Verite ));break;}
				case 32:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Verite ));break;}
				case 33:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Verite ));break;}
				case 34:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Valorite ));break;}
				case 35:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Valorite ));break;}
				case 36:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailGloves), 1025099, 5099, true, BulkMaterialType.Valorite ));break;}
				case 110:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.None ));break;}
				case 111:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.None ));break;}
				case 112:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.None ));break;}
				case 113:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.DullCopper ));break; }
				case 114:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.DullCopper ));break;}
				case 115:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.DullCopper ));break;}
				case 116:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.ShadowIron ));break; }
				case 117:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.ShadowIron ));break;}
				case 118:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.ShadowIron ));break;}
				case 119:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Copper ));break; }
				case 120:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Copper ));break;}
				case 121:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Copper ));break;}
				case 122:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Bronze ));break; }
				case 123:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Bronze ));break;}
				case 124:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Bronze ));break;}
				case 125:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Gold ));break; }
				case 126:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Gold ));break;}
				case 127:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Gold ));break;}
				case 128:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Agapite ));break;}
				case 129:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Agapite ));break; }
				case 130:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Agapite ));break;}
				case 131:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Verite ));break;}
				case 132:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Verite ));break; }
				case 133:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Verite ));break;}
				case 134:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Valorite ));break;}
				case 135:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Valorite ));break; }
				case 136:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailLegs), 1025104, 5104, true, BulkMaterialType.Valorite ));break;}
				case 210:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.None ));break; }
				case 211:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.None ));break;}
				case 212:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.None ));break;}
				case 213:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.DullCopper ));break; }
				case 214:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.DullCopper ));break;}
				case 215:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.DullCopper ));break;}
				case 216:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.ShadowIron ));break; }
				case 217:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.ShadowIron ));break;}
				case 218:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.ShadowIron ));break;}
				case 219:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Copper ));break; }
				case 220:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Copper ));break;}
				case 221:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Copper ));break;}
				case 222:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Bronze ));break; }
				case 223:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Bronze ));break;}
				case 224:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Bronze ));break;}
				case 225:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Gold ));break; }
				case 226:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Gold ));break;}
				case 227:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Gold ));break;}
				case 228:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Agapite ));break;}
				case 229:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Agapite ));break; }
				case 230:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Agapite ));break;}
				case 231:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Verite ));break;}
				case 232:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Verite ));break; }
				case 233:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Verite ));break;}
				case 234:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Valorite ));break;}
				case 235:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Valorite ));break; }
				case 236:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailArms), 1025103, 5103, true, BulkMaterialType.Valorite ));break;}
				case 310:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.None ));break; }
				case 311:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.None ));break;}
				case 312:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.None ));break;}
				case 313:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.DullCopper ));break; }
				case 314:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.DullCopper ));break;}
				case 315:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.DullCopper ));break;}
				case 316:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.ShadowIron ));break; }
				case 317:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.ShadowIron ));break;}
				case 318:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.ShadowIron ));break;}
				case 319:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Copper ));break; }
				case 320:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Copper ));break;}
				case 321:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Copper ));break;}
				case 322:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Bronze ));break; }
				case 323:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Bronze ));break;}
				case 324:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Bronze ));break;}
				case 325:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Gold ));break; }
				case 326:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Gold ));break;}
				case 327:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Gold ));break;}
				case 328:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Agapite ));break;}
				case 329:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Agapite ));break; }
				case 330:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Agapite ));break;}
				case 331:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Verite ));break;}
				case 332:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Verite ));break; }
				case 333:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Verite ));break;}
				case 334:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Valorite ));break;}
				case 335:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Valorite ));break; }
				case 336:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(RingmailChest), 1025100, 5100, true, BulkMaterialType.Valorite ));break;}
				case 410:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.None ));break; }
				case 411:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.None ));break;}
				case 412:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.None ));break;}
				case 413:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.DullCopper ));break; }
				case 414:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.DullCopper ));break;}
				case 415:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.DullCopper ));break;}
				case 416:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.ShadowIron ));break; }
				case 417:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.ShadowIron ));break;}
				case 418:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.ShadowIron ));break;}
				case 419:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Copper ));break; }
				case 420:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Copper ));break;}
				case 421:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Copper ));break;}
				case 422:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Bronze ));break; }
				case 423:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Bronze ));break;}
				case 424:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Bronze ));break;}
				case 425:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Gold ));break; }
				case 426:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Gold ));break;}
				case 427:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Gold ));break;}
				case 428:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Agapite ));break;}
				case 429:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Agapite ));break; }
				case 430:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Agapite ));break;}
				case 431:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Verite ));break;}
				case 432:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Verite ));break; }
				case 433:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Verite ));break;}
				case 434:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Valorite ));break;}
				case 435:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Valorite ));break; }
				case 436:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainCoif), 1025051, 5051, true, BulkMaterialType.Valorite ));break;}
				case 510:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.None ));break; }
				case 511:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.None ));break;}
				case 512:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.None ));break;}
				case 513:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.DullCopper ));break; }
				case 514:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.DullCopper ));break;}
				case 515:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.DullCopper ));break;}
				case 516:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.ShadowIron ));break; }
				case 517:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.ShadowIron ));break;}
				case 518:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.ShadowIron ));break;}
				case 519:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Copper ));break; }
				case 520:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Copper ));break;}
				case 521:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Copper ));break;}
				case 522:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Bronze ));break; }
				case 523:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Bronze ));break;}
				case 524:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Bronze ));break;}
				case 525:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Gold ));break; }
				case 526:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Gold ));break;}
				case 527:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Gold ));break;}
				case 528:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Agapite ));break;}
				case 529:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Agapite ));break; }
				case 530:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Agapite ));break;}
				case 531:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Verite ));break;}
				case 532:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Verite ));break; }
				case 533:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Verite ));break;}
				case 534:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Valorite ));break;}
				case 535:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Valorite ));break; }
				case 536:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainLegs), 1025054, 5054, true, BulkMaterialType.Valorite ));break;}
				case 610:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.None ));break; }
				case 611:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.None ));break;}
				case 612:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.None ));break;}
				case 613:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.DullCopper ));break; }
				case 614:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.DullCopper ));break;}
				case 615:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.DullCopper ));break;}
				case 616:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.ShadowIron ));break; }
				case 617:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.ShadowIron ));break;}
				case 618:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.ShadowIron ));break;}
				case 619:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Copper ));break; }
				case 620:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Copper ));break;}
				case 621:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Copper ));break;}
				case 622:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Bronze ));break; }
				case 623:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Bronze ));break;}
				case 624:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Bronze ));break;}
				case 625:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Gold ));break; }
				case 626:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Gold ));break;}
				case 627:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Gold ));break;}
				case 628:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Agapite ));break;}
				case 629:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Agapite ));break; }
				case 630:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Agapite ));break;}
				case 631:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Verite ));break;}
				case 632:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Verite ));break; }
				case 633:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Verite ));break;}
				case 634:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Valorite ));break;}
				case 635:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Valorite ));break; }
				case 636:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ChainChest), 1025055, 5055, true, BulkMaterialType.Valorite ));break;}
				case 710:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.None ));break; }
				case 711:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.None ));break;}
				case 712:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.None ));break;}
				case 713:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.DullCopper ));break; }
				case 714:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.DullCopper ));break;}
				case 715:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.DullCopper ));break;}
				case 716:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.ShadowIron ));break; }
				case 717:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.ShadowIron ));break;}
				case 718:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.ShadowIron ));break;}
				case 719:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Copper ));break; }
				case 720:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Copper ));break;}
				case 721:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Copper ));break;}
				case 722:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Bronze ));break; }
				case 723:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Bronze ));break;}
				case 724:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Bronze ));break;}
				case 725:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Gold ));break; }
				case 726:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Gold ));break;}
				case 727:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Gold ));break;}
				case 728:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Agapite ));break;}
				case 729:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Agapite ));break; }
				case 730:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Agapite ));break;}
				case 731:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Verite ));break;}
				case 732:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Verite ));break; }
				case 733:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Verite ));break;}
				case 734:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Valorite ));break;}
				case 735:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Valorite ));break; }
				case 736:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateArms), 1025136, 5136, true, BulkMaterialType.Valorite ));break;}
				case 810:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.None ));break; }
				case 811:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.None ));break;}
				case 812:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.None ));break;}
				case 813:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.DullCopper ));break; }
				case 814:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.DullCopper ));break;}
				case 815:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.DullCopper ));break;}
				case 816:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.ShadowIron ));break; }
				case 817:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.ShadowIron ));break;}
				case 818:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.ShadowIron ));break;}
				case 819:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Copper ));break; }
				case 820:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Copper ));break;}
				case 821:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Copper ));break;}
				case 822:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Bronze ));break; }
				case 823:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Bronze ));break;}
				case 824:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Bronze ));break;}
				case 825:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Gold ));break; }
				case 826:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Gold ));break;}
				case 827:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Gold ));break;}
				case 828:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Agapite ));break;}
				case 829:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Agapite ));break; }
				case 830:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Agapite ));break;}
				case 831:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Verite ));break;}
				case 832:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Verite ));break; }
				case 833:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Verite ));break;}
				case 834:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Valorite ));break;}
				case 835:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Valorite ));break; }
				case 836:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGloves), 1025140, 5140, true, BulkMaterialType.Valorite ));break;}
				case 910:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.None ));break; }
				case 911:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.None ));break;}
				case 912:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.None ));break;}
				case 913:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.DullCopper ));break; }
				case 914:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.DullCopper ));break;}
				case 915:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.DullCopper ));break;}
				case 916:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.ShadowIron ));break; }
				case 917:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.ShadowIron ));break;}
				case 918:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.ShadowIron ));break;}
				case 919:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Copper ));break; }
				case 920:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Copper ));break;}
				case 921:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Copper ));break;}
				case 922:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Bronze ));break; }
				case 923:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Bronze ));break;}
				case 924:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Bronze ));break;}
				case 925:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Gold ));break; }
				case 926:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Gold ));break;}
				case 927:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Gold ));break;}
				case 928:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Agapite ));break;}
				case 929:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Agapite ));break; }
				case 930:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Agapite ));break;}
				case 931:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Verite ));break;}
				case 932:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Verite ));break; }
				case 933:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Verite ));break;}
				case 934:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Valorite ));break;}
				case 935:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Valorite ));break; }
				case 936:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateLegs), 1025137, 5137, true, BulkMaterialType.Valorite ));break;}
				case 1010:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.None ));break; }
				case 1011:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.None ));break;}
				case 1012:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.None ));break;}
				case 1013:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.DullCopper ));break; }
				case 1014:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.DullCopper ));break;}
				case 1015:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.DullCopper ));break;}
				case 1016:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.ShadowIron ));break; }
				case 1017:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.ShadowIron ));break;}
				case 1018:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.ShadowIron ));break;}
				case 1019:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Copper ));break; }
				case 1020:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Copper ));break;}
				case 1021:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Copper ));break;}
				case 1022:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Bronze ));break; }
				case 1023:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Bronze ));break;}
				case 1024:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Bronze ));break;}
				case 1025:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Gold ));break; }
				case 1026:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Gold ));break;}
				case 1027:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Gold ));break;}
				case 1028:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Agapite ));break;}
				case 1029:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Agapite ));break; }
				case 1030:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Agapite ));break;}
				case 1031:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Verite ));break;}
				case 1032:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Verite ));break; }
				case 1033:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Verite ));break;}
				case 1034:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Valorite ));break;}
				case 1035:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Valorite ));break; }
				case 1036:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateChest), 1025141, 5141, true, BulkMaterialType.Valorite ));break;}
				case 1110:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.None ));break; }
				case 1111:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.None ));break;}
				case 1112:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.None ));break;}
				case 1113:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.DullCopper ));break; }
				case 1114:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.DullCopper ));break;}
				case 1115:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.DullCopper ));break;}
				case 1116:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.ShadowIron ));break; }
				case 1117:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.ShadowIron ));break;}
				case 1118:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.ShadowIron ));break;}
				case 1119:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Copper ));break; }
				case 1120:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Copper ));break;}
				case 1121:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Copper ));break;}
				case 1122:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Bronze ));break; }
				case 1123:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Bronze ));break;}
				case 1124:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Bronze ));break;}
				case 1125:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Gold ));break; }
				case 1126:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Gold ));break;}
				case 1127:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Gold ));break;}
				case 1128:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Agapite ));break;}
				case 1129:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Agapite ));break; }
				case 1130:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Agapite ));break;}
				case 1131:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Verite ));break;}
				case 1132:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Verite ));break; }
				case 1133:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Verite ));break;}
				case 1134:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Valorite ));break;}
				case 1135:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Valorite ));break; }
				case 1136:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateGorget), 1025139, 5139, true, BulkMaterialType.Valorite ));break;}
				case 1210:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.None ));break; }
				case 1211:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.None ));break;}
				case 1212:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.None ));break;}
				case 1213:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.DullCopper ));break; }
				case 1214:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.DullCopper ));break;}
				case 1215:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.DullCopper ));break;}
				case 1216:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.ShadowIron ));break; }
				case 1217:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.ShadowIron ));break;}
				case 1218:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.ShadowIron ));break;}
				case 1219:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Copper ));break; }
				case 1220:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Copper ));break;}
				case 1221:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Copper ));break;}
				case 1222:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Bronze ));break; }
				case 1223:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Bronze ));break;}
				case 1224:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Bronze ));break;}
				case 1225:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Gold ));break; }
				case 1226:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Gold ));break;}
				case 1227:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Gold ));break;}
				case 1228:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Agapite ));break;}
				case 1229:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Agapite ));break; }
				case 1230:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Agapite ));break;}
				case 1231:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Verite ));break;}
				case 1232:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Verite ));break; }
				case 1233:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Verite ));break;}
				case 1234:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Valorite ));break;}
				case 1235:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Valorite ));break; }
				case 1236:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(PlateHelm), 1025138, 5138, true, BulkMaterialType.Valorite ));break;}
				case 1310:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.None ));break; }
				case 1311:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.None ));break;}
				case 1312:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.None ));break;}
				case 1313:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.DullCopper ));break; }
				case 1314:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.DullCopper ));break;}
				case 1315:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.DullCopper ));break;}
				case 1316:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.ShadowIron ));break; }
				case 1317:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.ShadowIron ));break;}
				case 1318:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.ShadowIron ));break;}
				case 1319:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Copper ));break; }
				case 1320:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Copper ));break;}
				case 1321:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Copper ));break;}
				case 1322:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Bronze ));break; }
				case 1323:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Bronze ));break;}
				case 1324:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Bronze ));break;}
				case 1325:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Gold ));break; }
				case 1326:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Gold ));break;}
				case 1327:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Gold ));break;}
				case 1328:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Agapite ));break;}
				case 1329:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Agapite ));break; }
				case 1330:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Agapite ));break;}
				case 1331:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Verite ));break;}
				case 1332:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Verite ));break; }
				case 1333:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Verite ));break;}
				case 1334:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Valorite ));break;}
				case 1335:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Valorite ));break; }
				case 1336:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(FemalePlateChest), 1027172, 7172, true, BulkMaterialType.Valorite ));break;}
				case 1410:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.None ));break; }
				case 1411:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.None ));break;}
				case 1412:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.None ));break;}
				case 1413:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.DullCopper ));break; }
				case 1414:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.DullCopper ));break;}
				case 1415:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.DullCopper ));break;}
				case 1416:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.ShadowIron ));break; }
				case 1417:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.ShadowIron ));break;}
				case 1418:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.ShadowIron ));break;}
				case 1419:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Copper ));break; }
				case 1420:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Copper ));break;}
				case 1421:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Copper ));break;}
				case 1422:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Bronze ));break; }
				case 1423:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Bronze ));break;}
				case 1424:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Bronze ));break;}
				case 1425:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Gold ));break; }
				case 1426:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Gold ));break;}
				case 1427:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Gold ));break;}
				case 1428:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Agapite ));break;}
				case 1429:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Agapite ));break; }
				case 1430:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Agapite ));break;}
				case 1431:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Verite ));break;}
				case 1432:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Verite ));break; }
				case 1433:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Verite ));break;}
				case 1434:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Valorite ));break;}
				case 1435:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Valorite ));break; }
				case 1436:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BronzeShield), 1027026, 7026, true, BulkMaterialType.Valorite ));break;}
				case 1510:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.None ));break; }
				case 1511:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.None ));break;}
				case 1512:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.None ));break;}
				case 1513:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.DullCopper ));break; }
				case 1514:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.DullCopper ));break;}
				case 1515:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.DullCopper ));break;}
				case 1516:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.ShadowIron ));break; }
				case 1517:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.ShadowIron ));break;}
				case 1518:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.ShadowIron ));break;}
				case 1519:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Copper ));break; }
				case 1520:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Copper ));break;}
				case 1521:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Copper ));break;}
				case 1522:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Bronze ));break; }
				case 1523:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Bronze ));break;}
				case 1524:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Bronze ));break;}
				case 1525:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Gold ));break; }
				case 1526:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Gold ));break;}
				case 1527:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Gold ));break;}
				case 1528:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Agapite ));break;}
				case 1529:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Agapite ));break; }
				case 1530:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Agapite ));break;}
				case 1531:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Verite ));break;}
				case 1532:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Verite ));break; }
				case 1533:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Verite ));break;}
				case 1534:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Valorite ));break;}
				case 1535:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Valorite ));break; }
				case 1536:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Buckler), 1027027, 7027, true, BulkMaterialType.Valorite ));break;}
				case 1610:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.None ));break; }
				case 1611:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.None ));break;}
				case 1612:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.None ));break;}
				case 1613:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.DullCopper ));break; }
				case 1614:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.DullCopper ));break;}
				case 1615:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.DullCopper ));break;}
				case 1616:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.ShadowIron ));break; }
				case 1617:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.ShadowIron ));break;}
				case 1618:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.ShadowIron ));break;}
				case 1619:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Copper ));break; }
				case 1620:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Copper ));break;}
				case 1621:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Copper ));break;}
				case 1622:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Bronze ));break; }
				case 1623:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Bronze ));break;}
				case 1624:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Bronze ));break;}
				case 1625:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Gold ));break; }
				case 1626:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Gold ));break;}
				case 1627:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Gold ));break;}
				case 1628:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Agapite ));break;}
				case 1629:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Agapite ));break; }
				case 1630:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Agapite ));break;}
				case 1631:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Verite ));break;}
				case 1632:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Verite ));break; }
				case 1633:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Verite ));break;}
				case 1634:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Valorite ));break;}
				case 1635:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Valorite ));break; }
				case 1636:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HeaterShield), 1027030, 7030, true, BulkMaterialType.Valorite ));break;}
				case 1710:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.None ));break; }
				case 1711:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.None ));break;}
				case 1712:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.None ));break;}
				case 1713:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.DullCopper ));break; }
				case 1714:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.DullCopper ));break;}
				case 1715:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.DullCopper ));break;}
				case 1716:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.ShadowIron ));break; }
				case 1717:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.ShadowIron ));break;}
				case 1718:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.ShadowIron ));break;}
				case 1719:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Copper ));break; }
				case 1720:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Copper ));break;}
				case 1721:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Copper ));break;}
				case 1722:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Bronze ));break; }
				case 1723:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Bronze ));break;}
				case 1724:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Bronze ));break;}
				case 1725:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Gold ));break; }
				case 1726:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Gold ));break;}
				case 1727:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Gold ));break;}
				case 1728:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Agapite ));break;}
				case 1729:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Agapite ));break; }
				case 1730:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Agapite ));break;}
				case 1731:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Verite ));break;}
				case 1732:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Verite ));break; }
				case 1733:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Verite ));break;}
				case 1734:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Valorite ));break;}
				case 1735:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Valorite ));break; }
				case 1736:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalKiteShield), 1027028, 7028, true, BulkMaterialType.Valorite ));break;}
				case 1810:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.None ));break; }
				case 1811:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.None ));break;}
				case 1812:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.None ));break;}
				case 1813:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.DullCopper ));break; }
				case 1814:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.DullCopper ));break;}
				case 1815:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.DullCopper ));break;}
				case 1816:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.ShadowIron ));break; }
				case 1817:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.ShadowIron ));break;}
				case 1818:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.ShadowIron ));break;}
				case 1819:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Copper ));break; }
				case 1820:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Copper ));break;}
				case 1821:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Copper ));break;}
				case 1822:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Bronze ));break; }
				case 1823:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Bronze ));break;}
				case 1824:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Bronze ));break;}
				case 1825:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Gold ));break; }
				case 1826:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Gold ));break;}
				case 1827:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Gold ));break;}
				case 1828:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Agapite ));break;}
				case 1829:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Agapite ));break; }
				case 1830:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Agapite ));break;}
				case 1831:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Verite ));break;}
				case 1832:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Verite ));break; }
				case 1833:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Verite ));break;}
				case 1834:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Valorite ));break;}
				case 1835:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Valorite ));break; }
				case 1836:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(MetalShield), 1027035, 7035, true, BulkMaterialType.Valorite ));break;}
				case 1910:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.None ));break; }
				case 1911:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.None ));break;}
				case 1912:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.None ));break;}
				case 1913:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.DullCopper ));break; }
				case 1914:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.DullCopper ));break;}
				case 1915:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.DullCopper ));break;}
				case 1916:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.ShadowIron ));break; }
				case 1917:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.ShadowIron ));break;}
				case 1918:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.ShadowIron ));break;}
				case 1919:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Copper ));break; }
				case 1920:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Copper ));break;}
				case 1921:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Copper ));break;}
				case 1922:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Bronze ));break; }
				case 1923:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Bronze ));break;}
				case 1924:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Bronze ));break;}
				case 1925:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Gold ));break; }
				case 1926:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Gold ));break;}
				case 1927:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Gold ));break;}
				case 1928:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Agapite ));break;}
				case 1929:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Agapite ));break; }
				case 1930:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Agapite ));break;}
				case 1931:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Verite ));break;}
				case 1932:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Verite ));break; }
				case 1933:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Verite ));break;}
				case 1934:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Valorite ));break;}
				case 1935:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Valorite ));break; }
				case 1936:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WoodenKiteShield), 1027032, 7032, true, BulkMaterialType.Valorite ));break;}
				case 2010:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Axe), 1023913, 3913, true, BulkMaterialType.None ));break; }
				case 2011:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Axe), 1023913, 3913, true, BulkMaterialType.None ));break;}
				case 2012:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Axe), 1023913, 3913, true, BulkMaterialType.None ));break;}
				case 2013:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Axe), 1023913, 3913, true, BulkMaterialType.DullCopper ));break; }
				case 2014:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Axe), 1023913, 3913, true, BulkMaterialType.DullCopper ));break;}
				case 2015:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Axe), 1023913, 3913, true, BulkMaterialType.DullCopper ));break;}
				case 2016:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Axe), 1023913, 3913, true, BulkMaterialType.ShadowIron ));break; }
				case 2017:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Axe), 1023913, 3913, true, BulkMaterialType.ShadowIron ));break;}
				case 2018:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Axe), 1023913, 3913, true, BulkMaterialType.ShadowIron ));break;}
				case 2019:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Copper ));break; }
				case 2020:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Copper ));break;}
				case 2021:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Copper ));break;}
				case 2022:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Bronze ));break; }
				case 2023:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Bronze ));break;}
				case 2024:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Bronze ));break;}
				case 2025:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Gold ));break; }
				case 2026:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Gold ));break;}
				case 2027:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Gold ));break;}
				case 2028:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Agapite ));break;}
				case 2029:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Agapite ));break; }
				case 2030:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Agapite ));break;}
				case 2031:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Verite ));break;}
				case 2032:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Verite ));break; }
				case 2033:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Verite ));break;}
				case 2034:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Valorite ));break;}
				case 2035:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Valorite ));break; }
				case 2036:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Axe), 1023913, 3913, true, BulkMaterialType.Valorite ));break;}
				case 2110:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.None ));break; }
				case 2111:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.None ));break;}
				case 2112:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.None ));break;}
				case 2113:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.DullCopper ));break; }
				case 2114:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.DullCopper ));break;}
				case 2115:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.DullCopper ));break;}
				case 2116:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.ShadowIron ));break; }
				case 2117:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.ShadowIron ));break;}
				case 2118:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.ShadowIron ));break;}
				case 2119:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Copper ));break; }
				case 2120:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Copper ));break;}
				case 2121:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Copper ));break;}
				case 2122:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Bronze ));break; }
				case 2123:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Bronze ));break;}
				case 2124:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Bronze ));break;}
				case 2125:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Gold ));break; }
				case 2126:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Gold ));break;}
				case 2127:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Gold ));break;}
				case 2128:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Agapite ));break;}
				case 2129:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Agapite ));break; }
				case 2130:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Agapite ));break;}
				case 2131:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Verite ));break;}
				case 2132:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Verite ));break; }
				case 2133:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Verite ));break;}
				case 2134:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Valorite ));break;}
				case 2135:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Valorite ));break; }
				case 2136:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(BattleAxe), 1023911, 3911, true, BulkMaterialType.Valorite ));break;}
				case 2210:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.None ));break; }
				case 2211:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.None ));break;}
				case 2212:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.None ));break;}
				case 2213:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.DullCopper ));break; }
				case 2214:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.DullCopper ));break;}
				case 2215:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.DullCopper ));break;}
				case 2216:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.ShadowIron ));break; }
				case 2217:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.ShadowIron ));break;}
				case 2218:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.ShadowIron ));break;}
				case 2219:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Copper ));break; }
				case 2220:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Copper ));break;}
				case 2221:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Copper ));break;}
				case 2222:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Bronze ));break; }
				case 2223:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Bronze ));break;}
				case 2224:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Bronze ));break;}
				case 2225:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Gold ));break; }
				case 2226:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Gold ));break;}
				case 2227:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Gold ));break;}
				case 2228:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Agapite ));break;}
				case 2229:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Agapite ));break; }
				case 2230:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Agapite ));break;}
				case 2231:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Verite ));break;}
				case 2232:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Verite ));break; }
				case 2233:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Verite ));break;}
				case 2234:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Valorite ));break;}
				case 2235:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Valorite ));break; }
				case 2236:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(DoubleAxe), 1023915, 3915, true, BulkMaterialType.Valorite ));break;}
				case 2310:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.None ));break; }
				case 2311:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.None ));break;}
				case 2312:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.None ));break;}
				case 2313:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.DullCopper ));break; }
				case 2314:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.DullCopper ));break;}
				case 2315:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.DullCopper ));break;}
				case 2316:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.ShadowIron ));break; }
				case 2317:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.ShadowIron ));break;}
				case 2318:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.ShadowIron ));break;}
				case 2319:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Copper ));break; }
				case 2320:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Copper ));break;}
				case 2321:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Copper ));break;}
				case 2322:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Bronze ));break; }
				case 2323:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Bronze ));break;}
				case 2324:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Bronze ));break;}
				case 2325:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Gold ));break; }
				case 2326:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Gold ));break;}
				case 2327:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Gold ));break;}
				case 2328:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Agapite ));break;}
				case 2329:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Agapite ));break; }
				case 2330:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Agapite ));break;}
				case 2331:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Verite ));break;}
				case 2332:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Verite ));break; }
				case 2333:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Verite ));break;}
				case 2334:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Valorite ));break;}
				case 2335:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Valorite ));break; }
				case 2336:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(LargeBattleAxe), 1025115, 5115, true, BulkMaterialType.Valorite ));break;}
				case 2410:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.None ));break; }
				case 2411:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.None ));break;}
				case 2412:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.None ));break;}
				case 2413:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.DullCopper ));break; }
				case 2414:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.DullCopper ));break;}
				case 2415:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.DullCopper ));break;}
				case 2416:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.ShadowIron ));break; }
				case 2417:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.ShadowIron ));break;}
				case 2418:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.ShadowIron ));break;}
				case 2419:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Copper ));break; }
				case 2420:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Copper ));break;}
				case 2421:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Copper ));break;}
				case 2422:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Bronze ));break; }
				case 2423:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Bronze ));break;}
				case 2424:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Bronze ));break;}
				case 2425:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Gold ));break; }
				case 2426:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Gold ));break;}
				case 2427:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Gold ));break;}
				case 2428:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Agapite ));break;}
				case 2429:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Agapite ));break; }
				case 2430:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Agapite ));break;}
				case 2431:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Verite ));break;}
				case 2432:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Verite ));break; }
				case 2433:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Verite ));break;}
				case 2434:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Valorite ));break;}
				case 2435:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Valorite ));break; }
				case 2436:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(TwoHandedAxe), 1025187, 5187, true, BulkMaterialType.Valorite ));break;}
				case 2510:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.None ));break; }
				case 2511:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.None ));break;}
				case 2512:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.None ));break;}
				case 2513:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.DullCopper ));break; }
				case 2514:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.DullCopper ));break;}
				case 2515:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.DullCopper ));break;}
				case 2516:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.ShadowIron ));break; }
				case 2517:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.ShadowIron ));break;}
				case 2518:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.ShadowIron ));break;}
				case 2519:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Copper ));break; }
				case 2520:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Copper ));break;}
				case 2521:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Copper ));break;}
				case 2522:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Bronze ));break; }
				case 2523:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Bronze ));break;}
				case 2524:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Bronze ));break;}
				case 2525:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Gold ));break; }
				case 2526:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Gold ));break;}
				case 2527:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Gold ));break;}
				case 2528:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Agapite ));break;}
				case 2529:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Agapite ));break; }
				case 2530:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Agapite ));break;}
				case 2531:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Verite ));break;}
				case 2532:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Verite ));break; }
				case 2533:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Verite ));break;}
				case 2534:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Valorite ));break;}
				case 2535:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Valorite ));break; }
				case 2536:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarAxe), 1025040, 5040, true, BulkMaterialType.Valorite ));break;}
				case 2610:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.None ));break; }
				case 2611:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.None ));break;}
				case 2612:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.None ));break;}
				case 2613:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.DullCopper ));break; }
				case 2614:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.DullCopper ));break;}
				case 2615:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.DullCopper ));break;}
				case 2616:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.ShadowIron ));break; }
				case 2617:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.ShadowIron ));break;}
				case 2618:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.ShadowIron ));break;}
				case 2619:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Copper ));break; }
				case 2620:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Copper ));break;}
				case 2621:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Copper ));break;}
				case 2622:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Bronze ));break; }
				case 2623:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Bronze ));break;}
				case 2624:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Bronze ));break;}
				case 2625:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Gold ));break; }
				case 2626:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Gold ));break;}
				case 2627:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Gold ));break;}
				case 2628:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Agapite ));break;}
				case 2629:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Agapite ));break; }
				case 2630:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Agapite ));break;}
				case 2631:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Verite ));break;}
				case 2632:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Verite ));break; }
				case 2633:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Verite ));break;}
				case 2634:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Valorite ));break;}
				case 2635:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Valorite ));break; }
				case 2636:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(HammerPick), 1025181, 5181, true, BulkMaterialType.Valorite ));break;}
				case 2710:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Mace), 1023932, 3932, true, BulkMaterialType.None ));break; }
				case 2711:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Mace), 1023932, 3932, true, BulkMaterialType.None ));break;}
				case 2712:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Mace), 1023932, 3932, true, BulkMaterialType.None ));break;}
				case 2713:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Mace), 1023932, 3932, true, BulkMaterialType.DullCopper ));break; }
				case 2714:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Mace), 1023932, 3932, true, BulkMaterialType.DullCopper ));break;}
				case 2715:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Mace), 1023932, 3932, true, BulkMaterialType.DullCopper ));break;}
				case 2716:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Mace), 1023932, 3932, true, BulkMaterialType.ShadowIron ));break; }
				case 2717:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Mace), 1023932, 3932, true, BulkMaterialType.ShadowIron ));break;}
				case 2718:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Mace), 1023932, 3932, true, BulkMaterialType.ShadowIron ));break;}
				case 2719:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Copper ));break; }
				case 2720:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Copper ));break;}
				case 2721:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Copper ));break;}
				case 2722:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Bronze ));break; }
				case 2723:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Bronze ));break;}
				case 2724:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Bronze ));break;}
				case 2725:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Gold ));break; }
				case 2726:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Gold ));break;}
				case 2727:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Gold ));break;}
				case 2728:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Agapite ));break;}
				case 2729:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Agapite ));break; }
				case 2730:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Agapite ));break;}
				case 2731:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Verite ));break;}
				case 2732:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Verite ));break; }
				case 2733:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Verite ));break;}
				case 2734:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Valorite ));break;}
				case 2735:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Valorite ));break; }
				case 2736:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Mace), 1023932, 3932, true, BulkMaterialType.Valorite ));break;}
				case 2810:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Maul), 1025179, 5179, true, BulkMaterialType.None ));break; }
				case 2811:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Maul), 1025179, 5179, true, BulkMaterialType.None ));break;}
				case 2812:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Maul), 1025179, 5179, true, BulkMaterialType.None ));break;}
				case 2813:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Maul), 1025179, 5179, true, BulkMaterialType.DullCopper ));break; }
				case 2814:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Maul), 1025179, 5179, true, BulkMaterialType.DullCopper ));break;}
				case 2815:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Maul), 1025179, 5179, true, BulkMaterialType.DullCopper ));break;}
				case 2816:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Maul), 1025179, 5179, true, BulkMaterialType.ShadowIron ));break; }
				case 2817:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Maul), 1025179, 5179, true, BulkMaterialType.ShadowIron ));break;}
				case 2818:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Maul), 1025179, 5179, true, BulkMaterialType.ShadowIron ));break;}
				case 2819:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Copper ));break; }
				case 2820:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Copper ));break;}
				case 2821:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Copper ));break;}
				case 2822:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Bronze ));break; }
				case 2823:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Bronze ));break;}
				case 2824:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Bronze ));break;}
				case 2825:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Gold ));break; }
				case 2826:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Gold ));break;}
				case 2827:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Gold ));break;}
				case 2828:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Agapite ));break;}
				case 2829:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Agapite ));break; }
				case 2830:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Agapite ));break;}
				case 2831:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Verite ));break;}
				case 2832:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Verite ));break; }
				case 2833:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Verite ));break;}
				case 2834:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Valorite ));break;}
				case 2835:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Valorite ));break; }
				case 2836:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Maul), 1025179, 5179, true, BulkMaterialType.Valorite ));break;}
				case 2910:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.None ));break; }
				case 2911:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.None ));break;}
				case 2912:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.None ));break;}
				case 2913:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.DullCopper ));break; }
				case 2914:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.DullCopper ));break;}
				case 2915:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.DullCopper ));break;}
				case 2916:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.ShadowIron ));break; }
				case 2917:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.ShadowIron ));break;}
				case 2918:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.ShadowIron ));break;}
				case 2919:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Copper ));break; }
				case 2920:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Copper ));break;}
				case 2921:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Copper ));break;}
				case 2922:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Bronze ));break; }
				case 2923:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Bronze ));break;}
				case 2924:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Bronze ));break;}
				case 2925:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Gold ));break; }
				case 2926:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Gold ));break;}
				case 2927:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Gold ));break;}
				case 2928:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Agapite ));break;}
				case 2929:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Agapite ));break; }
				case 2930:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Agapite ));break;}
				case 2931:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Verite ));break;}
				case 2932:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Verite ));break; }
				case 2933:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Verite ));break;}
				case 2934:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Valorite ));break;}
				case 2935:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Valorite ));break; }
				case 2936:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarHammer), 1025177, 5177, true, BulkMaterialType.Valorite ));break;}
				case 3010:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.None ));break; }
				case 3011:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.None ));break;}
				case 3012:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.None ));break;}
				case 3013:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.DullCopper ));break; }
				case 3014:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.DullCopper ));break;}
				case 3015:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.DullCopper ));break;}
				case 3016:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.ShadowIron ));break; }
				case 3017:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.ShadowIron ));break;}
				case 3018:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.ShadowIron ));break;}
				case 3019:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Copper ));break; }
				case 3020:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Copper ));break;}
				case 3021:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Copper ));break;}
				case 3022:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Bronze ));break; }
				case 3023:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Bronze ));break;}
				case 3024:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Bronze ));break;}
				case 3025:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Gold ));break; }
				case 3026:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Gold ));break;}
				case 3027:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Gold ));break;}
				case 3028:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Agapite ));break;}
				case 3029:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Agapite ));break; }
				case 3030:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Agapite ));break;}
				case 3031:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Verite ));break;}
				case 3032:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Verite ));break; }
				case 3033:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Verite ));break;}
				case 3034:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Valorite ));break;}
				case 3035:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Valorite ));break; }
				case 3036:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarMace), 1025127, 5127, true, BulkMaterialType.Valorite ));break;}
				case 3110:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.None ));break; }
				case 3111:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.None ));break;}
				case 3112:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.None ));break;}
				case 3113:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.DullCopper ));break; }
				case 3114:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.DullCopper ));break;}
				case 3115:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.DullCopper ));break;}
				case 3116:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.ShadowIron ));break; }
				case 3117:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.ShadowIron ));break;}
				case 3118:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.ShadowIron ));break;}
				case 3119:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Copper ));break; }
				case 3120:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Copper ));break;}
				case 3121:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Copper ));break;}
				case 3122:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Bronze ));break; }
				case 3123:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Bronze ));break;}
				case 3124:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Bronze ));break;}
				case 3125:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Gold ));break; }
				case 3126:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Gold ));break;}
				case 3127:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Gold ));break;}
				case 3128:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Agapite ));break;}
				case 3129:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Agapite ));break; }
				case 3130:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Agapite ));break;}
				case 3131:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Verite ));break;}
				case 3132:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Verite ));break; }
				case 3133:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Verite ));break;}
				case 3134:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Valorite ));break;}
				case 3135:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Valorite ));break; }
				case 3136:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Broadsword), 1023934, 3934, true, BulkMaterialType.Valorite ));break;}
				case 3210:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.None ));break; }
				case 3211:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.None ));break;}
				case 3212:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.None ));break;}
				case 3213:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.DullCopper ));break; }
				case 3214:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.DullCopper ));break;}
				case 3215:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.DullCopper ));break;}
				case 3216:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.ShadowIron ));break; }
				case 3217:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.ShadowIron ));break;}
				case 3218:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.ShadowIron ));break;}
				case 3219:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Copper ));break; }
				case 3220:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Copper ));break;}
				case 3221:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Copper ));break;}
				case 3222:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Bronze ));break; }
				case 3223:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Bronze ));break;}
				case 3224:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Bronze ));break;}
				case 3225:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Gold ));break; }
				case 3226:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Gold ));break;}
				case 3227:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Gold ));break;}
				case 3228:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Agapite ));break;}
				case 3229:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Agapite ));break; }
				case 3230:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Agapite ));break;}
				case 3231:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Verite ));break;}
				case 3232:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Verite ));break; }
				case 3233:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Verite ));break;}
				case 3234:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Valorite ));break;}
				case 3235:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Valorite ));break; }
				case 3236:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Cutlass), 1025185, 5185, true, BulkMaterialType.Valorite ));break;}
				case 3310:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Katana), 1025119, 5119, true, BulkMaterialType.None ));break; }
				case 3311:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Katana), 1025119, 5119, true, BulkMaterialType.None ));break;}
				case 3312:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Katana), 1025119, 5119, true, BulkMaterialType.None ));break;}
				case 3313:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Katana), 1025119, 5119, true, BulkMaterialType.DullCopper ));break; }
				case 3314:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Katana), 1025119, 5119, true, BulkMaterialType.DullCopper ));break;}
				case 3315:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Katana), 1025119, 5119, true, BulkMaterialType.DullCopper ));break;}
				case 3316:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Katana), 1025119, 5119, true, BulkMaterialType.ShadowIron ));break; }
				case 3317:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Katana), 1025119, 5119, true, BulkMaterialType.ShadowIron ));break;}
				case 3318:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Katana), 1025119, 5119, true, BulkMaterialType.ShadowIron ));break;}
				case 3319:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Copper ));break; }
				case 3320:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Copper ));break;}
				case 3321:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Copper ));break;}
				case 3322:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Bronze ));break; }
				case 3323:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Bronze ));break;}
				case 3324:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Bronze ));break;}
				case 3325:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Gold ));break; }
				case 3326:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Gold ));break;}
				case 3327:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Gold ));break;}
				case 3328:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Agapite ));break;}
				case 3329:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Agapite ));break; }
				case 3330:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Agapite ));break;}
				case 3331:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Verite ));break;}
				case 3332:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Verite ));break; }
				case 3333:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Verite ));break;}
				case 3334:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Valorite ));break;}
				case 3335:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Valorite ));break; }
				case 3336:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Katana), 1025119, 5119, true, BulkMaterialType.Valorite ));break;}
				case 3410:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.None ));break; }
				case 3411:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.None ));break;}
				case 3412:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.None ));break;}
				case 3413:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.DullCopper ));break; }
				case 3414:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.DullCopper ));break;}
				case 3415:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.DullCopper ));break;}
				case 3416:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.ShadowIron ));break; }
				case 3417:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.ShadowIron ));break;}
				case 3418:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.ShadowIron ));break;}
				case 3419:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Copper ));break; }
				case 3420:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Copper ));break;}
				case 3421:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Copper ));break;}
				case 3422:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Bronze ));break; }
				case 3423:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Bronze ));break;}
				case 3424:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Bronze ));break;}
				case 3425:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Gold ));break; }
				case 3426:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Gold ));break;}
				case 3427:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Gold ));break;}
				case 3428:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Agapite ));break;}
				case 3429:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Agapite ));break; }
				case 3430:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Agapite ));break;}
				case 3431:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Verite ));break;}
				case 3432:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Verite ));break; }
				case 3433:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Verite ));break;}
				case 3434:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Valorite ));break;}
				case 3435:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Valorite ));break; }
				case 3436:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Longsword), 1023937, 3937, true, BulkMaterialType.Valorite ));break;}
				case 3510:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.None ));break; }
				case 3511:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.None ));break;}
				case 3512:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.None ));break;}
				case 3513:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.DullCopper ));break; }
				case 3514:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.DullCopper ));break;}
				case 3515:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.DullCopper ));break;}
				case 3516:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.ShadowIron ));break; }
				case 3517:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.ShadowIron ));break;}
				case 3518:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.ShadowIron ));break;}
				case 3519:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Copper ));break; }
				case 3520:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Copper ));break;}
				case 3521:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Copper ));break;}
				case 3522:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Bronze ));break; }
				case 3523:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Bronze ));break;}
				case 3524:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Bronze ));break;}
				case 3525:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Gold ));break; }
				case 3526:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Gold ));break;}
				case 3527:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Gold ));break;}
				case 3528:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Agapite ));break;}
				case 3529:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Agapite ));break; }
				case 3530:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Agapite ));break;}
				case 3531:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Verite ));break;}
				case 3532:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Verite ));break; }
				case 3533:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Verite ));break;}
				case 3534:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Valorite ));break;}
				case 3535:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Valorite ));break; }
				case 3536:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Scimitar), 1025046, 5046, true, BulkMaterialType.Valorite ));break;}
				case 3610:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.None ));break; }
				case 3611:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.None ));break;}
				case 3612:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.None ));break;}
				case 3613:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.DullCopper ));break; }
				case 3614:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.DullCopper ));break;}
				case 3615:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.DullCopper ));break;}
				case 3616:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.ShadowIron ));break; }
				case 3617:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.ShadowIron ));break;}
				case 3618:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.ShadowIron ));break;}
				case 3619:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Copper ));break; }
				case 3620:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Copper ));break;}
				case 3621:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Copper ));break;}
				case 3622:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Bronze ));break; }
				case 3623:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Bronze ));break;}
				case 3624:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Bronze ));break;}
				case 3625:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Gold ));break; }
				case 3626:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Gold ));break;}
				case 3627:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Gold ));break;}
				case 3628:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Agapite ));break;}
				case 3629:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Agapite ));break; }
				case 3630:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Agapite ));break;}
				case 3631:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Verite ));break;}
				case 3632:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Verite ));break; }
				case 3633:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Verite ));break;}
				case 3634:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Valorite ));break;}
				case 3635:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Valorite ));break; }
				case 3636:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(VikingSword), 1025049, 5049, true, BulkMaterialType.Valorite ));break;}
				case 3710:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.None ));break; }
				case 3711:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.None ));break;}
				case 3712:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.None ));break;}
				case 3713:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.DullCopper ));break; }
				case 3714:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.DullCopper ));break;}
				case 3715:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.DullCopper ));break;}
				case 3716:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.ShadowIron ));break; }
				case 3717:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.ShadowIron ));break;}
				case 3718:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.ShadowIron ));break;}
				case 3719:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Copper ));break; }
				case 3720:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Copper ));break;}
				case 3721:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Copper ));break;}
				case 3722:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Bronze ));break; }
				case 3723:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Bronze ));break;}
				case 3724:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Bronze ));break;}
				case 3725:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Gold ));break; }
				case 3726:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Gold ));break;}
				case 3727:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Gold ));break;}
				case 3728:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Agapite ));break;}
				case 3729:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Agapite ));break; }
				case 3730:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Agapite ));break;}
				case 3731:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Verite ));break;}
				case 3732:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Verite ));break; }
				case 3733:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Verite ));break;}
				case 3734:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Valorite ));break;}
				case 3735:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Valorite ));break; }
				case 3736:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Dagger), 1023922, 3922, true, BulkMaterialType.Valorite ));break;}
				case 3810:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.None ));break; }
				case 3811:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.None ));break;}
				case 3812:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.None ));break;}
				case 3813:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.DullCopper ));break; }
				case 3814:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.DullCopper ));break;}
				case 3815:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.DullCopper ));break;}
				case 3816:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.ShadowIron ));break; }
				case 3817:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.ShadowIron ));break;}
				case 3818:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.ShadowIron ));break;}
				case 3819:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Copper ));break; }
				case 3820:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Copper ));break;}
				case 3821:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Copper ));break;}
				case 3822:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Bronze ));break; }
				case 3823:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Bronze ));break;}
				case 3824:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Bronze ));break;}
				case 3825:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Gold ));break; }
				case 3826:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Gold ));break;}
				case 3827:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Gold ));break;}
				case 3828:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Agapite ));break;}
				case 3829:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Agapite ));break; }
				case 3830:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Agapite ));break;}
				case 3831:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Verite ));break;}
				case 3832:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Verite ));break; }
				case 3833:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Verite ));break;}
				case 3834:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Valorite ));break;}
				case 3835:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Valorite ));break; }
				case 3836:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(ShortSpear), 1025123, 5123, true, BulkMaterialType.Valorite ));break;}
				case 3910:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Spear), 1023938, 3938, true, BulkMaterialType.None ));break; }
				case 3911:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Spear), 1023938, 3938, true, BulkMaterialType.None ));break;}
				case 3912:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Spear), 1023938, 3938, true, BulkMaterialType.None ));break;}
				case 3913:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Spear), 1023938, 3938, true, BulkMaterialType.DullCopper ));break; }
				case 3914:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Spear), 1023938, 3938, true, BulkMaterialType.DullCopper ));break;}
				case 3915:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Spear), 1023938, 3938, true, BulkMaterialType.DullCopper ));break;}
				case 3916:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Spear), 1023938, 3938, true, BulkMaterialType.ShadowIron ));break; }
				case 3917:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Spear), 1023938, 3938, true, BulkMaterialType.ShadowIron ));break;}
				case 3918:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Spear), 1023938, 3938, true, BulkMaterialType.ShadowIron ));break;}
				case 3919:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Copper ));break; }
				case 3920:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Copper ));break;}
				case 3921:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Copper ));break;}
				case 3922:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Bronze ));break; }
				case 3923:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Bronze ));break;}
				case 3924:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Bronze ));break;}
				case 3925:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Gold ));break; }
				case 3926:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Gold ));break;}
				case 3927:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Gold ));break;}
				case 3928:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Agapite ));break;}
				case 3929:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Agapite ));break; }
				case 3930:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Agapite ));break;}
				case 3931:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Verite ));break;}
				case 3932:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Verite ));break; }
				case 3933:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Verite ));break;}
				case 3934:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Valorite ));break;}
				case 3935:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Valorite ));break; }
				case 3936:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Spear), 1023938, 3938, true, BulkMaterialType.Valorite ));break;}
				case 4010:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.None ));break; }
				case 4011:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.None ));break;}
				case 4012:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.None ));break;}
				case 4013:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.DullCopper ));break; }
				case 4014:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.DullCopper ));break;}
				case 4015:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.DullCopper ));break;}
				case 4016:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.ShadowIron ));break; }
				case 4017:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.ShadowIron ));break;}
				case 4018:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.ShadowIron ));break;}
				case 4019:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Copper ));break; }
				case 4020:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Copper ));break;}
				case 4021:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Copper ));break;}
				case 4022:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Bronze ));break; }
				case 4023:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Bronze ));break;}
				case 4024:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Bronze ));break;}
				case 4025:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Gold ));break; }
				case 4026:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Gold ));break;}
				case 4027:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Gold ));break;}
				case 4028:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Agapite ));break;}
				case 4029:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Agapite ));break; }
				case 4030:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Agapite ));break;}
				case 4031:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Verite ));break;}
				case 4032:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Verite ));break; }
				case 4033:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Verite ));break;}
				case 4034:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Valorite ));break;}
				case 4035:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Valorite ));break; }
				case 4036:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(WarFork), 1025125, 5125, true, BulkMaterialType.Valorite ));break;}
				case 4110:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.None ));break; }
				case 4111:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.None ));break;}
				case 4112:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.None ));break;}
				case 4113:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.DullCopper ));break; }
				case 4114:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.DullCopper ));break;}
				case 4115:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.DullCopper ));break;}
				case 4116:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.ShadowIron ));break; }
				case 4117:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.ShadowIron ));break;}
				case 4118:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.ShadowIron ));break;}
				case 4119:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Copper ));break; }
				case 4120:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Copper ));break;}
				case 4121:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Copper ));break;}
				case 4122:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Bronze ));break; }
				case 4123:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Bronze ));break;}
				case 4124:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Bronze ));break;}
				case 4125:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Gold ));break; }
				case 4126:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Gold ));break;}
				case 4127:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Gold ));break;}
				case 4128:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Agapite ));break;}
				case 4129:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Agapite ));break; }
				case 4130:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Agapite ));break;}
				case 4131:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Verite ));break;}
				case 4132:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Verite ));break; }
				case 4133:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Verite ));break;}
				case 4134:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Valorite ));break;}
				case 4135:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Valorite ));break; }
				case 4136:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Kryss), 1025121, 5121, true, BulkMaterialType.Valorite ));break;}
				case 4210:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.None ));break; }
				case 4211:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.None ));break;}
				case 4212:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.None ));break;}
				case 4213:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.DullCopper ));break; }
				case 4214:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.DullCopper ));break;}
				case 4215:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.DullCopper ));break;}
				case 4216:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.ShadowIron ));break; }
				case 4217:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.ShadowIron ));break;}
				case 4218:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.ShadowIron ));break;}
				case 4219:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Copper ));break; }
				case 4220:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Copper ));break;}
				case 4221:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Copper ));break;}
				case 4222:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Bronze ));break; }
				case 4223:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Bronze ));break;}
				case 4224:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Bronze ));break;}
				case 4225:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Gold ));break; }
				case 4226:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Gold ));break;}
				case 4227:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Gold ));break;}
				case 4228:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Agapite ));break;}
				case 4229:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Agapite ));break; }
				case 4230:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Agapite ));break;}
				case 4231:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Verite ));break;}
				case 4232:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Verite ));break; }
				case 4233:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Verite ));break;}
				case 4234:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Valorite ));break;}
				case 4235:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Valorite ));break; }
				case 4236:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Bardiche), 1023917, 3917, true, BulkMaterialType.Valorite ));break;}
				case 4310:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.None ));break; }
				case 4311:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.None ));break;}
				case 4312:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.None ));break;}
				case 4313:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.DullCopper ));break; }
				case 4314:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.DullCopper ));break;}
				case 4315:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.DullCopper ));break;}
				case 4316:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.ShadowIron ));break; }
				case 4317:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.ShadowIron ));break;}
				case 4318:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.ShadowIron ));break;}
				case 4319:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Copper ));break; }
				case 4320:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Copper ));break;}
				case 4321:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Copper ));break;}
				case 4322:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Bronze ));break; }
				case 4323:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Bronze ));break;}
				case 4324:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Bronze ));break;}
				case 4325:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Gold ));break; }
				case 4326:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Gold ));break;}
				case 4327:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Gold ));break;}
				case 4328:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Agapite ));break;}
				case 4329:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Agapite ));break; }
				case 4330:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Agapite ));break;}
				case 4331:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Verite ));break;}
				case 4332:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Verite ));break; }
				case 4333:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Verite ));break;}
				case 4334:{from.AddToBackpack(new SmallSmithBOD( 0, 10, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Valorite ));break;}
				case 4335:{from.AddToBackpack(new SmallSmithBOD( 0, 15, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Valorite ));break; }
				case 4336:{from.AddToBackpack(new SmallSmithBOD( 0, 20, typeof(Halberd), 1025182, 5182, true, BulkMaterialType.Valorite ));break;}
				case 4410:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherGorget), 1025063, 5063, true, BulkMaterialType.None ));break; }
				case 4411:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherGorget), 1025063, 5063, true, BulkMaterialType.None ));break;}
				case 4412:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherGorget), 1025063, 5063, true, BulkMaterialType.None ));break;}
				case 4413:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherGorget), 1025063, 5063, true, BulkMaterialType.Spined ));break; }
				case 4414:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherGorget), 1025063, 5063, true, BulkMaterialType.Spined ));break;}
				case 4415:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherGorget), 1025063, 5063, true, BulkMaterialType.Spined ));break;}
				case 4416:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherGorget), 1025063, 5063, true, BulkMaterialType.Horned ));break; }
				case 4417:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherGorget), 1025063, 5063, true, BulkMaterialType.Horned ));break;}
				case 4418:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherGorget), 1025063, 5063, true, BulkMaterialType.Horned ));break;}
				case 4419:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherGorget), 1025063, 5063, true, BulkMaterialType.Barbed ));break; }
				case 4420:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherGorget), 1025063, 5063, true, BulkMaterialType.Barbed ));break;}
				case 4421:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherGorget), 1025063, 5063, true, BulkMaterialType.Barbed ));break;}
				case 4510:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherCap), 1027609, 7609, true, BulkMaterialType.None ));break; }
				case 4511:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherCap), 1027609, 7609, true, BulkMaterialType.None ));break;}
				case 4512:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherCap), 1027609, 7609, true, BulkMaterialType.None ));break;}
				case 4513:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherCap), 1027609, 7609, true, BulkMaterialType.Spined ));break; }
				case 4514:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherCap), 1027609, 7609, true, BulkMaterialType.Spined ));break;}
				case 4515:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherCap), 1027609, 7609, true, BulkMaterialType.Spined ));break;}
				case 4516:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherCap), 1027609, 7609, true, BulkMaterialType.Horned ));break; }
				case 4517:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherCap), 1027609, 7609, true, BulkMaterialType.Horned ));break;}
				case 4518:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherCap), 1027609, 7609, true, BulkMaterialType.Horned ));break;}
				case 4519:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherCap), 1027609, 7609, true, BulkMaterialType.Barbed ));break; }
				case 4520:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherCap), 1027609, 7609, true, BulkMaterialType.Barbed ));break;}
				case 4521:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherCap), 1027609, 7609, true, BulkMaterialType.Barbed ));break;}
				case 4610:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherGloves), 1025062, 5062, true, BulkMaterialType.None ));break; }
				case 4611:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherGloves), 1025062, 5062, true, BulkMaterialType.None ));break;}
				case 4612:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherGloves), 1025062, 5062, true, BulkMaterialType.None ));break;}
				case 4613:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherGloves), 1025062, 5062, true, BulkMaterialType.Spined ));break; }
				case 4614:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherGloves), 1025062, 5062, true, BulkMaterialType.Spined ));break;}
				case 4615:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherGloves), 1025062, 5062, true, BulkMaterialType.Spined ));break;}
				case 4616:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherGloves), 1025062, 5062, true, BulkMaterialType.Horned ));break; }
				case 4617:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherGloves), 1025062, 5062, true, BulkMaterialType.Horned ));break;}
				case 4618:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherGloves), 1025062, 5062, true, BulkMaterialType.Horned ));break;}
				case 4619:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherGloves), 1025062, 5062, true, BulkMaterialType.Barbed ));break; }
				case 4620:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherGloves), 1025062, 5062, true, BulkMaterialType.Barbed ));break;}
				case 4621:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherGloves), 1025062, 5062, true, BulkMaterialType.Barbed ));break;}
				case 4710:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherArms), 1025069, 5069, true, BulkMaterialType.None ));break; }
				case 4711:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherArms), 1025069, 5069, true, BulkMaterialType.None ));break;}
				case 4712:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherArms), 1025069, 5069, true, BulkMaterialType.None ));break;}
				case 4713:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherArms), 1025069, 5069, true, BulkMaterialType.Spined ));break; }
				case 4714:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherArms), 1025069, 5069, true, BulkMaterialType.Spined ));break;}
				case 4715:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherArms), 1025069, 5069, true, BulkMaterialType.Spined ));break;}
				case 4716:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherArms), 1025069, 5069, true, BulkMaterialType.Horned ));break; }
				case 4717:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherArms), 1025069, 5069, true, BulkMaterialType.Horned ));break;}
				case 4718:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherArms), 1025069, 5069, true, BulkMaterialType.Horned ));break;}
				case 4719:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherArms), 1025069, 5069, true, BulkMaterialType.Barbed ));break; }
				case 4720:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherArms), 1025069, 5069, true, BulkMaterialType.Barbed ));break;}
				case 4721:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherArms), 1025069, 5069, true, BulkMaterialType.Barbed ));break;}
				case 4810:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherLegs), 1025067, 5067, true, BulkMaterialType.None ));break; }
				case 4811:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherLegs), 1025067, 5067, true, BulkMaterialType.None ));break;}
				case 4812:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherLegs), 1025067, 5067, true, BulkMaterialType.None ));break;}
				case 4813:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherLegs), 1025067, 5067, true, BulkMaterialType.Spined ));break; }
				case 4814:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherLegs), 1025067, 5067, true, BulkMaterialType.Spined ));break;}
				case 4815:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherLegs), 1025067, 5067, true, BulkMaterialType.Spined ));break;}
				case 4816:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherLegs), 1025067, 5067, true, BulkMaterialType.Horned ));break; }
				case 4817:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherLegs), 1025067, 5067, true, BulkMaterialType.Horned ));break;}
				case 4818:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherLegs), 1025067, 5067, true, BulkMaterialType.Horned ));break;}
				case 4819:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherLegs), 1025067, 5067, true, BulkMaterialType.Barbed ));break; }
				case 4820:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherLegs), 1025067, 5067, true, BulkMaterialType.Barbed ));break;}
				case 4821:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherLegs), 1025067, 5067, true, BulkMaterialType.Barbed ));break;}
				case 4910:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherChest), 1025068, 5068, true, BulkMaterialType.None ));break; }
				case 4911:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherChest), 1025068, 5068, true, BulkMaterialType.None ));break;}
				case 4912:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherChest), 1025068, 5068, true, BulkMaterialType.None ));break;}
				case 4913:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherChest), 1025068, 5068, true, BulkMaterialType.Spined ));break; }
				case 4914:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherChest), 1025068, 5068, true, BulkMaterialType.Spined ));break;}
				case 4915:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherChest), 1025068, 5068, true, BulkMaterialType.Spined ));break;}
				case 4916:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherChest), 1025068, 5068, true, BulkMaterialType.Horned ));break; }
				case 4917:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherChest), 1025068, 5068, true, BulkMaterialType.Horned ));break;}
				case 4918:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherChest), 1025068, 5068, true, BulkMaterialType.Horned ));break;}
				case 4919:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherChest), 1025068, 5068, true, BulkMaterialType.Barbed ));break; }
				case 4920:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherChest), 1025068, 5068, true, BulkMaterialType.Barbed ));break;}
				case 4921:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherChest), 1025068, 5068, true, BulkMaterialType.Barbed ));break;}
				case 5010:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedGorget), 1025078, 5078, true, BulkMaterialType.None ));break; }
				case 5011:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedGorget), 1025078, 5078, true, BulkMaterialType.None ));break;}
				case 5012:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedGorget), 1025078, 5078, true, BulkMaterialType.None ));break;}
				case 5013:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedGorget), 1025078, 5078, true, BulkMaterialType.Spined ));break; }
				case 5014:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedGorget), 1025078, 5078, true, BulkMaterialType.Spined ));break;}
				case 5015:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedGorget), 1025078, 5078, true, BulkMaterialType.Spined ));break;}
				case 5016:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedGorget), 1025078, 5078, true, BulkMaterialType.Horned ));break; }
				case 5017:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedGorget), 1025078, 5078, true, BulkMaterialType.Horned ));break;}
				case 5018:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedGorget), 1025078, 5078, true, BulkMaterialType.Horned ));break;}
				case 5019:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedGorget), 1025078, 5078, true, BulkMaterialType.Barbed ));break; }
				case 5020:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedGorget), 1025078, 5078, true, BulkMaterialType.Barbed ));break;}
				case 5021:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedGorget), 1025078, 5078, true, BulkMaterialType.Barbed ));break;}
				case 5110:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedGloves), 1025077, 5077, true, BulkMaterialType.None ));break; }
				case 5111:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedGloves), 1025077, 5077, true, BulkMaterialType.None ));break;}
				case 5112:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedGloves), 1025077, 5077, true, BulkMaterialType.None ));break;}
				case 5113:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedGloves), 1025077, 5077, true, BulkMaterialType.Spined ));break; }
				case 5114:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedGloves), 1025077, 5077, true, BulkMaterialType.Spined ));break;}
				case 5115:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedGloves), 1025077, 5077, true, BulkMaterialType.Spined ));break;}
				case 5116:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedGloves), 1025077, 5077, true, BulkMaterialType.Horned ));break; }
				case 5117:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedGloves), 1025077, 5077, true, BulkMaterialType.Horned ));break;}
				case 5118:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedGloves), 1025077, 5077, true, BulkMaterialType.Horned ));break;}
				case 5119:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedGloves), 1025077, 5077, true, BulkMaterialType.Barbed ));break; }
				case 5120:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedGloves), 1025077, 5077, true, BulkMaterialType.Barbed ));break;}
				case 5121:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedGloves), 1025077, 5077, true, BulkMaterialType.Barbed ));break;}
				case 5210:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedArms), 1025084, 5084, true, BulkMaterialType.None ));break; }
				case 5211:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedArms), 1025084, 5084, true, BulkMaterialType.None ));break;}
				case 5212:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedArms), 1025084, 5084, true, BulkMaterialType.None ));break;}
				case 5213:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedArms), 1025084, 5084, true, BulkMaterialType.Spined ));break; }
				case 5214:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedArms), 1025084, 5084, true, BulkMaterialType.Spined ));break;}
				case 5215:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedArms), 1025084, 5084, true, BulkMaterialType.Spined ));break;}
				case 5216:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedArms), 1025084, 5084, true, BulkMaterialType.Horned ));break; }
				case 5217:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedArms), 1025084, 5084, true, BulkMaterialType.Horned ));break;}
				case 5218:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedArms), 1025084, 5084, true, BulkMaterialType.Horned ));break;}
				case 5219:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedArms), 1025084, 5084, true, BulkMaterialType.Barbed ));break; }
				case 5220:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedArms), 1025084, 5084, true, BulkMaterialType.Barbed ));break;}
				case 5221:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedArms), 1025084, 5084, true, BulkMaterialType.Barbed ));break;}
				case 5310:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedLegs), 1025082, 5082, true, BulkMaterialType.None ));break; }
				case 5311:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedLegs), 1025082, 5082, true, BulkMaterialType.None ));break;}
				case 5312:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedLegs), 1025082, 5082, true, BulkMaterialType.None ));break;}
				case 5313:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedLegs), 1025082, 5082, true, BulkMaterialType.Spined ));break; }
				case 5314:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedLegs), 1025082, 5082, true, BulkMaterialType.Spined ));break;}
				case 5315:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedLegs), 1025082, 5082, true, BulkMaterialType.Spined ));break;}
				case 5316:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedLegs), 1025082, 5082, true, BulkMaterialType.Horned ));break; }
				case 5317:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedLegs), 1025082, 5082, true, BulkMaterialType.Horned ));break;}
				case 5318:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedLegs), 1025082, 5082, true, BulkMaterialType.Horned ));break;}
				case 5319:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedLegs), 1025082, 5082, true, BulkMaterialType.Barbed ));break; }
				case 5320:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedLegs), 1025082, 5082, true, BulkMaterialType.Barbed ));break;}
				case 5321:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedLegs), 1025082, 5082, true, BulkMaterialType.Barbed ));break;}
				case 5410:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedChest), 1025083, 5083, true, BulkMaterialType.None ));break; }
				case 5411:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedChest), 1025083, 5083, true, BulkMaterialType.None ));break;}
				case 5412:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedChest), 1025083, 5083, true, BulkMaterialType.None ));break;}
				case 5413:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedChest), 1025083, 5083, true, BulkMaterialType.Spined ));break; }
				case 5414:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedChest), 1025083, 5083, true, BulkMaterialType.Spined ));break;}
				case 5415:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedChest), 1025083, 5083, true, BulkMaterialType.Spined ));break;}
				case 5416:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedChest), 1025083, 5083, true, BulkMaterialType.Horned ));break; }
				case 5417:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedChest), 1025083, 5083, true, BulkMaterialType.Horned ));break;}
				case 5418:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedChest), 1025083, 5083, true, BulkMaterialType.Horned ));break;}
				case 5419:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedChest), 1025083, 5083, true, BulkMaterialType.Barbed ));break; }
				case 5420:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedChest), 1025083, 5083, true, BulkMaterialType.Barbed ));break;}
				case 5421:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedChest), 1025083, 5083, true, BulkMaterialType.Barbed ));break;}
				case 5510:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneHelm), 1025201, 5201, true, BulkMaterialType.None ));break; }
				case 5511:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneHelm), 1025201, 5201, true, BulkMaterialType.None ));break;}
				case 5512:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneHelm), 1025201, 5201, true, BulkMaterialType.None ));break;}
				case 5513:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneHelm), 1025201, 5201, true, BulkMaterialType.Spined ));break; }
				case 5514:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneHelm), 1025201, 5201, true, BulkMaterialType.Spined ));break;}
				case 5515:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneHelm), 1025201, 5201, true, BulkMaterialType.Spined ));break;}
				case 5516:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneHelm), 1025201, 5201, true, BulkMaterialType.Horned ));break; }
				case 5517:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneHelm), 1025201, 5201, true, BulkMaterialType.Horned ));break;}
				case 5518:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneHelm), 1025201, 5201, true, BulkMaterialType.Horned ));break;}
				case 5519:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneHelm), 1025201, 5201, true, BulkMaterialType.Barbed ));break; }
				case 5520:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneHelm), 1025201, 5201, true, BulkMaterialType.Barbed ));break;}
				case 5521:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneHelm), 1025201, 5201, true, BulkMaterialType.Barbed ));break;}
				case 5610:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneGloves), 1025200, 5200, true, BulkMaterialType.None ));break; }
				case 5611:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneGloves), 1025200, 5200, true, BulkMaterialType.None ));break;}
				case 5612:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneGloves), 1025200, 5200, true, BulkMaterialType.None ));break;}
				case 5613:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneGloves), 1025200, 5200, true, BulkMaterialType.Spined ));break; }
				case 5614:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneGloves), 1025200, 5200, true, BulkMaterialType.Spined ));break;}
				case 5615:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneGloves), 1025200, 5200, true, BulkMaterialType.Spined ));break;}
				case 5616:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneGloves), 1025200, 5200, true, BulkMaterialType.Horned ));break; }
				case 5617:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneGloves), 1025200, 5200, true, BulkMaterialType.Horned ));break;}
				case 5618:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneGloves), 1025200, 5200, true, BulkMaterialType.Horned ));break;}
				case 5619:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneGloves), 1025200, 5200, true, BulkMaterialType.Barbed ));break; }
				case 5620:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneGloves), 1025200, 5200, true, BulkMaterialType.Barbed ));break;}
				case 5621:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneGloves), 1025200, 5200, true, BulkMaterialType.Barbed ));break;}
				case 5710:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneArms), 1025198, 5198, true, BulkMaterialType.None ));break; }
				case 5711:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneArms), 1025198, 5198, true, BulkMaterialType.None ));break;}
				case 5712:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneArms), 1025198, 5198, true, BulkMaterialType.None ));break;}
				case 5713:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneArms), 1025198, 5198, true, BulkMaterialType.Spined ));break; }
				case 5714:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneArms), 1025198, 5198, true, BulkMaterialType.Spined ));break;}
				case 5715:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneArms), 1025198, 5198, true, BulkMaterialType.Spined ));break;}
				case 5716:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneArms), 1025198, 5198, true, BulkMaterialType.Horned ));break; }
				case 5717:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneArms), 1025198, 5198, true, BulkMaterialType.Horned ));break;}
				case 5718:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneArms), 1025198, 5198, true, BulkMaterialType.Horned ));break;}
				case 5719:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneArms), 1025198, 5198, true, BulkMaterialType.Barbed ));break; }
				case 5720:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneArms), 1025198, 5198, true, BulkMaterialType.Barbed ));break;}
				case 5721:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneArms), 1025198, 5198, true, BulkMaterialType.Barbed ));break;}
				case 5810:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneLegs), 1025202, 5202, true, BulkMaterialType.None ));break; }
				case 5811:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneLegs), 1025202, 5202, true, BulkMaterialType.None ));break;}
				case 5812:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneLegs), 1025202, 5202, true, BulkMaterialType.None ));break;}
				case 5813:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneLegs), 1025202, 5202, true, BulkMaterialType.Spined ));break; }
				case 5814:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneLegs), 1025202, 5202, true, BulkMaterialType.Spined ));break;}
				case 5815:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneLegs), 1025202, 5202, true, BulkMaterialType.Spined ));break;}
				case 5816:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneLegs), 1025202, 5202, true, BulkMaterialType.Horned ));break; }
				case 5817:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneLegs), 1025202, 5202, true, BulkMaterialType.Horned ));break;}
				case 5818:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneLegs), 1025202, 5202, true, BulkMaterialType.Horned ));break;}
				case 5819:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneLegs), 1025202, 5202, true, BulkMaterialType.Barbed ));break; }
				case 5820:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneLegs), 1025202, 5202, true, BulkMaterialType.Barbed ));break;}
				case 5821:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneLegs), 1025202, 5202, true, BulkMaterialType.Barbed ));break;}
				case 5910:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneChest), 1025199, 5199, true, BulkMaterialType.None ));break; }
				case 5911:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneChest), 1025199, 5199, true, BulkMaterialType.None ));break;}
				case 5912:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneChest), 1025199, 5199, true, BulkMaterialType.None ));break;}
				case 5913:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneChest), 1025199, 5199, true, BulkMaterialType.Spined ));break; }
				case 5914:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneChest), 1025199, 5199, true, BulkMaterialType.Spined ));break;}
				case 5915:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneChest), 1025199, 5199, true, BulkMaterialType.Spined ));break;}
				case 5916:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneChest), 1025199, 5199, true, BulkMaterialType.Horned ));break; }
				case 5917:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneChest), 1025199, 5199, true, BulkMaterialType.Horned ));break;}
				case 5918:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneChest), 1025199, 5199, true, BulkMaterialType.Horned ));break;}
				case 5919:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BoneChest), 1025199, 5199, true, BulkMaterialType.Barbed ));break; }
				case 5920:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BoneChest), 1025199, 5199, true, BulkMaterialType.Barbed ));break;}
				case 5921:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BoneChest), 1025199, 5199, true, BulkMaterialType.Barbed ));break;}
				case 6010:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherShorts), 1027168, 7168, true, BulkMaterialType.None ));break; }
				case 6011:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherShorts), 1027168, 7168, true, BulkMaterialType.None ));break;}
				case 6012:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherShorts), 1027168, 7168, true, BulkMaterialType.None ));break;}
				case 6013:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherShorts), 1027168, 7168, true, BulkMaterialType.Spined ));break; }
				case 6014:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherShorts), 1027168, 7168, true, BulkMaterialType.Spined ));break;}
				case 6015:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherShorts), 1027168, 7168, true, BulkMaterialType.Spined ));break;}
				case 6016:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherShorts), 1027168, 7168, true, BulkMaterialType.Horned ));break; }
				case 6017:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherShorts), 1027168, 7168, true, BulkMaterialType.Horned ));break;}
				case 6018:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherShorts), 1027168, 7168, true, BulkMaterialType.Horned ));break;}
				case 6019:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherShorts), 1027168, 7168, true, BulkMaterialType.Barbed ));break; }
				case 6020:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherShorts), 1027168, 7168, true, BulkMaterialType.Barbed ));break;}
				case 6021:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherShorts), 1027168, 7168, true, BulkMaterialType.Barbed ));break;}	
				case 6110:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherSkirt), 1027176, 7176, true, BulkMaterialType.None ));break; }
				case 6111:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherSkirt), 1027176, 7176, true, BulkMaterialType.None ));break;}
				case 6112:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherSkirt), 1027176, 7176, true, BulkMaterialType.None ));break;}
				case 6113:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherSkirt), 1027176, 7176, true, BulkMaterialType.Spined ));break; }
				case 6114:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherSkirt), 1027176, 7176, true, BulkMaterialType.Spined ));break;}
				case 6115:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherSkirt), 1027176, 7176, true, BulkMaterialType.Spined ));break;}
				case 6116:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherSkirt), 1027176, 7176, true, BulkMaterialType.Horned ));break; }
				case 6117:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherSkirt), 1027176, 7176, true, BulkMaterialType.Horned ));break;}
				case 6118:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherSkirt), 1027176, 7176, true, BulkMaterialType.Horned ));break;}
				case 6119:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherSkirt), 1027176, 7176, true, BulkMaterialType.Barbed ));break; }
				case 6120:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherSkirt), 1027176, 7176, true, BulkMaterialType.Barbed ));break;}
				case 6121:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherSkirt), 1027176, 7176, true, BulkMaterialType.Barbed ));break;}
				case 6210:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherBustierArms), 1027178, 7178, true, BulkMaterialType.None ));break; }
				case 6211:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherBustierArms), 1027178, 7178, true, BulkMaterialType.None ));break;}
				case 6212:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherBustierArms), 1027178, 7178, true, BulkMaterialType.None ));break;}
				case 6213:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherBustierArms), 1027178, 7178, true, BulkMaterialType.Spined ));break; }
				case 6214:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherBustierArms), 1027178, 7178, true, BulkMaterialType.Spined ));break;}
				case 6215:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherBustierArms), 1027178, 7178, true, BulkMaterialType.Spined ));break;}
				case 6216:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherBustierArms), 1027178, 7178, true, BulkMaterialType.Horned ));break; }
				case 6217:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherBustierArms), 1027178, 7178, true, BulkMaterialType.Horned ));break;}
				case 6218:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherBustierArms), 1027178, 7178, true, BulkMaterialType.Horned ));break;}
				case 6219:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LeatherBustierArms), 1027178, 7178, true, BulkMaterialType.Barbed ));break; }
				case 6220:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LeatherBustierArms), 1027178, 7178, true, BulkMaterialType.Barbed ));break;}
				case 6221:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LeatherBustierArms), 1027178, 7178, true, BulkMaterialType.Barbed ));break;}
				case 6310:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedBustierArms), 1027180, 7180, true, BulkMaterialType.None ));break; }
				case 6311:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedBustierArms), 1027180, 7180, true, BulkMaterialType.None ));break;}
				case 6312:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedBustierArms), 1027180, 7180, true, BulkMaterialType.None ));break;}
				case 6313:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedBustierArms), 1027180, 7180, true, BulkMaterialType.Spined ));break; }
				case 6314:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedBustierArms), 1027180, 7180, true, BulkMaterialType.Spined ));break;}
				case 6315:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedBustierArms), 1027180, 7180, true, BulkMaterialType.Spined ));break;}
				case 6316:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedBustierArms), 1027180, 7180, true, BulkMaterialType.Horned ));break; }
				case 6317:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedBustierArms), 1027180, 7180, true, BulkMaterialType.Horned ));break;}
				case 6318:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedBustierArms), 1027180, 7180, true, BulkMaterialType.Horned ));break;}
				case 6319:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StuddedBustierArms), 1027180, 7180, true, BulkMaterialType.Barbed ));break; }
				case 6320:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StuddedBustierArms), 1027180, 7180, true, BulkMaterialType.Barbed ));break;}
				case 6321:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StuddedBustierArms), 1027180, 7180, true, BulkMaterialType.Barbed ));break;}
				case 6410:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(FemaleLeatherChest), 1027174, 7174, true, BulkMaterialType.None ));break; }
				case 6411:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(FemaleLeatherChest), 1027174, 7174, true, BulkMaterialType.None ));break;}
				case 6412:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(FemaleLeatherChest), 1027174, 7174, true, BulkMaterialType.None ));break;}
				case 6413:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(FemaleLeatherChest), 1027174, 7174, true, BulkMaterialType.Spined ));break; }
				case 6414:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(FemaleLeatherChest), 1027174, 7174, true, BulkMaterialType.Spined ));break;}
				case 6415:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(FemaleLeatherChest), 1027174, 7174, true, BulkMaterialType.Spined ));break;}
				case 6416:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(FemaleLeatherChest), 1027174, 7174, true, BulkMaterialType.Horned ));break; }
				case 6417:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(FemaleLeatherChest), 1027174, 7174, true, BulkMaterialType.Horned ));break;}
				case 6418:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(FemaleLeatherChest), 1027174, 7174, true, BulkMaterialType.Horned ));break;}
				case 6419:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(FemaleLeatherChest), 1027174, 7174, true, BulkMaterialType.Barbed ));break; }
				case 6420:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(FemaleLeatherChest), 1027174, 7174, true, BulkMaterialType.Barbed ));break;}
				case 6421:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(FemaleLeatherChest), 1027174, 7174, true, BulkMaterialType.Barbed ));break;}
				case 6510:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(FemaleStuddedChest), 1027170, 7170, true, BulkMaterialType.None ));break; }
				case 6511:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(FemaleStuddedChest), 1027170, 7170, true, BulkMaterialType.None ));break;}
				case 6512:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(FemaleStuddedChest), 1027170, 7170, true, BulkMaterialType.None ));break;}
				case 6513:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(FemaleStuddedChest), 1027170, 7170, true, BulkMaterialType.Spined ));break; }
				case 6514:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(FemaleStuddedChest), 1027170, 7170, true, BulkMaterialType.Spined ));break;}
				case 6515:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(FemaleStuddedChest), 1027170, 7170, true, BulkMaterialType.Spined ));break;}
				case 6516:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(FemaleStuddedChest), 1027170, 7170, true, BulkMaterialType.Horned ));break; }
				case 6517:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(FemaleStuddedChest), 1027170, 7170, true, BulkMaterialType.Horned ));break;}
				case 6518:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(FemaleStuddedChest), 1027170, 7170, true, BulkMaterialType.Horned ));break;}
				case 6519:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(FemaleStuddedChest), 1027170, 7170, true, BulkMaterialType.Barbed ));break; }
				case 6520:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(FemaleStuddedChest), 1027170, 7170, true, BulkMaterialType.Barbed ));break;}
				case 6521:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(FemaleStuddedChest), 1027170, 7170, true, BulkMaterialType.Barbed ));break;}
				case 6601:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(Bandana), 1025440, 5440, true, BulkMaterialType.None ));break; }
				case 6602:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(Bandana), 1025440, 5440, true, BulkMaterialType.None ));break;}
				case 6603:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(Bandana), 1025440, 5440, true, BulkMaterialType.None ));break;}
				case 6701:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(SkullCap), 1025444, 5444, true, BulkMaterialType.None ));break; }
				case 6702:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(SkullCap), 1025444, 5444, true, BulkMaterialType.None ));break;}
				case 6703:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(SkullCap), 1025444, 5444, true, BulkMaterialType.None ));break;}
				case 6801:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(FloppyHat), 1025907, 5907, true, BulkMaterialType.None ));break; }
				case 6802:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(FloppyHat), 1025907, 5907, true, BulkMaterialType.None ));break;}
				case 6803:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(FloppyHat), 1025907, 5907, true, BulkMaterialType.None ));break;}
				case 6901:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(WideBrimHat), 1025908, 5908, true, BulkMaterialType.None ));break; }
				case 6902:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(WideBrimHat), 1025908, 5908, true, BulkMaterialType.None ));break;}
				case 6903:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(WideBrimHat), 1025908, 5908, true, BulkMaterialType.None ));break;}
				case 7001:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(Cap), 1025909, 5909, true, BulkMaterialType.None ));break; }
				case 7002:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(Cap), 1025909, 5909, true, BulkMaterialType.None ));break;}
				case 7003:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(Cap), 1025909, 5909, true, BulkMaterialType.None ));break;}
				case 7101:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(TallStrawHat), 1025910, 5910, true, BulkMaterialType.None ));break; }
				case 7102:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(TallStrawHat), 1025910, 5910, true, BulkMaterialType.None ));break;}
				case 7103:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(TallStrawHat), 1025910, 5910, true, BulkMaterialType.None ));break;}
				case 7201:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(StrawHat), 1025911, 5911, true, BulkMaterialType.None ));break; }
				case 7202:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(StrawHat), 1025911, 5911, true, BulkMaterialType.None ));break;}
				case 7203:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(StrawHat), 1025911, 5911, true, BulkMaterialType.None ));break;}
				case 7301:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(WizardsHat), 1025912, 5912, true, BulkMaterialType.None ));break; }
				case 7302:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(WizardsHat), 1025912, 5912, true, BulkMaterialType.None ));break;}
				case 7303:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(WizardsHat), 1025912, 5912, true, BulkMaterialType.None ));break;}
				case 7401:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(Bonnet), 1025913, 5913, true, BulkMaterialType.None ));break; }
				case 7402:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(Bonnet), 1025913, 5913, true, BulkMaterialType.None ));break;}
				case 7403:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(Bonnet), 1025913, 5913, true, BulkMaterialType.None ));break;}
				case 7501:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(FeatheredHat), 1025914, 5914, true, BulkMaterialType.None ));break; }
				case 7502:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(FeatheredHat), 1025914, 5914, true, BulkMaterialType.None ));break;}
				case 7503:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(FeatheredHat), 1025914, 5914, true, BulkMaterialType.None ));break;}
				case 7601:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(TricorneHat), 1025915, 5915, true, BulkMaterialType.None ));break; }
				case 7602:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(TricorneHat), 1025915, 5915, true, BulkMaterialType.None ));break;}
				case 7603:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(TricorneHat), 1025915, 5915, true, BulkMaterialType.None ));break;}
				case 7701:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(JesterHat), 1025916, 5916, true, BulkMaterialType.None ));break; }
				case 7702:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(JesterHat), 1025916, 5916, true, BulkMaterialType.None ));break;}
				case 7703:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(JesterHat), 1025916, 5916, true, BulkMaterialType.None ));break;}
				case 7801:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(Doublet), 1028059, 8059, true, BulkMaterialType.None ));break; }
				case 7802:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(Doublet), 1028059, 8059, true, BulkMaterialType.None ));break;}
				case 7803:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(Doublet), 1028059, 8059, true, BulkMaterialType.None ));break;}
				case 7901:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(Shirt), 1025399, 5399, true, BulkMaterialType.None ));break; }
				case 7902:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(Shirt), 1025399, 5399, true, BulkMaterialType.None ));break;}
				case 7903:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(Shirt), 1025399, 5399, true, BulkMaterialType.None ));break;}
				case 8001:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(FancyShirt), 1027933, 7933, true, BulkMaterialType.None ));break; }
				case 8002:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(FancyShirt), 1027933, 7933, true, BulkMaterialType.None ));break;}
				case 8003:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(FancyShirt), 1027933, 7933, true, BulkMaterialType.None ));break;}
				case 8101:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(Tunic), 1028097, 8097, true, BulkMaterialType.None ));break; }
				case 8102:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(Tunic), 1028097, 8097, true, BulkMaterialType.None ));break;}
				case 8103:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(Tunic), 1028097, 8097, true, BulkMaterialType.None ));break;}
				case 8201:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(Surcoat), 1028189, 8189, true, BulkMaterialType.None ));break; }
				case 8202:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(Surcoat), 1028189, 8189, true, BulkMaterialType.None ));break;}
				case 8203:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(Surcoat), 1028189, 8189, true, BulkMaterialType.None ));break;}
				case 8301:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(PlainDress), 1027937, 7937, true, BulkMaterialType.None ));break; }
				case 8302:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(PlainDress), 1027937, 7937, true, BulkMaterialType.None ));break;}
				case 8303:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(PlainDress), 1027937, 7937, true, BulkMaterialType.None ));break;}
				case 8401:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(FancyDress), 1027936, 7936, true, BulkMaterialType.None ));break; }
				case 8402:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(FancyDress), 1027936, 7936, true, BulkMaterialType.None ));break;}
				case 8403:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(FancyDress), 1027936, 7936, true, BulkMaterialType.None ));break;}
				case 8501:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(Cloak), 1025397, 5397, true, BulkMaterialType.None ));break; }
				case 8502:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(Cloak), 1025397, 5397, true, BulkMaterialType.None ));break;}
				case 8503:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(Cloak), 1025397, 5397, true, BulkMaterialType.None ));break;}
				case 8601:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(Robe), 1027939, 7939, true, BulkMaterialType.None ));break; }
				case 8602:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(Robe), 1027939, 7939, true, BulkMaterialType.None ));break;}
				case 8603:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(Robe), 1027939, 7939, true, BulkMaterialType.None ));break;}
				case 8701:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(JesterSuit), 1028095, 8095, true, BulkMaterialType.None ));break; }
				case 8702:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(JesterSuit), 1028095, 8095, true, BulkMaterialType.None ));break;}
				case 8703:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(JesterSuit), 1028095, 8095, true, BulkMaterialType.None ));break;}
				case 8801:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(ShortPants), 1025422, 5422, true, BulkMaterialType.None ));break; }
				case 8802:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(ShortPants), 1025422, 5422, true, BulkMaterialType.None ));break;}
				case 8803:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(ShortPants), 1025422, 5422, true, BulkMaterialType.None ));break;}
				case 8901:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(LongPants), 1025433, 5433, true, BulkMaterialType.None ));break; }
				case 8902:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(LongPants), 1025433, 5433, true, BulkMaterialType.None ));break;}
				case 8903:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(LongPants), 1025433, 5433, true, BulkMaterialType.None ));break;}
				case 9001:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(Kilt), 1025431, 5431, true, BulkMaterialType.None ));break; }
				case 9002:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(Kilt), 1025431, 5431, true, BulkMaterialType.None ));break;}
				case 9003:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(Kilt), 1025431, 5431, true, BulkMaterialType.None ));break;}
				case 9101:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(Skirt), 1025398, 5398, true, BulkMaterialType.None ));break; }
				case 9102:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(Skirt), 1025398, 5398, true, BulkMaterialType.None ));break;}
				case 9103:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(Skirt), 1025398, 5398, true, BulkMaterialType.None ));break;}
				case 9201:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(Boots), 1025899, 5899, true, BulkMaterialType.None ));break; }
				case 9202:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(Boots), 1025899, 5899, true, BulkMaterialType.None ));break;}
				case 9203:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(Boots), 1025899, 5899, true, BulkMaterialType.None ));break;}
				case 9301:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(ThighBoots), 1025905, 5905, true, BulkMaterialType.None ));break; }
				case 9302:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(ThighBoots), 1025905, 5905, true, BulkMaterialType.None ));break;}
				case 9303:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(ThighBoots), 1025905, 5905, true, BulkMaterialType.None ));break;}
				case 9401:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(Shoes), 1025903, 5903, true, BulkMaterialType.None ));break; }
				case 9402:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(Shoes), 1025903, 5903, true, BulkMaterialType.None ));break;}
				case 9403:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(Shoes), 1025903, 5903, true, BulkMaterialType.None ));break;}
				case 9501:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(Sandals), 1025901, 5901, true, BulkMaterialType.None ));break; }
				case 9502:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(Sandals), 1025901, 5901, true, BulkMaterialType.None ));break;}
				case 9503:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(Sandals), 1025901, 5901, true, BulkMaterialType.None ));break;}
				case 9601:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(BodySash), 1025441, 5441, true, BulkMaterialType.None ));break; }
				case 9602:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(BodySash), 1025441, 5441, true, BulkMaterialType.None ));break;}
				case 9603:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(BodySash), 1025441, 5441, true, BulkMaterialType.None ));break;}
				case 9701:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(HalfApron), 1025435, 5435, true, BulkMaterialType.None ));break; }
				case 9702:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(HalfApron), 1025435, 5435, true, BulkMaterialType.None ));break;}
				case 9703:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(HalfApron), 1025435, 5435, true, BulkMaterialType.None ));break;}
				case 9801:{from.AddToBackpack(new SmallTailorBOD( 0, 10, typeof(FullApron), 1025437, 5437, true, BulkMaterialType.None ));break; }
				case 9802:{from.AddToBackpack(new SmallTailorBOD( 0, 15, typeof(FullApron), 1025437, 5437, true, BulkMaterialType.None ));break;}
				case 9803:{from.AddToBackpack(new SmallTailorBOD( 0, 20, typeof(FullApron), 1025437, 5437, true, BulkMaterialType.None ));break;}
				
				
			}			
		}
	}
}