using System;

namespace Server.Items
{
	public class ClosedBarrel : BaseContainer
	{
        public override int DefaultDropSound { get { return 0x42; } }
        
        public override int DefaultGumpID { get { return 0x3E; } }

		[Constructable]
		public ClosedBarrel() : base( 0x0FAE )
		{
			Weight = 25.0;
       
		}

        //public override void GetProperties(ObjectPropertyList list)
        //{
        //    base.GetProperties(list);
        //
        //   list.Add("Artifact Rarity 5");
        //}

		public ClosedBarrel( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();

			if ( Weight == 0.0 )
				Weight = 25.0;
		}
	}
}