using System;
using Server.Network;
using Server.Items;

namespace Server.Mobiles
{
    [CorpseName("a corpse of giant roach")] 
    public class GiantRoach : Beetle
    {

        [Constructable]
        public GiantRoach() : base()
        {

            Name = "a cock roach";
            Hue = 1175;

            
            SetStr(100,300);
            SetDex(150, 300); 
            SetInt(50, 100);

            SetHits(200, 350); 
            SetMana(0); 

            
            SetDamageType(ResistanceType.Physical, 22);
            SetDamageType(ResistanceType.Cold, 22);
            SetDamageType(ResistanceType.Fire, 22);
            SetDamageType(ResistanceType.Energy, 22);
            SetDamageType(ResistanceType.Poison, 22);

            
            SetResistance(ResistanceType.Physical, 10, 30);
            SetResistance(ResistanceType.Cold, 10, 30);
            SetResistance(ResistanceType.Fire, 10, 30);
            SetResistance(ResistanceType.Energy, 10, 30);
            SetResistance(ResistanceType.Poison, 10, 30);

            Fame = 2200; 

            Karma = -2200; 


            Tamable = true; 
            ControlSlots = 2; 
            MinTameSkill = 96.1; 
   
        }

        public override bool AlwaysMurderer { get { return false; } } 
        public override FoodType FavoriteFood { get { return FoodType.Fish | FoodType.Meat; } } 
        public override int Meat { get { return 2; } }

        public GiantRoach(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}
