using System.IO;
using Server.Gumps;
using Server.Mobiles;

namespace Server.Commands
{
    [PropertyObject]
    public sealed class VendorPriceControl
    {
        private static int m_BuyScale;
        private static int m_SellScale;
        private static VendorPriceControl m_Instance = new VendorPriceControl();

        [CommandProperty(AccessLevel.GameMaster)]
        public static int BuyScale
        {
            get
            {
                return m_BuyScale;
            }
            set
            {
                m_BuyScale = value;
            }
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public static int SellScale
        {
            get
            {
                return m_SellScale;
            }
            set
            {
                m_SellScale = value;
            }
        }

        public static void Initialize()
        {
            Server.EventSink.WorldSave += new WorldSaveEventHandler(SaveVPCSettings);
            if (File.Exists(Path.Combine(Core.BaseDirectory, @"Export\Saves\Current\Vendor\VPCSettings.bin")))
            {
                Load();
            }
            else
                LoadDefaults();

            CommandSystem.Register("VendorPriceControl", AccessLevel.GameMaster, new CommandEventHandler(VPC_OnCommand));
        }

        public static void SaveVPCSettings(WorldSaveEventArgs e)
        {
            string SavePath = Path.Combine(Core.BaseDirectory, @"Export\Saves\Current\Vendor\VPCSettings.bin");

            if (!Directory.Exists(Path.Combine(Core.BaseDirectory, @"Export\Saves\Current\Vendor")))
            {
                Directory.CreateDirectory(Path.Combine(Core.BaseDirectory, @"Export\Saves\Current\Vendor"));
            }

            GenericWriter writer = new BinaryFileWriter(SavePath, true);

            try
            {
                Serialize(writer);
            }
            catch
            { }
            writer.Close();
        }

        private static void Load()
        {
            try
            {
                string SavePath = Path.Combine(Core.BaseDirectory, @"Export\Saves\Current\Vendor\VPCSettings.bin");

                using (FileStream fs = new FileStream(SavePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    BinaryReader br = new BinaryReader(fs);
                    BinaryFileReader reader = new BinaryFileReader(br);

                    try
                    {
                        Deserialize(reader);
                    }
                    catch
                    { }
                    reader.Close();
                }
            }
            catch
            { }
        }

        private static void LoadDefaults()
        {
            m_BuyScale = 100;
            m_SellScale = 100;
        }

        public static void Serialize(GenericWriter writer)
        {
            writer.Write(0);
            writer.Write(m_BuyScale);
            writer.Write(m_SellScale);
        }

        public static void Deserialize(GenericReader reader)
        {
            int version = reader.ReadInt();

            switch (version)
            {
                case 0:
                    {
                        m_BuyScale = reader.ReadInt();
                        m_SellScale = reader.ReadInt();
                        break;
                    }
            }
        }

        [Usage("VendorPriceControl")]
        [Description("Allows scaling of vendor prices.")]
        public static void VPC_OnCommand(CommandEventArgs e)
        {
            PlayerMobile from = e.Mobile as PlayerMobile;

            if (from != null && !from.Deleted)
            {
                from.SendGump(new PropertiesGump(e.Mobile, m_Instance));
            }
        }
    }
}