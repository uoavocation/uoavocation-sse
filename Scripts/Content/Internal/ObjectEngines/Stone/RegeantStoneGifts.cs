using System;
using Server;
using Server.Items;

namespace Server.Items
{
	public class BagOfReagents : Bag
	{
		[Constructable]
		public BagOfReagents() : this( 50 )
		{
		}

		[Constructable]
		public BagOfReagents( int amount )
		{
			DropItem( new BlackPearl   ( amount ) );
			DropItem( new Bloodmoss    ( amount ) );
			DropItem( new Garlic       ( amount ) );
			DropItem( new Ginseng      ( amount ) );
			DropItem( new MandrakeRoot ( amount ) );
			DropItem( new Nightshade   ( amount ) );
			DropItem( new SulfurousAsh ( amount ) );
			DropItem( new SpidersSilk  ( amount ) );
		}

		public BagOfReagents( Serial serial ) : base( serial )
		{
		}

		public override void Serialize( GenericWriter writer )
		{
			base.Serialize( writer );

			writer.Write( (int) 0 ); // version
		}

		public override void Deserialize( GenericReader reader )
		{
			base.Deserialize( reader );

			int version = reader.ReadInt();
		}
	}

    public class BagOfNecroReagents : Bag
    {
        [Constructable]
        public BagOfNecroReagents(): this(50)
        {
        }

        [Constructable]
        public BagOfNecroReagents(int amount)
        {
            DropItem(new BatWing(amount));
            DropItem(new GraveDust(amount));
            DropItem(new DaemonBlood(amount));
            DropItem(new NoxCrystal(amount));
            DropItem(new PigIron(amount));
        }

        public BagOfNecroReagents(Serial serial): base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }

    public class BagOfDruidReagents : Bag
    {

        [Constructable]
        public BagOfDruidReagents()
            : this(1)
        {
            Movable = true;
            Hue = 0x48C;
            Name = "bag of druid reagents";
        }

        [Constructable]
        public BagOfDruidReagents(int amount)
        {
            DropItem(new BlackPearl(35));
            DropItem(new Bloodmoss(35));
            DropItem(new Garlic(35));
            DropItem(new Ginseng(35));
            DropItem(new MandrakeRoot(35));
            DropItem(new Nightshade(35));
            DropItem(new SulfurousAsh(35));
            DropItem(new SpidersSilk(35));
            DropItem(new PetrafiedWood(35));
            DropItem(new DestroyingAngel(35));
            DropItem(new SpringWater(35));
        }

        public BagOfDruidReagents(Serial serial)
            : base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }

    public class BagOfAllReagents : Bag
    {
        [Constructable]
        public BagOfAllReagents(): this(50)
        {
        }

        [Constructable]
        public BagOfAllReagents(int amount)
        {
            DropItem(new BlackPearl(amount));
            DropItem(new Bloodmoss(amount));
            DropItem(new Garlic(amount));
            DropItem(new Ginseng(amount));
            DropItem(new MandrakeRoot(amount));
            DropItem(new Nightshade(amount));
            DropItem(new SulfurousAsh(amount));
            DropItem(new SpidersSilk(amount));
            DropItem(new BatWing(amount));
            DropItem(new GraveDust(amount));
            DropItem(new DaemonBlood(amount));
            DropItem(new NoxCrystal(amount));
            DropItem(new PigIron(amount));
            DropItem(new PetrafiedWood(35));
            DropItem(new DestroyingAngel(35));
            DropItem(new SpringWater(35));
        }

        public BagOfAllReagents(Serial serial): base(serial)
        {
        }

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);

            writer.Write((int)0); // version
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);

            int version = reader.ReadInt();
        }
    }
}